//! Cell primitives
#![allow(rustdoc::broken_intra_doc_links)]
#[cfg(not(loom))]
pub use std::*;

#[cfg(loom)]
pub use loom::*;

/// Reexport of the overlaps of the [`::std::cell`] and [`loom::cell`] cell
/// modules with the std implementations.
#[cfg(not(loom))]
mod std {
	pub use cell::Cell;
	use std::cell;

	/// The standard [`UnsafeCell`](::std::cell::UnsafeCell) with loom api
	#[derive(Debug)]
	pub struct UnsafeCell<T>(std::cell::UnsafeCell<T>);
	impl<T> UnsafeCell<T> {
		/// Create a new UnsafeCell with [`::std::cell::UnsafeCell::new`]
		pub fn new(data: T) -> UnsafeCell<T> {
			UnsafeCell(std::cell::UnsafeCell::new(data))
		}

		/// Get an immuable point to the wrapped value
		pub fn with<R>(&self, f: impl FnOnce(*const T) -> R) -> R {
			f(self.0.get())
		}

		/// Get an mutable point to the wrapped value
		pub fn with_mut<R>(&self, f: impl FnOnce(*mut T) -> R) -> R {
			f(self.0.get())
		}

		/// Unwraps the value.
		pub fn into_inner(self) -> T {
			self.0.into_inner()
		}
	}
}

/// Reexport of the overlaps of the [`::std::cell`] and [`loom::cell`] cell
/// modules with the loom implementations.
#[cfg(loom)]
mod loom {
	pub use cell::Cell;
	pub use cell::UnsafeCell;
	use loom::cell;
}
