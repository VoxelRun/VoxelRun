//! Compiler and Processor hints
#![allow(rustdoc::broken_intra_doc_links)]
#[cfg(not(loom))]
pub use std::*;

#[cfg(loom)]
pub use loom::*;

/// Reexport of the overlaps of the [`::std::hint`] and [`loom::hint`] hint
/// modules with the std implementations.
#[cfg(not(loom))]
mod std {
	pub use hint::spin_loop;
	pub use hint::unreachable_unchecked;
	use std::hint;
}

/// Reexport of the overlaps of the [`::std::hint`] and [`loom::hint`] hint
/// modules with the loom implementations.
#[cfg(loom)]
mod loom {
	pub use hint::spin_loop;
	pub use hint::unreachable_unchecked;
	use loom::hint;
}
