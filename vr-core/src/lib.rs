//! The Core utilities used in all code modules of the VoxelRun game
pub mod cell;
pub mod hint;
pub mod sync;
pub mod thread;
pub mod unwind;
pub mod utils;

pub use declarations::*;

/// declarations of Shorts and Longs for systems which have a pointer width of
/// 32 bit
#[cfg(target_pointer_width = "32")]
mod declarations {
	/// A half width integer type
	pub type Short = i16;
	/// A full width integer type
	pub type Long = i32;

	/// A half width unsigned integer type
	pub type UnsignedShort = u16;
	/// A full width unsigned integer type
	pub type UnsignedLong = u32;
}

/// declarations of Shorts and Longs for systems which have a pointer width of
/// 64 bit
#[cfg(target_pointer_width = "64")]
mod declarations {
	/// A half width integer type
	pub type Short = i32;
	/// A full width integer type
	pub type Long = i64;

	/// A half width unsigned integer type
	pub type UnsignedShort = u32;
	/// A full width unsigned integer type
	pub type UnsignedLong = u64;
}

/// An assertion which gets executed on debug and loom builds
#[macro_export]
macro_rules! debug_or_loom_assert {
    ($($arg:tt)*) => (if cfg!(any(debug_assertions, loom)) { assert!($($arg)*) })
}

/// An assertion which gets executed on debug and loom builds
#[macro_export]
macro_rules! debug_or_loom_assert_eq {
    ($($arg:tt)*) => (if cfg!(any(debug_assertions, loom)) { assert_eq!($($arg)*) })
}

/// An assertion which gets executed on debug and loom builds
#[macro_export]
macro_rules! debug_or_loom_assert_ne {
    ($($arg:tt)*) => (if cfg!(any(debug_assertions, loom)) { assert_ne!($($arg)*) })
}
