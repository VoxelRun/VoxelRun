//! Useful synchronization primitives.
//!
//! ## The need for testability
//!
//! Conceptually, synchronization primitives are hard to work with and write
//! correct code with. Therefore, certain libraries to test correctness of
//! parellelized execution were developed. One of them is
//! [loom](https://github.com/tokio-rs/loom) by the developers of the tokio
//! asynchronous runtime. It is useful for checking all possible executions of
//! a concurrent task, but to properly track atomic operations and other
//! primitives provided by the [standard library](::std::sync) it needs its
//! own mock implementation.
//!
//! This module re-exposes these mock implementation or their
//! [standard library](::std::sync) counterpart depending on if the loom
//! compiler configuration is active.
//!
//! Additionally it provides the Atomic alternatives to the
//! [`Short`](crate::Short), [`UnsignedShort`](crate::UnsignedShort),
//! [`Long`](crate::Long), [crate::UnsignedLong] types.

#![allow(rustdoc::broken_intra_doc_links)]
#[cfg(not(loom))]
pub use std::*;

#[cfg(loom)]
pub use loom::*;

/// declarations of Shorts and Longs for systems which posesss 64 bit atomics
#[cfg(target_pointer_width = "64")]
mod declarations {
	/// A half width atomic integer type
	pub type AtomicShort = super::atomic::AtomicI32;
	/// A full width atomic integer type
	pub type AtomicLong = super::atomic::AtomicI64;

	/// A half width unsigned atomic integer type
	pub type AtomicUnsignedShort = super::atomic::AtomicU32;
	/// A full width unsigned atomic integer type
	pub type AtomicUnsignedLong = super::atomic::AtomicU64;
}

/// declarations of Shorts and Longs for systems which posesss 32 bit atomics
#[cfg(target_pointer_width = "32")]
mod declarations {
	/// A half width atomic integer type
	pub type AtomicShort = super::atomic::AtomicI16;
	/// A full width atomic integer type
	pub type AtomicLong = super::atomic::AtomicI32;

	/// A half width unsigned atomic integer type
	pub type AtomicUnsignedShort = super::atomic::AtomicU16;
	/// A full width unsigned atomic integer type
	pub type AtomicUnsignedLong = super::atomic::AtomicU32;
}

/// Reexport of the overlaps of the [`::std::sync`] and [`loom::sync`] sync
/// modules with the standard implementations
#[cfg(not(loom))]
mod std {
	use std::sync;
	/// Reexport of the overlaps of the [`::std::sync::atomic`] and [`loom::sync::atomic`] sync
	/// modules with the standard implementations
	pub mod atomic {
		pub use atomic::AtomicU16;
		pub use atomic::AtomicU32;
		pub use atomic::AtomicU64;
		pub use atomic::AtomicU8;
		pub use atomic::AtomicUsize;
		use std::sync::atomic;

		pub use atomic::AtomicI16;
		pub use atomic::AtomicI32;
		pub use atomic::AtomicI64;
		pub use atomic::AtomicI8;
		pub use atomic::AtomicIsize;

		pub use atomic::AtomicBool;
		pub use atomic::AtomicPtr;

		pub use atomic::fence;

		pub use super::super::declarations::*;
	}
	/// Reexport of the overlaps of the [`::std::sync::mpsc`] and [`loom::sync::mpsc`] sync
	/// modules with the standard implementations
	pub mod mpsc {
		pub use mpsc::channel;
		pub use mpsc::Receiver;
		pub use mpsc::Sender;
		use std::sync::mpsc;
	}
	pub use sync::Arc;
	pub use sync::Barrier;
	pub use sync::Condvar;
	pub use sync::Mutex;
	pub use sync::MutexGuard;
	pub use sync::RwLock;
	pub use sync::RwLockReadGuard;
	pub use sync::RwLockWriteGuard;
	pub use sync::WaitTimeoutResult;
}

/// Reexport of the overlaps of the [`::std::sync`] and [`loom::sync`] sync
/// modules with the loom implementations
#[cfg(loom)]
mod loom {
	use loom::sync;
	/// Reexport of the overlaps of the [`::std::sync::atomic`] and [`loom::sync::atomic`] sync
	/// modules with the loom implementations
	pub mod atomic {
		pub use atomic::AtomicU16;
		pub use atomic::AtomicU32;
		pub use atomic::AtomicU64;
		pub use atomic::AtomicU8;
		pub use atomic::AtomicUsize;
		use loom::sync::atomic;

		pub use atomic::AtomicI16;
		pub use atomic::AtomicI32;
		pub use atomic::AtomicI64;
		pub use atomic::AtomicI8;
		pub use atomic::AtomicIsize;

		pub use atomic::AtomicBool;
		pub use atomic::AtomicPtr;

		pub use atomic::fence;

		pub use super::super::declarations::*;
	}
	/// Reexport of the overlaps of the [`::std::sync::mpsc`] and [`loom::sync::mpsc`] sync
	/// modules with the loom implementations
	pub mod mpsc {
		use loom::sync::mpsc;
		pub use mpsc::channel;
		pub use mpsc::Receiver;
		pub use mpsc::Sender;
	}
	pub use sync::Arc;
	pub use sync::Barrier;
	pub use sync::Condvar;
	pub use sync::Mutex;
	pub use sync::MutexGuard;
	pub use sync::RwLock;
	pub use sync::RwLockReadGuard;
	pub use sync::RwLockWriteGuard;
	pub use sync::WaitTimeoutResult;
}
