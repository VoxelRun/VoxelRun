//! Thread primitives
#![allow(rustdoc::broken_intra_doc_links)]
#[cfg(not(loom))]
pub use std::*;

#[cfg(loom)]
pub use loom::*;

/// Reexport of the overlaps of the [`::std::thread`] and [`loom::thread`] thread
/// modules with the standard implementations
#[cfg(not(loom))]
mod std {
	use std::thread;
	pub use thread::current;
	pub use thread::park;
	pub use thread::spawn;
	pub use thread::yield_now;
	pub use thread::AccessError;
	pub use thread::Builder;
	pub use thread::JoinHandle;
	pub use thread::LocalKey;
	pub use thread::Thread;
	pub use thread::ThreadId;
}

/// Reexport of the overlaps of the [`::std::thread`] and [`loom::thread`] thread
/// modules with the loom implementations
#[cfg(loom)]
mod loom {
	use loom::thread;
	pub use thread::current;
	pub use thread::park;
	pub use thread::spawn;
	pub use thread::yield_now;
	pub use thread::AccessError;
	pub use thread::Builder;
	pub use thread::JoinHandle;
	pub use thread::LocalKey;
	pub use thread::Thread;
	pub use thread::ThreadId;
}
