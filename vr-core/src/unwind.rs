//! Module for dealing with unwinding, especisally unwind safety for unsafe code

use core::{any::Any, panic::AssertUnwindSafe};
use std::{
	panic::{catch_unwind, resume_unwind},
	process::abort,
	thread,
};

use log::error;

/// Catches an unwind if the function panics, assering that the function is
/// unwind safe as well.
pub fn halt_unwinding<F, T>(function: F) -> thread::Result<T>
where
	F: FnOnce() -> T,
{
	catch_unwind(AssertUnwindSafe(function))
}

/// resume unwinding with the panic payload
pub fn resume_unwinding(payload: Box<dyn Any + Send>) -> ! {
	resume_unwind(payload)
}

/// If this type is dropped, the thread will be aborted
pub struct AbortIfPanic {
	/// The log target that should be used for the log message
	pub target: &'static str,
}

impl Drop for AbortIfPanic {
	#[track_caller]
	fn drop(&mut self) {
		error!(target: self.target, "Detected unexpected panic; aborting");
		abort()
	}
}
