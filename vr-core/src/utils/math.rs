//! Module containing utilities related to math

use core::{
	ops::{Range, RangeBounds},
	sync::atomic::{AtomicUsize, Ordering},
};

/// Normalise arbitrary [`RangeBounds`] to a [`Range`]
///
/// # Panics
///
/// Panics if
/// - the `start` of the range is greater than its end (in debug)
/// - if the `start` or `end` are greater than `max`, for inclusive ranges, if they
/// are greater equal to `max`.
pub fn simplify_range<R>(range: R, max: usize) -> Range<usize>
where
	R: RangeBounds<usize>,
{
	let start = match range.start_bound() {
		core::ops::Bound::Unbounded => 0,
		core::ops::Bound::Included(&i) if i <= max => i,
		core::ops::Bound::Excluded(&i) if i < max => i + 1,
		bound => panic!("range start {bound:?} should be <= max {max}"),
	};

	let end = match range.end_bound() {
		core::ops::Bound::Unbounded => max,
		core::ops::Bound::Excluded(&i) if i <= max => i,
		core::ops::Bound::Included(&i) if i < max => i + 1,
		bound => panic!("range end {bound:?} should be <= max {max}"),
	};

	debug_assert!(
		start <= end,
		"range start {start:?} should be <= range end {end:?}"
	);
	start..end
}

/// Decrement an [`AtomicUsize`] and check whether it would overflow.
///
/// This functionr return [`None`] if an overflow would occur.
pub fn checked_decrement(u: &AtomicUsize) -> Option<usize> {
	u.fetch_update(Ordering::Relaxed, Ordering::Relaxed, |u| u.checked_sub(1)).ok()
}
