//! A module containing a [`Send`]-able raw pointer.

/// A raw pointer that can be share among threads. It is possible to do this
/// withouth any unsafe code by converting pointers to usize or to
/// [`AtomicPtr<T>`](crate::sync::atomic::AtomicPtr<T>) then back to a raw pointer
/// for use. This approach is preferred becase code that uses this type is
/// more explicit.
///
/// Unsafe code is still required to dereference the pointer, so this type is
/// not unsound on its own, although it does partly lift the uncoditional
/// not [`Send`] and not [`Sync`] on raw pointers. As always, dereference with care.
#[derive()]
pub struct SendPtr<T> {
	/// The underlying ptr
	ptr: *mut T,
}

// Safety: !Send for raw pointers is not for safety, just a lint
unsafe impl<T> Send for SendPtr<T> where T: Send {}
// Safety: !Sync for raw pointers is not for safety, just a lint
unsafe impl<T> Sync for SendPtr<T> where T: Send {}

impl<T> SendPtr<T> {
	/// Create a new [`SendPtr`] from a `*mut T`.
	pub fn new(ptr: *mut T) -> Self {
		Self { ptr }
	}

	/// Get the underlying raw pointer.
	pub fn get(self) -> *mut T {
		self.ptr
	}
}

impl<T> Clone for SendPtr<T> {
	fn clone(&self) -> Self {
		*self
	}
}

impl<T> Copy for SendPtr<T> {}
