//! Basic threadpool benchmarks

#![allow(missing_docs)]
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use vr_threading::{handle::DefaultHandle, threadpool::ThreadPoolBuilder, ThreadPoolHandle};

/// Calculate the nth fibonacci number with the vr-threading-Threadpool
fn fibonacci_self(n: u64) -> u64 {
	if n < 2 {
		return n;
	}

	let (a, b) = DefaultHandle::join(|| fibonacci_self(n - 2), || fibonacci_self(n - 1));
	a + b
}

/// Calculate the nth fibonacci number single threaded
fn fibonacci_single(n: u64) -> u64 {
	if n < 2 {
		return n;
	}

	fibonacci_single(n - 2) + fibonacci_single(n - 1)
}

/// Calculate the nth fibonacci number with rayon
fn fibonacci_rayon(n: u64) -> u64 {
	if n < 2 {
		return n;
	}

	let (a, b) = rayon::join(|| fibonacci_rayon(n - 2), || fibonacci_rayon(n - 1));
	a + b
}

/// Benchmarking function for [`fibonacci_self`]
fn fibonacci(c: &mut Criterion) {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut group = c.benchmark_group("Fibonacci(32)");
			group.bench_function("vr-threading", |b| b.iter(|| fibonacci_self(black_box(32))));
			group.bench_function("single threaded", |b| {
				b.iter(|| fibonacci_single(black_box(32)))
			});

			group.bench_function("rayon", |b| b.iter(|| fibonacci_rayon(black_box(32))));
			group.finish()
		})
		.unwrap();
}

criterion_group!(benches, fibonacci,);
criterion_main!(benches);
