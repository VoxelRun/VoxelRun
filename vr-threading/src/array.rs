//! Module containing the [`IntoParallelIterator`] implementation for arrays.

use core::{marker::PhantomData, mem::ManuallyDrop};

use crate::{
	iter::{plumbing::bridge, IndexedParallelIterator, IntoParallelIterator, ParallelIterator},
	slice::{Iter, IterMut},
	vec::DrainProducer,
	ThreadPoolHandle,
};

/// A [`ParallelIterator`] over an array consuming it.
pub struct IntoIter<T, const N: usize, H>
where
	H: ThreadPoolHandle,
{
	/// The underlying array.
	array: [T; N],
	/// This iterator's [`ThreadPoolHandle`]
	_handle: PhantomData<H>,
}

impl<T, const N: usize, H> ParallelIterator<H> for IntoIter<T, N, H>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(N)
	}
}

impl<T, const N: usize, H> IndexedParallelIterator<H> for IntoIter<T, N, H>
where
	H: ThreadPoolHandle,
	T: Send,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		N
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		// Safety: Drain all items, then an empty array will fall out of scope.
		unsafe {
			let mut array = ManuallyDrop::new(self.array);
			let producer = DrainProducer::new(array.as_mut_slice());
			callback.callback(producer)
		}
	}
}

impl<T, H, const N: usize> IntoParallelIterator<H> for [T; N]
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;
	type Iter = IntoIter<T, N, H>;

	fn into_par_iter(self) -> Self::Iter {
		IntoIter {
			array: self,
			_handle: PhantomData,
		}
	}
}

impl<'data, T, H, const N: usize> IntoParallelIterator<H> for &'data [T; N]
where
	H: ThreadPoolHandle,
	T: Sync + 'data,
{
	type Item = &'data T;
	type Iter = Iter<'data, T, H>;

	fn into_par_iter(self) -> Self::Iter {
		<&[T] as IntoParallelIterator<H>>::into_par_iter(self)
	}
}

impl<'data, T, H, const N: usize> IntoParallelIterator<H> for &'data mut [T; N]
where
	H: ThreadPoolHandle,
	T: Send + 'data,
{
	type Item = &'data mut T;
	type Iter = IterMut<'data, T, H>;

	fn into_par_iter(self) -> Self::Iter {
		<&mut [T] as IntoParallelIterator<H>>::into_par_iter(self)
	}
}
