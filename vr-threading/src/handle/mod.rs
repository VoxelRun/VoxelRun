//! The default handles to standard a ThreadPool from this crate, Created using
//! a [`ThreadPoolBuilder`](crate::threadpool::ThreadPoolBuilder).
//! The [`DefaultHandle`] zero sized type to interact with the threadpool.
//!
//! The [`DefaultHandle`] is an implementor of [`ThreadPoolHandle`] meaning it
//! provides the following functions:
//!
//! - [`join`](DefaultHandle::join)
//! - [`join_context`](DefaultHandle::join_context)
//! - [`task`](DefaultHandle::task)
//!
//! Additionally, it provides a scope API, that makes it possible to create
//! fork-join blocks, with tasks that will be executed before the scope ends.
//! This is possible with the [`scope`](DefaultHandle::scope)-API.
//!
//! If any of the functions in [`DefaultHandle`] are called outside of a
//! threadpool created with [`ThreadPoolBuilder`](crate::threadpool::ThreadPoolBuilder),
//! they will panic.

use crate::{
	registry::{scope::Scope, WorkerThread},
	Context, ThreadPoolHandle,
};

/// A handle to the default thread pool.
#[derive(Clone, Copy)]
pub struct DefaultHandle;

impl ThreadPoolHandle for DefaultHandle {
	/// Takes two closures and _potentially_ runs them in parallel. It returns
	/// a pair of the results from those closures.
	///
	/// Conceptually, [`join`](DefaultHandle::join) is similar to spawning two
	/// threads, one executing each of the closures. However, the implementation
	/// is quite different and incurs very low overhead. The underyling technique
	/// is called "work stealing": The ThreadPool uses a fixed pool of worker
	/// threads and attempts to only execute code in parallel when there are
	/// idle CPUs to handle it.
	///
	///
	/// # Examples
	///
	/// This example uses join to perform a quick-sort (not this is not
	/// a particularly optimized implementation).
	/// ```
	/// use vr_threading::{handle::DefaultHandle, ThreadPoolHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let mut v = vec![1, 2, 5, 3, 4];
	///     quick_sort(&mut v);
	///     assert_eq!(v, vec![1, 2,3, 4, 5]);
	/// }).unwrap();
	///
	///
	/// fn partition<T>(v: &mut [T]) -> usize
	/// where
	///     T: PartialOrd + Send
	/// {
	///     let pivot = v.len() -  1;
	///     let mut i = 0;
	///     for j in 0..pivot {
	///         if v[j] < v[pivot] {
	///              v.swap(i, j);
	///              i += 1;
	///         }
	///     }
	///     v.swap(i, pivot);
	///     i
	/// }
	///
	/// fn quick_sort<T>(v: &mut [T])
	/// where
	///     T: PartialOrd + Send
	/// {
	///     if v.len() > 1 {
	///          let mid = partition(v);
	///          let (lo, hi) = v.split_at_mut(mid);
	///          DefaultHandle::join(|| quick_sort(lo), || quick_sort(hi));
	///     }
	/// }
	/// ```
	///
	/// # Warning about I/O
	///
	/// The assumption is that closures given to [`join`](DefaultHandle::join)
	/// are CPU-bound tasks that do not perform I/O or other blocking operations.
	/// If you do perform I/O, and that I/O should block (e. g. waiting for a
	/// network reequest), the overall performance may be poor. Moreover, if
	/// you cause one closure to be blocked waiting on another (e. g. using
	/// a channel), that could lead to a deadlock.
	///
	/// # Panics
	///
	/// No matter what happes, both closures will always be executed. If a
	/// single closure panics, whether it be the first or the second closure,
	/// that panic will be propagated and hence [`join`](DefaultHandle::join)
	/// will panic with the same panic value. If both closures panic,
	/// [`join`](DefaultHandle::join) will panic with the panic payload from
	/// the first closure.
	///
	/// This will also panic if not executed in a threadpool constructed with
	/// [`ThreadPoolBuilder::spawn`](crate::threadpool::ThreadPoolBuilder)
	fn join<F1, F2, T1, T2>(task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce() -> T1,
		T1: Send,
		F2: Send + FnOnce() -> T2,
		T2: Send,
	{
		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"<DefaultHandle as ThreadPoolHandle>::join was executed outside a threadpool"
		);

		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };
		current.join(task1, task2)
	}

	/// Identical to [`join`](DefaultHandle::join), except that the closures
	/// have a parameter that provides context for the way the closure has
	/// been called, especially indicating whether they're executing on a
	/// different thread than where [`join_context`](DefaultHandle::join_context)
	/// was called. This will occur if the second job is stolen by a different
	/// thread.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{threadpool, handle::DefaultHandle, ThreadPoolHandle, Context};
	/// use std::sync::Barrier;
	/// use core::num::NonZeroUsize;
	///
	/// threadpool::ThreadPoolBuilder::new()
	///     .num_threads(NonZeroUsize::new(2).unwrap())
	///     .spawn(|| {
	///         let barrier = Barrier::new(2);
	///         let (a_migrated, b_migrated) = DefaultHandle::join_context(
	///             |a| {
	///                 barrier.wait();
	///                 a.migrated()
	///             },
	///             |b| {
	///                 barrier.wait();
	///                 b.migrated()
	///             },
	///         );
	///
	///         assert!(!a_migrated); // closure a was executed on the current thread
	///         assert!(b_migrated); // closure b was stolen by another thread
	///     })
	///     .unwrap();
	/// ```
	fn join_context<F1, F2, T1, T2>(task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce(Context) -> T1,
		F2: Send + FnOnce(Context) -> T2,
		T2: Send,
		T1: Send,
	{
		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"<DefaultHandle as ThreadPoolHandle>::join_context was executed outside a threadpool"
		);

		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };
		current.join_context(task1, task2)
	}

	/// Puts the task into the Threadpool's job queue or in the "static" or
	/// "global" queue, if the job queue is full. Just like a standard thread,
	/// this task is not tied to the current stack frame, and hence it cannot
	/// hold any references other than those with a `'static` lifetime. If you
	/// want to create a task that references stack data, use the
	/// [`scope`](DefaultHandle::scope) function to create a scope.
	///
	/// Since tasks created with this function cannot hold references into the
	/// enclosing stack frame, you almost certainly want to use a `move` closure
	/// as their argument (otherwise, the closure will typically hold references
	/// to any variables from the enclosing function that you happen to use).
	///
	/// This API is no guaranteed order of execution, given that other threads
	/// may steal tasks at any time. However, they are generally prioritized
	/// in LIFO order on the thread from which they were spawned. Other threads
	/// always steal from the end of the deque, like FIFO order. The idea that
	/// "recent" tasks are most likely to be fresh in the local CPU's cache,
	/// while other threads can steal older "stale" tasks.
	///
	/// The Threadpool will **always** outlive the task.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{threadpool, ThreadPoolHandle, handle::DefaultHandle};
	/// use std::sync::atomic::{AtomicUsize, Ordering};
	///
	/// static GLOBAL_COUNTER: AtomicUsize = AtomicUsize::new(0);
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     GLOBAL_COUNTER.fetch_add(1, Ordering::SeqCst);
	/// }).unwrap();
	///
	/// assert_eq!(1, GLOBAL_COUNTER.load(Ordering::SeqCst));
	/// ```
	/// # Panic handling
	///
	/// If the closure should panic, the resulting panic will be prepagated to
	/// the panic handler registered in the
	/// [`ThreadPoolBuilder`](crate::threadpool::ThreadPoolBuilder), if any.
	/// See [`ThreadPoolBuilder::panic_handler`](crate::threadpool::ThreadPoolBuilder::panic_handler)
	/// for more details.
	///
	/// # Panics
	///
	/// This panics if not executed in a threadpool constructed with
	/// [`ThreadPoolBuilder::spawn`](crate::threadpool::ThreadPoolBuilder)
	fn task<F>(task: F)
	where
		F: 'static + FnOnce() + Send,
	{
		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"<DefaultHandle as ThreadPoolHandle>::task was executed outside a threadpool"
		);

		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };
		current.task(task);
	}

	fn current_num_threads() -> core::num::NonZeroUsize {
		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"<DefaultHandle as ThreadPoolHandle>::task was executed outside a threadpool"
		);
		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };

		current.registry().num_threads()
	}
}

impl DefaultHandle {
	/// Creates a new "fork-join" scope `s` and invokes the closure with a
	/// references to `s`. This closure can then spawn asynchronous tasks into
	/// `s`. Those tasks may run asynchronously with respect to the closure;
	/// they may themeselves spawn additional tasks into `s`. When the closure
	/// returns, it will block until all tasks that hve been spawned into `s`
	/// complete.
	///
	/// [`scope`](DefaultHandle::scope) is a more flexible building block
	/// compared to [`join`](DefaultHandle::join), since a loop can be used
	/// to spawn any number of tasks without recursing. However, that flexibility
	/// comes at a performance price: Tasks spawned using
	/// [`scope`](DefaultHandle::scope) must be allocated on the heap, whereas
	/// [`join`](DefaultHandle::join) can make exclusive use of the stack.
	/// **Prefer [`join`](DefaultHandle::join) where possible**.
	///
	/// # Example
	///
	/// The [`join`](DefaultHandle::join) function launches two closures and waits
	/// for them to stop. One could implement [`join`](DefaultHandle::join) using
	/// a scope like so, although it would be less efficient than the real
	/// implementation.
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool, ThreadPoolHandle};
	///
	/// pub fn join<F1, F2, T1, T2>(task1: F1, task2: F2) -> (T1, T2)
	/// where
	///     F1: Send + FnOnce() -> T1,
	///     F2: Send + FnOnce() -> T2,
	///     T1: Send,
	///     T2: Send,
	/// {
	///     let mut result_1 = None;
	///     let result_2 = DefaultHandle::scope(|s| {
	///         s.spawn(|_| result_1 = Some(task1()));
	///         Some(task2())
	///     });
	///     (result_1.unwrap(), result_2.unwrap())
	/// }
	///
	/// // Quicksort with this join implementation
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let mut v = vec![1, 2, 5, 3, 4];
	///     quick_sort(&mut v);
	///     assert_eq!(v, vec![1, 2,3, 4, 5]);
	/// }).unwrap();
	///
	/// fn partition<T>(v: &mut [T]) -> usize
	/// where
	///     T: PartialOrd + Send
	/// {
	///     let pivot = v.len() -  1;
	///     let mut i = 0;
	///     for j in 0..pivot {
	///         if v[j] < v[pivot] {
	///              v.swap(i, j);
	///              i += 1;
	///         }
	///     }
	///     v.swap(i, pivot);
	///     i
	/// }
	///
	/// fn quick_sort<T>(v: &mut [T])
	/// where
	///     T: PartialOrd + Send
	/// {
	///     if v.len() > 1 {
	///          let mid = partition(v);
	///          let (lo, hi) = v.split_at_mut(mid);
	///          join(|| quick_sort(lo), || quick_sort(hi));
	///     }
	/// }
	/// ```
	/// # Note on threading  
	///
	/// The closure given to [`sope`](DefaultHandle::scope) executes in the
	/// threadpool, as do those given to [`spawn`](Scope::spawn). This means
	/// that you can't access thread-local variables (well, you can, but they
	/// may be different from what you expect).
	///
	/// # Task execution
	///
	/// Task execution potentially starts as soon as [`spawn`](Scope::spawn) is
	/// called. The task will end sometime before [`scope`](DefaultHandle::scope)
	/// returns. Note that the _closure_ given to scope may return much earlier.
	/// In general the lifetime of a scope created like [`scope(body)`] goes
	/// something like this:
	///
	/// 1. Scope begins when `scope(body)` is called
	/// 2. Scope body `body()` is invoked
	///     - Scope tasks may be spawned and are _potentially_ executed
	/// 3. Scope body returns
	/// 4. Scope tasks execute, possibly spawning more tasks, this may be concurrent to
	/// 5. Once all tasks are done, scope ands and `scope` returns
	///
	/// To see how and when tasks are joined, consider this example
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     // point start
	///     DefaultHandle::scope(|s| {
	///         s.spawn(|s| {                          // task s.1
	///             s.spawn(|s| {                      // task s.1.1
	///                 DefaultHandle::scope(|t| {
	///                     t.spawn(|_| ());           // task t.1
	///                     t.spawn(|_| ());           // task t.2
	///                 });
	///             });
	///         });
	///         s.spawn(|s| {});                       // task s.2
	///         // point mid
	///     });
	///     // point end
	/// }).unwrap();
	/// ```
	///
	/// The various tasks that are run will execute roughly like so:
	/// ```norust
	/// | (start)
	/// |
	/// | (scope `s` created)
	/// +-----------------------------------------------+ (task s.2)
	/// +-------+ (task s.1)                            |
	/// |       |                                       |
	/// |       +---+ (task s.1.1)                      |
	/// |       |   |                                   |
	/// |       |   | (scope `t` created)               |
	/// |       |   +----------------+ (task t.2)       |
	/// |       |   +---+ (task t.1) |                  |
	/// | (mid) |   |   |            |                  |
	/// :       |   |<--+------------+ (scope `t` ends) |
	/// :       |   |                                   |
	/// | <-----+---+-----------------------------------+ (scope `s` ends)
	/// |
	/// | (end)
	/// ```
	///
	/// The point here is that everything spawned into the scope `s` will terminate
	/// (at latest) at the same point — right before the original call to
	/// [`scope`](DefaultHandle::scope) returns. This includes new subtasks
	/// (e. g., task `s.1.1`). If a new scope is created (such as `t`), the
	/// things spawned into that scope will be joined before that scope returns,
	/// which in turns occurs before the creating task (task `s.1.1` in this case)
	/// finishes.
	///
	/// There is no guaranteed order of execution for spawns in a scope, given
	/// that other threads mayh steal tasks at any time. However, they are
	/// generally prioritized in a LIFO order on the thread from which they
	/// were spawned. So in this example, absent any stealing, we can expect
	/// `s.2` to execute before `s.1`, and `t.2` before `t.1`. Other threads
	/// always steal from other ends of the deque, like FIFO order.  The idea
	/// is that "recent" tasks are most likely to be fresh in the local CPU's
	/// cache, while other threads can steal older “stale” tasks.
	///
	/// # Accessing Stack Data
	///
	/// In general, spawned tasks may access stack data in place that outlives
	/// the scope itself. Other data must be fully owned by the spawned task.
	///
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let ok = vec![1, 2, 3];
	///     DefaultHandle::scope(|s| {
	///         let bad = vec![3, 2 ,1];
	///         s.spawn(|_| {
	///             println!("{:?}", ok); // => `ok` is accessable as it outlives the scope `s`
	///             // println!("{:?}", bad); // => compilation error, as spawn may outlive bad
	///         })
	///     })
	/// }).unwrap()
	/// ```
	///
	/// As the comments example above suggests, to reference `bad` we must take
	/// ownership of it. One way to to this is to deatch the closure from the
	/// surrounding stack frame, using the `move` keyword. This will cause it
	/// to take ownership of _all_ variables it touches, in this case this
	/// includes `ok` _and_ `bad`.
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let ok = vec![1, 2, 3];
	///     DefaultHandle::scope(|s| {
	///         let bad = vec![3, 2 ,1];
	///         s.spawn(move |_| {
	///             println!("{:?}", ok); // => `ok` is accessable as it outlives the scope `s`
	///             println!("{:?}", bad); // => compilation error, spawn may outlive bad
	///         });
	///         // the closure is fine, but now we can't use ok anywhere else,
	///         // since the previous task took ownership of it
	///         // s.spawn(|_| println!("{:?}", ok));
	///     })
	/// }).unwrap()
	/// ```
	/// While this works, it could be a problem if we want to use `ok` elsewhere.
	/// There are two choices. We keep the closure as a `move` closure, but
	/// instead of referencing the variable `ok`, we create a shadowed variable
	/// that is a borrow of `ok` and capture that.
	///
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let ok = vec![1, 2, 3];
	///     DefaultHandle::scope(|s| {
	///         let bad = vec![3, 2 ,1];
	///         let ok: &Vec<_> = &ok;
	///         s.spawn(move |_| {
	///             println!("{:?}", ok); // => `ok` is accessable as it outlives the scope `s`
	///             println!("{:?}", bad);
	///         });
	///         s.spawn(move |_| println!("{:?}", ok));
	///     })
	/// }).unwrap()
	/// ```
	///
	/// Another option is to not use the `move` keyword and instead to take ownership
	/// of individual variables
	///
	/// ```
	/// use vr_threading::{handle::DefaultHandle, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let ok = vec![1, 2, 3];
	///     DefaultHandle::scope(|s| {
	///         let bad = vec![3, 2 ,1];
	///         s.spawn(|_| {
	///             let bad = bad;
	///             println!("{:?}", ok); // => `ok` is accessable as it outlives the scope `s`
	///             println!("{:?}", bad);
	///         });
	///         s.spawn(|_| println!("{:?}", ok));
	///     })
	/// }).unwrap()
	/// ```
	///
	/// # Panics
	///
	/// If a panic occurs, either in the closure given to
	/// [`scope`](DefaultHandle::scope) or in any of the spawned jobs, that panic
	/// will be propagated and the call to [`scope`](DefaultHandle::scope) will
	/// panic. If multiple panics occur, it is non-deterministic which of their
	/// panic values will propagate. Regardless, once a task is spawned using
	/// `scope.spawn`, it will execute, even if the spawning task should later
	/// panic. `scope()` returns once all spawned jobs have completed, and any
	/// panics are propagated at that point.
	///
	/// This will also panic if not executed in a threadpool constructed with
	/// [`ThreadPoolBuilder::spawn`](crate::threadpool::ThreadPoolBuilder)
	pub fn scope<'scope, F, T>(task: F) -> T
	where
		F: FnOnce(&Scope<'scope>) -> T,
		T: Send,
	{
		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"<DefaultHandle as ThreadPoolHandle>::task was executed outside a threadpool"
		);

		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };
		current.scope(task)
	}
}
