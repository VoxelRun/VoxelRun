//! Where the [`Chain`] adaptor is contained

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, Reducer, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};
use core::marker::PhantomData;

/// An iterator that links two iterators together, in a chain.
///
/// This `struct` is created by [`ParallelIterator::chain`] See its documentation
/// for more.
#[derive(Debug)]
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Chain<A, B, H>
where
	H: ThreadPoolHandle,
{
	/// The first half of the iterator
	iter_a: A,
	/// The second half of the joined iterator
	iter_b: B,
	/// The handle to the threadpool
	_handle: PhantomData<H>,
}

impl<A, B, H> Chain<A, B, H>
where
	A: ParallelIterator<H>,
	B: ParallelIterator<H, Item = A::Item>,
	H: ThreadPoolHandle,
{
	/// Create a new [`Chain`] which joins `A` and `B`.
	pub fn new(iter_a: A, iter_b: B) -> Self {
		Self {
			iter_a,
			iter_b,
			_handle: PhantomData,
		}
	}
}

impl<A, B, H> ParallelIterator<H> for Chain<A, B, H>
where
	A: ParallelIterator<H>,
	B: ParallelIterator<H, Item = A::Item>,
	H: ThreadPoolHandle,
{
	type Item = A::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let Chain { iter_a, iter_b, .. } = self;
		let (left, right, reducer) = if let Some(len) = iter_a.opt_len() {
			consumer.split_at(len)
		} else {
			let reducer = consumer.to_reducer();
			(consumer.split_off_left(), consumer, reducer)
		};

		let (a, b) = H::join(
			|| iter_a.drive_unindexed(left),
			|| iter_b.drive_unindexed(right),
		);
		reducer.reduce(a, b)
	}

	fn opt_len(&self) -> Option<usize> {
		self.iter_a.opt_len()?.checked_add(self.iter_b.opt_len()?)
	}
}

impl<A, B, H> IntoParallelIterator<H> for Chain<A, B, H>
where
	A: ParallelIterator<H>,
	B: ParallelIterator<H, Item = A::Item>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<A, B, H> IndexedParallelIterator<H> for Chain<A, B, H>
where
	H: ThreadPoolHandle,
	A: IndexedParallelIterator<H>,
	B: IndexedParallelIterator<H, Item = A::Item>,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let Chain { iter_a, iter_b, .. } = self;
		let (left, right, reducer) = consumer.split_at(iter_a.len());
		let (a, b) = H::join(|| iter_a.drive(left), || iter_b.drive(right));
		reducer.reduce(a, b)
	}

	fn len(&self) -> usize {
		self.iter_a
			.len()
			.checked_add(self.iter_b.len())
			.expect("overflow")
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		let a_len = self.iter_a.len();
		return self.iter_a.with_producer(CallbackA {
			callback,
			a_len,
			b: self.iter_b,
		});

		/// The callback invoked by iter a
		struct CallbackA<CB, B> {
			/// The previous callback
			callback: CB,
			/// The length of a
			a_len: usize,
			/// the iterator b
			b: B,
		}

		/// The callback invoked by iter b
		struct CallbackB<CB, A> {
			/// The previous callback
			callback: CB,
			/// the length of a
			a_len: usize,
			/// the iterator a
			a: A,
		}

		impl<CB, B, H> ProducerCallback<B::Item, H> for CallbackA<CB, B>
		where
			B: IndexedParallelIterator<H>,
			CB: ProducerCallback<B::Item, H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = B::Item>,
			{
				self.b.with_producer(CallbackB {
					callback: self.callback,
					a_len: self.a_len,
					a: producer,
				})
			}
		}

		impl<CB, A, H> ProducerCallback<A::Item, H> for CallbackB<CB, A>
		where
			A: Producer<H>,
			CB: ProducerCallback<A::Item, H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = A::Item>,
			{
				let producer = ChainProducer::new(self.a_len, self.a, producer);
				self.callback.callback(producer)
			}
		}
	}
}

/// A [`Producer`] for [`Chain`]
struct ChainProducer<A, B, H>
where
	H: ThreadPoolHandle,
{
	/// The length of the first half
	a_len: usize,
	/// The first producer
	producer_a: A,
	/// The second producer
	producer_b: B,
	/// The [`ThreadPoolHandle`]
	_handle: PhantomData<H>,
}

impl<A, B, H> ChainProducer<A, B, H>
where
	A: Producer<H>,
	B: Producer<H, Item = A::Item>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`ChainProducer<A, B, H>`].
	fn new(a_len: usize, producer_a: A, producer_b: B) -> Self {
		Self {
			a_len,
			producer_a,
			producer_b,
			_handle: PhantomData,
		}
	}
}

impl<A, B, H> Producer<H> for ChainProducer<A, B, H>
where
	A: Producer<H>,
	B: Producer<H, Item = A::Item>,
	H: ThreadPoolHandle,
{
	type Item = A::Item;

	type IntoIter = ChainSeq<A::IntoIter, B::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		if index <= self.a_len {
			let a_remaining = self.a_len - index;
			let (a_left, a_right) = self.producer_a.split_at(index);
			let (b_left, b_right) = self.producer_b.split_at(0);
			(
				ChainProducer::new(self.a_len, a_left, b_left),
				ChainProducer::new(a_remaining, a_right, b_right),
			)
		} else {
			let (a_left, a_right) = self.producer_a.split_at(index);
			let (b_left, b_right) = self.producer_b.split_at(index - self.a_len);
			(
				ChainProducer::new(self.a_len, a_left, b_left),
				ChainProducer::new(0, a_right, b_right),
			)
		}
	}

	fn into_iter(self) -> Self::IntoIter {
		ChainSeq::new(self.producer_a.into_iter(), self.producer_b.into_iter())
	}

	fn min_len(&self) -> usize {
		Ord::max(self.producer_a.min_len(), self.producer_b.min_len())
	}

	fn max_len(&self) -> usize {
		Ord::min(self.producer_a.max_len(), self.producer_b.max_len())
	}

	fn fold_with<F>(self, mut folder: F) -> F
	where
		F: Folder<Self::Item>,
	{
		folder = self.producer_a.fold_with(folder);
		if folder.full() {
			folder
		} else {
			self.producer_b.fold_with(folder)
		}
	}
}

/// Wrapper for [`Chain`](core::iter::Chain) to implement [`ExactSizeIterator`]
struct ChainSeq<A, B> {
	/// The underlying standard chain
	chain: core::iter::Chain<A, B>,
}

impl<A, B> ChainSeq<A, B>
where
	A: ExactSizeIterator,
	B: ExactSizeIterator<Item = A::Item>,
{
	/// Creates a new [`ChainSeq<A, B>`]. Look at [`Iterator::chain`] for more
	/// details
	fn new(iter_a: A, iter_b: B) -> Self {
		Self {
			chain: iter_a.chain(iter_b),
		}
	}
}

impl<A, B> Iterator for ChainSeq<A, B>
where
	A: Iterator,
	B: Iterator<Item = A::Item>,
{
	type Item = A::Item;

	fn next(&mut self) -> Option<Self::Item> {
		self.chain.next()
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.chain.size_hint()
	}
}

impl<A, B> ExactSizeIterator for ChainSeq<A, B>
where
	A: ExactSizeIterator,
	B: ExactSizeIterator<Item = A::Item>,
{
}

impl<A, B> DoubleEndedIterator for ChainSeq<A, B>
where
	A: DoubleEndedIterator,
	B: DoubleEndedIterator<Item = A::Item>,
{
	fn next_back(&mut self) -> Option<Self::Item> {
		self.chain.next_back()
	}
}
