//! Module in which the [`Cloned`] adaptor is contained
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Cloned`] is an iterator that copies the elements of an underlying iterator.
///
/// This struct is created by the [`cloned`](ParallelIterator::cloned) method on
/// [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Cloned<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The iterator whose items are copied
	iter: I,
	/// Handle marker
	_handle: PhantomData<H>,
}

impl<I, H> Cloned<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Cloned<I, H>`].
	pub fn new(iter: I) -> Self {
		Self {
			iter,
			_handle: PhantomData,
		}
	}
}

impl<'a, T, H, I> ParallelIterator<H> for Cloned<I, H>
where
	I: ParallelIterator<H, Item = &'a T>,
	T: 'a + Clone + Send + Sync,
	H: ThreadPoolHandle,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = ClonedConsumer::new(consumer);
		self.iter.drive_unindexed(higher_order_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.iter.opt_len()
	}
}

impl<'a, T, I, H> IndexedParallelIterator<H> for Cloned<I, H>
where
	I: IndexedParallelIterator<H, Item = &'a T>,
	T: 'a + Clone + Send + Sync,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_order_consumer = ClonedConsumer::new(consumer);
		self.iter.drive(higher_order_consumer)
	}

	fn len(&self) -> usize {
		self.iter.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.iter.with_producer(Callback { base: callback });

		/// The [`ProducerCallback`] responsible for cloning new items
		struct Callback<CB> {
			/// The previous callback
			base: CB,
		}

		impl<'a, T, CB, H> ProducerCallback<&'a T, H> for Callback<CB>
		where
			CB: ProducerCallback<T, H>,
			T: 'a + Clone + Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = &'a T>,
			{
				let producer = ClonedProducer { base: producer };
				self.base.callback(producer)
			}
		}
	}
}

/// A [`Consumer`] that clones its items
struct ClonedConsumer<C> {
	/// The wrapped [`Consumer]
	base: C,
}

impl<C> ClonedConsumer<C> {
	/// Creates a new [`ClonedConsumer<C>`].
	fn new(base: C) -> Self {
		Self { base }
	}
}

impl<'a, T, C, H> Consumer<&'a T, H> for ClonedConsumer<C>
where
	C: Consumer<T, H>,
	T: 'a + Clone,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = ClonedFolder<C::Folder>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			ClonedConsumer::new(left),
			ClonedConsumer::new(right),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
		}
	}
}

impl<'a, T, C, H> UnindexedConsumer<&'a T, H> for ClonedConsumer<C>
where
	C: UnindexedConsumer<T, H>,
	T: 'a + Clone,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self {
			base: self.base.split_off_left(),
		}
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] that clones its elements
struct ClonedFolder<F> {
	/// The wrapped [`Folder`]
	base: F,
}

impl<'a, T, F> Folder<&'a T> for ClonedFolder<F>
where
	F: Folder<T>,
	T: 'a + Clone,
{
	type Result = F::Result;

	fn consume(self, item: &'a T) -> Self {
		ClonedFolder {
			base: self.base.consume(item.clone()),
		}
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = &'a T>,
	{
		self.base = self.base.consume_iter(iter.into_iter().cloned());
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

/// A [`Producer`] of cloned items.
struct ClonedProducer<P> {
	/// The wrapped [`Producer`]
	base: P,
}

impl<P> ClonedProducer<P> {
	/// Creates a new [`ClonedProducer<P>`].
	fn new(base: P) -> Self {
		Self { base }
	}
}

impl<'a, T, P, H> Producer<H> for ClonedProducer<P>
where
	P: Producer<H, Item = &'a T>,
	T: 'a + Clone,
	H: ThreadPoolHandle,
{
	type Item = T;

	type IntoIter = core::iter::Cloned<P::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(ClonedProducer::new(left), ClonedProducer::new(right))
	}

	fn into_iter(self) -> Self::IntoIter {
		self.base.into_iter().cloned()
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<F>(self, folder: F) -> F
	where
		F: Folder<Self::Item>,
	{
		self.base.fold_with(ClonedFolder { base: folder }).base
	}
}

impl<'a, I, H, T> IntoParallelIterator<H> for Cloned<I, H>
where
	I: ParallelIterator<H, Item = &'a T>,
	T: 'a + Sync + Send + Clone,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}
