//! Module in which the [`Copied`] adaptor is contained
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Copied`] is an iterator that copies the elements of an underlying iterator.
///
/// This struct is created by the [`copied`](ParallelIterator::copied) method on
/// [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Copied<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The iterator the items of which are copied
	iter: I,
	/// Handle marker
	_marker: PhantomData<H>,
}

impl<I, H> Copied<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Copied<I, H>`].
	pub fn new(iter: I) -> Self {
		Self {
			iter,
			_marker: PhantomData,
		}
	}
}

impl<'a, T, H, I> ParallelIterator<H> for Copied<I, H>
where
	I: ParallelIterator<H, Item = &'a T>,
	T: 'a + Copy + Send + Sync,
	H: ThreadPoolHandle,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = CopiedConsumer::new(consumer);
		self.iter.drive_unindexed(higher_order_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.iter.opt_len()
	}
}

impl<'a, T, H, I> IndexedParallelIterator<H> for Copied<I, H>
where
	I: IndexedParallelIterator<H, Item = &'a T>,
	T: 'a + Copy + Send + Sync,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_order_consumer = CopiedConsumer::new(consumer);
		self.iter.drive(higher_order_consumer)
	}

	fn len(&self) -> usize {
		self.iter.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.iter.with_producer(Callback { callback });

		/// The [`ProducerCallback`] responsible for copying new items
		struct Callback<CB> {
			/// The underlying callback
			callback: CB,
		}

		impl<'a, T, CB, H> ProducerCallback<&'a T, H> for Callback<CB>
		where
			CB: ProducerCallback<T, H>,
			T: 'a + Copy + Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = &'a T>,
			{
				let producer = CopiedProducer { base: producer };
				self.callback.callback(producer)
			}
		}
	}
}

/// A [`Producer`] of copied items.
struct CopiedProducer<P> {
	/// The underlying [`Producer`]
	base: P,
}

impl<P> CopiedProducer<P> {
	/// Creates a new [`CopiedProducer<P>`].
	fn new(base: P) -> Self {
		Self { base }
	}
}

impl<'a, T, P, H> Producer<H> for CopiedProducer<P>
where
	P: Producer<H, Item = &'a T>,
	T: 'a + Copy,
	H: ThreadPoolHandle,
{
	type Item = T;

	type IntoIter = core::iter::Copied<P::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(CopiedProducer::new(left), CopiedProducer::new(right))
	}

	fn into_iter(self) -> Self::IntoIter {
		self.base.into_iter().copied()
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<F>(self, folder: F) -> F
	where
		F: Folder<Self::Item>,
	{
		self.base.fold_with(CopiedFolder { base: folder }).base
	}
}

/// A [`Consumer`] that copies the contents before passing them to the base consumer
struct CopiedConsumer<C> {
	/// The base [`Consumer`] the items are being passed to.
	base: C,
}

impl<C> CopiedConsumer<C> {
	/// Creates a new [`CopiedConsumer<C>`].
	fn new(base: C) -> Self {
		Self { base }
	}
}

impl<'a, T, C, H> Consumer<&'a T, H> for CopiedConsumer<C>
where
	C: Consumer<T, H>,
	T: 'a + Copy,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = CopiedFolder<C::Folder>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			CopiedConsumer::new(left),
			CopiedConsumer::new(right),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		CopiedFolder {
			base: self.base.into_folder(),
		}
	}
}

impl<'a, T, C, H> UnindexedConsumer<&'a T, H> for CopiedConsumer<C>
where
	C: UnindexedConsumer<T, H>,
	T: 'a + Copy,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		CopiedConsumer::new(self.base.split_off_left())
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] that copies the elements and passes them to the base
struct CopiedFolder<F> {
	/// The base receiving the elements
	base: F,
}

impl<'a, T, F> Folder<&'a T> for CopiedFolder<F>
where
	F: Folder<T>,
	T: 'a + Copy,
{
	type Result = F::Result;

	fn consume(self, item: &'a T) -> Self {
		CopiedFolder {
			base: self.base.consume(*item),
		}
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = &'a T>,
	{
		self.base = self.base.consume_iter(iter.into_iter().copied());
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

impl<'a, I, H, T> IntoParallelIterator<H> for Copied<I, H>
where
	I: ParallelIterator<H, Item = &'a T>,
	T: 'a + Sync + Send + Copy,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}
