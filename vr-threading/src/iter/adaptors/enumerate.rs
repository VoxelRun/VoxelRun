//! Module containing the [`Enumerate`] [`IndexedParallelIterator`]-adaptor.
use core::{marker::PhantomData, ops::Range};

use crate::{
	iter::{
		plumbing::{bridge, Producer, ProducerCallback},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Enumerate`] is an iterator that returns the current count along with the
/// element. This struct is created by the [`enumerate`](IndexedParallelIterator::enumerate)
/// method on [`IndexedParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Enumerate<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The iterator being enumerated.
	base: I,
	/// A [`ThreadPoolHandle`] to the active pool.
	_handle: PhantomData<H>,
}

impl<I, H> Enumerate<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Enumerate`] parallel iterator adaptor.
	pub fn new(base: I) -> Self {
		Self {
			base,
			_handle: PhantomData,
		}
	}
}

impl<I, H> IntoParallelIterator<H> for Enumerate<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, H> ParallelIterator<H> for Enumerate<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = (usize, I::Item);

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.len())
	}
}

impl<I, H> IndexedParallelIterator<H> for Enumerate<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.base.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.base.with_producer(Callback { callback });

		/// A callback which adds an enumeration to the [`IndexedParallelIterator`]
		struct Callback<CB> {
			/// The previous callback which expects `(usize, I::Item)`
			callback: CB,
		}

		impl<CB, I, H> ProducerCallback<I, H> for Callback<CB>
		where
			CB: ProducerCallback<(usize, I), H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, base: P) -> Self::Output
			where
				P: Producer<H, Item = I>,
			{
				let producer = EnumerateProducer { base, offset: 0 };
				self.callback.callback(producer)
			}
		}
	}
}

// Since Enumerate can only work when we know the size of the iterator we only
// need to implement a producer callback.

/// A [`Producer`] which gives `(usize, P::Item)`. Where the first item of that
/// tuple reflects at which position the item is.
struct EnumerateProducer<P> {
	/// The base [`Producer`] being enumerated.
	base: P,
	/// The offset to the last split
	offset: usize,
}

impl<P, H> Producer<H> for EnumerateProducer<P>
where
	P: Producer<H>,
	H: ThreadPoolHandle,
{
	type Item = (usize, P::Item);

	type IntoIter = core::iter::Zip<Range<usize>, P::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(
			Self {
				base: left,
				offset: self.offset,
			},
			Self {
				base: right,
				offset: self.offset + index,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		let base = self.base.into_iter();
		let end = self.offset + base.len();
		(self.offset..end).zip(base)
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}
}
