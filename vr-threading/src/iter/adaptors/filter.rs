//! Module containing the [`Filter`] [`ParallelIterator`] adaptor.
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Filter`] takes a predicate `filter_op` and filters out elements that match.
///
/// This struct is created by the [`filter`](ParallelIterator::filter) method
/// on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Filter<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	/// The [`ParallelIterator`] being filtered
	pi: I,
	/// The predicate for items that shall remain
	filter_op: P,
	/// A [`ThreadPoolHandle`] to the executor.
	_handle: PhantomData<H>,
}

impl<I, P, H> Filter<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Filter<I, P, H>`] from a [`ParallelIterator`] and a
	/// predicate `filter_op`.
	pub fn new(pi: I, filter_op: P) -> Self {
		Self {
			pi,
			filter_op,
			_handle: PhantomData,
		}
	}
}

impl<I, P, H> IntoParallelIterator<H> for Filter<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, P, H> ParallelIterator<H> for Filter<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FilterConsumer::new(consumer, &self.filter_op);
		self.pi.drive_unindexed(higher_order_consumer)
	}
}

/// A [`Consumer`] that may remove items from an iterator.
struct FilterConsumer<'p, C, P> {
	/// The base consumer
	base: C,
	/// A reference to the filter operation
	filter_op: &'p P,
}

impl<'p, C, P> FilterConsumer<'p, C, P> {
	/// Creates a new [`FilterConsumer<C, P>`] from a [`Consumer`] and a predicate
	fn new(base: C, filter_op: &'p P) -> Self {
		Self { base, filter_op }
	}
}

impl<'p, T, C, P, H> Consumer<T, H> for FilterConsumer<'p, C, P>
where
	P: 'p,
	C: Consumer<T, H>,
	P: Fn(&T) -> bool + Sync,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FilterFolder<'p, C::Folder, P>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.filter_op),
			Self::new(right, self.filter_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			filter_op: self.filter_op,
		}
	}
}

impl<'p, T, C, P, H> UnindexedConsumer<T, H> for FilterConsumer<'p, C, P>
where
	P: 'p,
	C: UnindexedConsumer<T, H>,
	P: Fn(&T) -> bool + Sync,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.filter_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] that ignores elements which do not match a predicate.
struct FilterFolder<'p, F, P> {
	/// The base [`Folder`].
	base: F,
	/// The predicate which has to be matched in order for the item to be taken.
	filter_op: &'p P,
}

impl<'p, C, P, T> Folder<T> for FilterFolder<'p, C, P>
where
	C: Folder<T>,
	P: Fn(&T) -> bool + 'p,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		if (self.filter_op)(&item) {
			let base = self.base.consume(item);
			FilterFolder {
				base,
				filter_op: self.filter_op,
			}
		} else {
			self
		}
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
