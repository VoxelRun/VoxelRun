//! Module containing the [`FilterMap`] iterator adaptor.
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`FilterMap`] creates an iterator that uses `filter_map_op` to both filter
/// and map elements. This struct is created by the [`filter_map`](ParallelIterator::filter_map)
/// method on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct FilterMap<I, P, R, H>
where
	I: ParallelIterator<H>,
	P: Fn(I::Item) -> Option<R> + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	/// The [`ParallelIterator`] getting filter-mapped.
	pi: I,
	/// The operation to apply to every element
	filter_map_op: P,
	/// The [`ThreadPoolHandle`] to the executor.
	_handle: PhantomData<H>,
}

impl<I, P, R, H> FilterMap<I, P, R, H>
where
	I: ParallelIterator<H>,
	P: Fn(I::Item) -> Option<R> + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`FilterMap<I, P, R, H>`].
	pub fn new(pi: I, filter_map_op: P) -> Self {
		Self {
			pi,
			filter_map_op,
			_handle: PhantomData,
		}
	}
}

impl<I, P, R, H> IntoParallelIterator<H> for FilterMap<I, P, R, H>
where
	I: ParallelIterator<H>,
	P: Fn(I::Item) -> Option<R> + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}
impl<I, P, R, H> ParallelIterator<H> for FilterMap<I, P, R, H>
where
	I: ParallelIterator<H>,
	P: Fn(I::Item) -> Option<R> + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = R;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FilterMapConsumer::new(consumer, &self.filter_map_op);
		self.pi.drive_unindexed(higher_order_consumer)
	}
}

/// A [`Consumer`] that filters out elements and maps them to another value using
/// a predicate.
struct FilterMapConsumer<'p, C, P> {
	/// The base [`Consumer`].
	base: C,
	/// The operation being applied to map and filter.
	filter_op: &'p P,
}

impl<'p, C, P> FilterMapConsumer<'p, C, P> {
	/// Creates a new [`FilterMapConsumer`] from a base [`Consumer`] and a
	/// predicate.
	fn new(base: C, filter_op: &'p P) -> Self {
		Self { base, filter_op }
	}
}

impl<'p, T, R, C, P, H> Consumer<T, H> for FilterMapConsumer<'p, C, P>
where
	C: Consumer<R, H>,
	P: Fn(T) -> Option<R> + Sync + 'p,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FilterMapFolder<'p, C::Folder, P>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.filter_op),
			Self::new(right, self.filter_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			filter_op: self.filter_op,
		}
	}
}

impl<'p, T, R, C, P, H> UnindexedConsumer<T, H> for FilterMapConsumer<'p, C, P>
where
	C: UnindexedConsumer<R, H>,
	P: Fn(T) -> Option<R> + Sync + 'p,
	H: ThreadPoolHandle,
{
	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}

	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.filter_op)
	}
}

/// A [`Folder`] that filters out and maps elements according to a predicate.
struct FilterMapFolder<'p, F, P> {
	/// The base [`Folder`]
	base: F,
	/// The operation being applied to each element.
	filter_op: &'p P,
}

impl<'p, F, P, T, R> Folder<T> for FilterMapFolder<'p, F, P>
where
	F: Folder<R>,
	P: Fn(T) -> Option<R> + Sync + 'p,
{
	type Result = F::Result;

	fn consume(self, item: T) -> Self {
		if let Some(mapped) = (self.filter_op)(item) {
			let base = self.base.consume(mapped);
			FilterMapFolder {
				base,
				filter_op: self.filter_op,
			}
		} else {
			self
		}
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
