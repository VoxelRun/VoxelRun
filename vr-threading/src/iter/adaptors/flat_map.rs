//! Module in which the [`FlatMap`] adaptor is contained

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Reducer, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`FlatMap`] maps each element to an item that can be iterated in parallel,
/// then flattens these iterators together. This struct is created by the
/// [`flat_map`](ParallelIterator::flat_map) method on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct FlatMap<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The base [`ParallelIterator`] over elements which implement [`IntoParallelIterator`].
	base: I,
	/// The operation which takes in the items and turns them into [`ParallelIterator`]s.
	map_op: F,
	/// A handle to the underlying threadpool.
	_handle: PhantomData<H>,
}

impl<I, F, R, H> FlatMap<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`FlatMap<I, F, R, H>`] parallel iterator adaptor.
	pub fn new(base: I, map_op: F) -> Self {
		Self {
			base,
			map_op,
			_handle: PhantomData,
		}
	}
}

impl<I, F, R, H> ParallelIterator<H> for FlatMap<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = R::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FlatMapConsumer::new(consumer, &self.map_op);
		self.base.drive_unindexed(higher_order_consumer)
	}
}

impl<I, F, R, H> IntoParallelIterator<H> for FlatMap<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Consumer`] which expands [`IntoParallelIterator`] elements into a base.
struct FlatMapConsumer<'f, C, F> {
	/// The base [`Consumer`]
	base: C,
	/// The map which creates the [`IntoParallelIterator`]-elements.
	map_op: &'f F,
}

impl<'f, C, F> FlatMapConsumer<'f, C, F> {
	/// Creates a new [`FlatMapConsumer<C, F>`].
	///
	/// Expects `base` to be an [`UnindexedConsumer`] and `map_op` to be a function
	/// which takes an item and outputs a [`IntoParallelIterator`].
	fn new(base: C, map_op: &'f F) -> Self {
		Self { base, map_op }
	}
}

impl<'f, T, U, C, F, H> Consumer<T, H> for FlatMapConsumer<'f, C, F>
where
	C: UnindexedConsumer<U::Item, H>,
	F: Fn(T) -> U + Sync,
	U: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FlatMapFolder<'f, C, F, C::Result, H>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.map_op),
			Self::new(right, self.map_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base,
			map_op: self.map_op,
			previous: None,
			_handle: PhantomData,
		}
	}
}

impl<'f, T, U, C, F, H> UnindexedConsumer<T, H> for FlatMapConsumer<'f, C, F>
where
	C: UnindexedConsumer<U::Item, H>,
	F: Fn(T) -> U + Sync,
	U: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.map_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] which expands [`IntoParallelIterator`] elements and let them
/// be consumed by a base consumer.
struct FlatMapFolder<'f, C, F, R, H> {
	/// The base [`UnindexedConsumer`].
	base: C,
	/// The operation producing the [`IntoParallelIterator`] eleemnts.
	map_op: &'f F,
	/// The previous result.
	previous: Option<R>,
	/// handle to the threadpool.
	_handle: PhantomData<H>,
}

impl<'f, T, U, C, F, H> Folder<T> for FlatMapFolder<'f, C, F, C::Result, H>
where
	C: UnindexedConsumer<U::Item, H>,
	F: Fn(T) -> U + Sync,
	U: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		let par_iter = (self.map_op)(item).into_par_iter();
		let consumer = self.base.split_off_left();
		let result = par_iter.drive_unindexed(consumer);

		let previous = match self.previous {
			None => Some(result),
			Some(previous) => Some(self.base.to_reducer().reduce(previous, result)),
		};

		Self {
			base: self.base,
			map_op: self.map_op,
			previous,
			_handle: PhantomData,
		}
	}

	fn complete(self) -> Self::Result {
		match self.previous {
			Some(previous) => previous,
			None => self.base.into_folder().complete(),
		}
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
