//! Module in which the [`FlatMapIter`] adaptor is contained.

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`FlatMapIter`] maps each element to a an item convertable to a sequential
/// iterator, then flattens these iterators together. This struct is created by
/// [`flat_map_iter`](ParallelIterator::flat_map_iter) method on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct FlatMapIter<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoIterator,
	R::Item: Send,
	H: ThreadPoolHandle,
{
	/// The base [`ParallelIterator`]
	base: I,
	/// The operation creating the [`IntoIterator`]-elements
	map_op: F,
	/// The handle to the underlying threadpool.
	_handle: PhantomData<H>,
}

impl<I, F, R, H> FlatMapIter<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoIterator,
	R::Item: Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`FlatMapIter`] parallel iterator adaptor.
	pub fn new(base: I, map_op: F) -> Self {
		Self {
			base,
			map_op,
			_handle: PhantomData,
		}
	}
}

impl<I, F, R, H> ParallelIterator<H> for FlatMapIter<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoIterator,
	R::Item: Send,
	H: ThreadPoolHandle,
{
	type Item = R::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FlatMapIterConsumer::new(consumer, &self.map_op);
		self.base.drive_unindexed(higher_order_consumer)
	}
}

impl<I, F, R, H> IntoParallelIterator<H> for FlatMapIter<I, F, R, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: IntoIterator,
	R::Item: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// The [`Consumer`] consuming the serially expanded elements.
pub struct FlatMapIterConsumer<'f, C, F> {
	/// The base [`UnindexedConsumer`]
	base: C,
	/// The operation creating the [`IntoIterator`]s
	map_op: &'f F,
}

impl<'f, C, F> FlatMapIterConsumer<'f, C, F> {
	/// Creates a new [`FlatMapIterConsumer<C, F>`], where `C` is an [`UnindexedConsumer`]
	/// and `F` is a function.
	pub fn new(base: C, map_op: &'f F) -> Self {
		Self { base, map_op }
	}
}

impl<'f, T, U, C, F, H> Consumer<T, H> for FlatMapIterConsumer<'f, C, F>
where
	C: UnindexedConsumer<U::Item, H>,
	F: Fn(T) -> U + Sync,
	U: IntoIterator,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FlatMapIterFolder<'f, C::Folder, F>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.map_op),
			Self::new(right, self.map_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			map_op: self.map_op,
		}
	}
}

impl<'f, T, U, C, F, H> UnindexedConsumer<T, H> for FlatMapIterConsumer<'f, C, F>
where
	C: UnindexedConsumer<U::Item, H>,
	F: Fn(T) -> U + Sync,
	U: IntoIterator,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.map_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] folding [`IntoIterator`] elements into a base [`Folder`]
pub struct FlatMapIterFolder<'f, C, F> {
	/// The base [`Folder`]
	base: C,
	/// The operation creating the [`IntoIterator`] elements.
	map_op: &'f F,
}

impl<'f, T, U, C, F> Folder<T> for FlatMapIterFolder<'f, C, F>
where
	C: Folder<U::Item>,
	F: Fn(T) -> U + Sync,
	U: IntoIterator,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		Self {
			base: self.base.consume_iter((self.map_op)(item)),
			map_op: self.map_op,
		}
	}

	fn consume_iter<I>(self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		Self {
			base: self
				.base
				.consume_iter(iter.into_iter().flat_map(self.map_op)),
			map_op: self.map_op,
		}
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
