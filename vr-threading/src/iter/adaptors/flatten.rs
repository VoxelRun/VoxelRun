//! Module in which the [`Flatten`] adaptor is contained

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Reducer, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Flatten`] turns each element into a [`ParallelIterator`], then flattens
/// these iterators together. This struct is created by the
/// [`flatten`](ParallelIterator::flatten) method on a [`ParallelIterator`]
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Flatten<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The underlying parrallel iterator
	pi: I,
	/// The [`ThreadPoolHandle`] for this adaptor.
	_handle: PhantomData<H>,
}

impl<I, H> Flatten<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Flatten<I, H>`] from a [`ParallelIterator`] over
	/// elements which implement [`IntoParallelIterator`].
	pub fn new(pi: I) -> Self {
		Self {
			pi,
			_handle: PhantomData,
		}
	}
}

impl<I, H> IntoParallelIterator<H> for Flatten<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, H> ParallelIterator<H> for Flatten<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = <I::Item as IntoParallelIterator<H>>::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FlattenConsumer::new(consumer);
		self.pi.drive_unindexed(higher_order_consumer)
	}
}

/// A consumer which flattens the elements of a base [`Consumer`]
struct FlattenConsumer<C> {
	/// The base [`Consumer`]
	base: C,
}

impl<C> FlattenConsumer<C> {
	/// Creates a new [`FlattenConsumer<C>`].
	fn new(base: C) -> Self {
		FlattenConsumer { base }
	}
}

impl<T, C, H> Consumer<T, H> for FlattenConsumer<C>
where
	T: IntoParallelIterator<H>,
	C: UnindexedConsumer<T::Item, H>,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FlattenFolder<C, C::Result, H>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(Self::new(left), Self::new(right), reducer)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base,
			previous: None,
			_handle: PhantomData,
		}
	}
}

impl<T, C, H> UnindexedConsumer<T, H> for FlattenConsumer<C>
where
	C: UnindexedConsumer<T::Item, H>,
	T: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left())
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] which takes each element, turns it into a [`ParallelIterator`] and
/// drives it, and returns the consumed result.
struct FlattenFolder<C, R, H> {
	/// The base [`Consumer`]
	base: C,
	/// The reduction of all previously yielded results
	previous: Option<R>,
	/// A [`ThreadPoolHandle`] to the threadpool driving this consumer.
	_handle: PhantomData<H>,
}

impl<T, C, H> Folder<T> for FlattenFolder<C, C::Result, H>
where
	C: UnindexedConsumer<T::Item, H>,
	T: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		let par_iter = item.into_par_iter();
		let consumer = self.base.split_off_left();
		let result = par_iter.drive_unindexed(consumer);

		Self {
			previous: match self.previous {
				Some(previous) => {
					let reducer = self.base.to_reducer();
					Some(reducer.reduce(previous, result))
				}
				None => Some(result),
			},
			base: self.base,
			_handle: PhantomData,
		}
	}

	fn complete(self) -> Self::Result {
		match self.previous {
			Some(previous) => previous,
			None => self.base.into_folder().complete(),
		}
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
