//! The module containing the [`FlattenIter`] [`ParallelIterator`]-adaptor.

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`FlattenIter`] turns each element to a serial [`Iterator`], then flattens
/// these iterators together. This struct is created by the
/// [`flatten_iter`](ParallelIterator::flatten_iter) method on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct FlattenIter<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoIterator,
	<I::Item as IntoIterator>::Item: Send,
	H: ThreadPoolHandle,
{
	/// The [`ParallelIterator`] to flatten serially
	pi: I,
	/// A [`ThreadPoolHandle`] to the executor.
	_handle: PhantomData<H>,
}

impl<I, H> FlattenIter<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoIterator,
	<I::Item as IntoIterator>::Item: Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`FlattenIter<I, H>`] from a [`ParallelIterator`].
	pub fn new(pi: I) -> Self {
		Self {
			pi,
			_handle: PhantomData,
		}
	}
}

impl<I, H> IntoParallelIterator<H> for FlattenIter<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoIterator,
	<I::Item as IntoIterator>::Item: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, H> ParallelIterator<H> for FlattenIter<I, H>
where
	I: ParallelIterator<H>,
	I::Item: IntoIterator,
	<I::Item as IntoIterator>::Item: Send,
	H: ThreadPoolHandle,
{
	type Item = <I::Item as IntoIterator>::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = FlattenIterConsumer::new(consumer);
		self.pi.drive_unindexed(higher_order_consumer)
	}
}

/// A [`Consumer`] which flattens elements sequentially.
struct FlattenIterConsumer<C> {
	/// The base consumer over [`Iterator`]s
	base: C,
}

impl<C> FlattenIterConsumer<C> {
	/// Creates a new [`FlattenIterConsumer<C>`].
	fn new(base: C) -> Self {
		Self { base }
	}
}

impl<T, C, H> Consumer<T, H> for FlattenIterConsumer<C>
where
	C: UnindexedConsumer<T::Item, H>,
	T: IntoIterator,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = FlattenIterFolder<C::Folder>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(Self::new(left), Self::new(right), reducer)
	}

	fn into_folder(self) -> Self::Folder {
		FlattenIterFolder {
			base: self.base.into_folder(),
		}
	}
}

impl<T, C, H> UnindexedConsumer<T, H> for FlattenIterConsumer<C>
where
	C: UnindexedConsumer<T::Item, H>,
	T: IntoIterator,
	H: ThreadPoolHandle,
{
	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}

	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left())
	}
}

/// A [`Folder`] over sequentaly iterators.
struct FlattenIterFolder<C> {
	/// The underlying [`Folder`] the items will be folded into.
	base: C,
}

impl<T, C> Folder<T> for FlattenIterFolder<C>
where
	C: Folder<T::Item>,
	T: IntoIterator,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		let base = self.base.consume_iter(item);
		FlattenIterFolder { base }
	}

	fn consume_iter<I>(self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		let iter = iter.into_iter().flatten();
		let base = self.base.consume_iter(iter);
		FlattenIterFolder { base }
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
