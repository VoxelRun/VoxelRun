//! Module containing the [`Inspect`] [`ParallelIterator`]-adaptor.
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Inspect`] takes an operation `inspect_op` and calls it for each element
/// before yielding it.
///
/// This `struct` is created by the [inspect](`ParallelIterator::inspect`)
/// method on [`ParallelIterator`]. See its documentation for more.
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Inspect<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(&I::Item) + Sync + Send,
	H: ThreadPoolHandle,
{
	/// The underlying [`ParallelIterator`] being inspected.
	pi: I,
	/// The function that inspects the element.
	inspect_op: F,
	/// The [`ThreadPoolHandle`] to the executor.
	_handle: PhantomData<H>,
}

impl<I, F, H> Inspect<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(&I::Item) + Sync + Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Inspect`].
	pub fn new(pi: I, inspect_op: F) -> Self {
		Self {
			pi,
			inspect_op,
			_handle: PhantomData,
		}
	}
}

impl<I, F, H> IntoParallelIterator<H> for Inspect<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(&I::Item) + Sync + Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, F, H> ParallelIterator<H> for Inspect<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(&I::Item) + Sync + Send,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = InspectConsumer::new(consumer, &self.inspect_op);
		self.pi.drive_unindexed(higher_order_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.pi.opt_len()
	}
}

impl<I, F, H> IndexedParallelIterator<H> for Inspect<I, F, H>
where
	I: IndexedParallelIterator<H>,
	F: Fn(&I::Item) + Sync + Send,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_order_consumer = InspectConsumer::new(consumer, &self.inspect_op);
		self.pi.drive(higher_order_consumer)
	}

	fn len(&self) -> usize {
		self.pi.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.pi.with_producer(Callback {
			callback,
			inspect_op: self.inspect_op,
		});

		/// A callback that creates a [`Producer`] that calls the `inspect_op`
		/// for each yielded element.
		struct Callback<CB, F> {
			/// The previous callback.
			callback: CB,
			/// The function being called to inspect the elements.
			inspect_op: F,
		}

		impl<T, F, CB, H> ProducerCallback<T, H> for Callback<CB, F>
		where
			CB: ProducerCallback<T, H>,
			F: Fn(&T) + Sync,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, base: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				let producer = InspectProducer {
					base,
					inspect_op: &self.inspect_op,
				};
				self.callback.callback(producer)
			}
		}
	}
}

/// A [`Consumer`] which applies an inspector function `F` to each element of
/// the base [`Consumer`].
struct InspectConsumer<'f, C, F> {
	/// The base [`Consumer`] which is being fed after inspecting the elements.
	base: C,
	/// The inspector function.
	inspect_op: &'f F,
}

impl<'f, C, F> InspectConsumer<'f, C, F> {
	/// Creates a new [`InspectConsumer`] from a [`Consumer`] and an `inspect_op`.
	fn new(base: C, inspect_op: &'f F) -> Self {
		Self { base, inspect_op }
	}
}

impl<'f, T, C, F, H> Consumer<T, H> for InspectConsumer<'f, C, F>
where
	F: 'f,
	C: Consumer<T, H>,
	F: Fn(&T) + Sync,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = InspectFolder<'f, F, C::Folder>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.inspect_op),
			Self::new(right, self.inspect_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			inspect_op: self.inspect_op,
		}
	}
}

impl<'f, T, C, F, H> UnindexedConsumer<T, H> for InspectConsumer<'f, C, F>
where
	F: 'f,
	C: UnindexedConsumer<T, H>,
	F: Fn(&T) + Sync,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.inspect_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] which applies an inspector function to each element.
struct InspectFolder<'f, F, B> {
	/// The [`Folder`] which will be fed after the elements were inspected.
	base: B,
	/// The function inspecting the elements..
	inspect_op: &'f F,
}

impl<'f, F, B, T> Folder<T> for InspectFolder<'f, F, B>
where
	B: Folder<T>,
	F: 'f + Fn(&T),
{
	type Result = B::Result;

	fn consume(self, item: T) -> Self {
		(self.inspect_op)(&item);
		Self {
			base: self.base.consume(item),
			inspect_op: self.inspect_op,
		}
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.base = self
			.base
			.consume_iter(iter.into_iter().inspect(self.inspect_op));
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

/// A [`Producer`] which inspects an item before yielding it.
struct InspectProducer<'f, F, P> {
	/// The [`Producer`] being inspected.
	base: P,
	/// The function that inspects the elements.
	inspect_op: &'f F,
}

impl<'f, F, P, H> Producer<H> for InspectProducer<'f, F, P>
where
	P: Producer<H>,
	F: Fn(&P::Item) + Sync,
	H: ThreadPoolHandle,
{
	type Item = P::Item;

	type IntoIter = core::iter::Inspect<P::IntoIter, &'f F>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(
			Self {
				base: left,
				inspect_op: self.inspect_op,
			},
			Self {
				base: right,
				inspect_op: self.inspect_op,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		self.base.into_iter().inspect(self.inspect_op)
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<G>(self, folder: G) -> G
	where
		G: Folder<Self::Item>,
	{
		self.base
			.fold_with(InspectFolder {
				base: folder,
				inspect_op: self.inspect_op,
			})
			.base
	}
}
