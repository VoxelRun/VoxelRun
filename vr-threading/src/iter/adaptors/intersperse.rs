//! Module in which the [`Intersperse`] parallel iterator adaptor is contained.
use core::{cell::Cell, iter::Fuse};

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Intersperse`] is an iterator that inserts a particular item between each
/// item of the adapted iterator. This struct is created by the
/// [`intersperse`](ParallelIterator::intersperse) method on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Intersperse<I, H>
where
	I: ParallelIterator<H>,
	I::Item: Clone + Send,
	H: ThreadPoolHandle,
{
	/// The iterator being interspersed.
	base: I,
	/// The items with which it will be interspersed.
	item: I::Item,
}

impl<I, H> Intersperse<I, H>
where
	I: ParallelIterator<H>,
	I::Item: Clone + Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Intersperse`].
	pub fn new(base: I, item: I::Item) -> Self {
		Self { base, item }
	}
}

impl<I, H> ParallelIterator<H> for Intersperse<I, H>
where
	I: ParallelIterator<H>,
	I::Item: Clone + Send,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let consumer = IntersperseConsumer::new(consumer, self.item);
		self.base.drive_unindexed(consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		match self.base.opt_len()? {
			0 => Some(0),
			len => len.checked_add(len - 1),
		}
	}
}

impl<I, H> IndexedParallelIterator<H> for Intersperse<I, H>
where
	I: IndexedParallelIterator<H>,
	I::Item: Clone + Send,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_consumer = IntersperseConsumer::new(consumer, self.item);
		self.base.drive(higher_consumer)
	}

	fn len(&self) -> usize {
		match self.base.len() {
			0 => 0,
			len => len.checked_add(len - 1).expect("overflow"),
		}
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		let len = self.len();
		return self.base.with_producer(Callback {
			callback,
			item: self.item,
			len,
		});

		/// [`ProducerCallback`] which adds on an [`IntersperseProducer`].
		struct Callback<CB, T> {
			/// the previous callback.
			callback: CB,
			/// The item being interspersed into the iterator.
			item: T,
			/// The length of the whole interspersed iterator.
			len: usize,
		}

		impl<CB, T, H> ProducerCallback<T, H> for Callback<CB, T>
		where
			CB: ProducerCallback<T, H>,
			T: Clone + Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				let producer = IntersperseProducer::new(producer, self.item, self.len);
				self.callback.callback(producer)
			}
		}
	}
}

impl<I, H> IntoParallelIterator<H> for Intersperse<I, H>
where
	I: ParallelIterator<H>,
	I::Item: Clone + Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Producer`] which intersperses items between the `base`s' elements.
struct IntersperseProducer<P, T> {
	/// The `base` [`Producer`].
	base: P,
	/// The item being interspersed between the elements from base.
	item: T,
	/// The length of this cutout, inclusive ot the interspersed elements.
	len: usize,
	/// Whether `item` should be cloned at the beginning or not.
	clone_first: bool,
}

impl<P, T> IntersperseProducer<P, T> {
	/// Creates a new [`IntersperseProducer`].
	fn new(base: P, item: T, len: usize) -> Self {
		Self {
			base,
			item,
			len,
			clone_first: false,
		}
	}
}

impl<P, H> Producer<H> for IntersperseProducer<P, P::Item>
where
	P: Producer<H>,
	P::Item: Send + Clone,
	H: ThreadPoolHandle,
{
	type Item = P::Item;

	type IntoIter = IntersperseIter<P::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		debug_assert!(index <= self.len);

		let base_index = (index + !self.clone_first as usize) / 2;
		let (left_base, right_base) = self.base.split_at(base_index);

		let left = Self {
			base: left_base,
			item: self.item.clone(),
			len: index,
			clone_first: self.clone_first,
		};

		let right = Self {
			base: right_base,
			item: self.item,
			len: self.len - index,
			clone_first: (index & 1 == 1) ^ self.clone_first,
		};

		(left, right)
	}

	fn into_iter(self) -> Self::IntoIter {
		Self::IntoIter {
			base: self.base.into_iter().fuse(),
			item: self.item,
			clone_first: self.len > 0 && self.clone_first,
			clone_last: self.len > 1 && (self.len & 1 == 0) ^ self.clone_first,
		}
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<F>(self, folder: F) -> F
	where
		F: Folder<Self::Item>,
	{
		let higher_folder = IntersperseFolder {
			base: folder,
			item: self.item,
			clone_first: self.clone_first,
		};
		self.base.fold_with(higher_folder).base
	}
}

/// An [`Iterator`] which intersperses items between those of an adapted iterator.
struct IntersperseIter<I>
where
	I: Iterator + DoubleEndedIterator + ExactSizeIterator,
	I::Item: Clone,
{
	/// The adapted iterator.
	base: Fuse<I>,
	/// The item being interspersed.
	item: I::Item,
	/// Whether to insert `item` at the front, too.
	clone_first: bool,
	/// Whether to insert `item` at the end, too.
	clone_last: bool,
}

impl<I> Iterator for IntersperseIter<I>
where
	I: Iterator + DoubleEndedIterator + ExactSizeIterator,
	I::Item: Clone,
{
	type Item = I::Item;

	fn next(&mut self) -> Option<Self::Item> {
		if self.clone_first {
			self.clone_first = false;
			Some(self.item.clone())
		} else if let next @ Some(_) = self.base.next() {
			self.clone_first = self.base.len() != 0;
			next
		} else if self.clone_last {
			self.clone_last = false;
			Some(self.item.clone())
		} else {
			None
		}
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		let len = self.len();
		(len, Some(len))
	}
}

impl<I> DoubleEndedIterator for IntersperseIter<I>
where
	I: Iterator + DoubleEndedIterator + ExactSizeIterator,
	I::Item: Clone,
{
	fn next_back(&mut self) -> Option<Self::Item> {
		if self.clone_last {
			self.clone_last = false;
			Some(self.item.clone())
		} else if let next_back @ Some(_) = self.base.next_back() {
			self.clone_last = self.base.len() != 0;
			next_back
		} else if self.clone_first {
			self.clone_first = false;
			Some(self.item.clone())
		} else {
			None
		}
	}
}

impl<I> ExactSizeIterator for IntersperseIter<I>
where
	I: Iterator + DoubleEndedIterator + ExactSizeIterator,
	I::Item: Clone,
{
	fn len(&self) -> usize {
		let len = self.base.len();
		len + len.saturating_sub(1) + self.clone_first as usize + self.clone_last as usize
	}
}

/// A [`Consumer`] interspersing items to give to a base.
struct IntersperseConsumer<C, T> {
	/// The base [`Consumer`].
	base: C,
	/// The item being interspersed.
	item: T,
	/// Whether the first element yielded is a clone of `item` or not.
	clone_first: Cell<bool>,
}

impl<C, T> IntersperseConsumer<C, T> {
	/// Creates a new [`IntersperseConsumer`].
	///
	/// Expects
	/// - `base` to be a [`Consumer`]
	/// - `T` to be [`Clone`] and [`Send`]
	fn new(base: C, item: T) -> Self {
		Self {
			base,
			item,
			clone_first: false.into(),
		}
	}
}

impl<C, T, H> Consumer<T, H> for IntersperseConsumer<C, T>
where
	C: Consumer<T, H>,
	T: Clone + Send,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = IntersperseFolder<C::Folder, T>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(mut self, index: usize) -> (Self, Self, Self::Reducer) {
		// feed the base consumer twice as many items except if not leading with
		// cloned item, then it's one less
		let base_index = index + index.saturating_sub(!self.clone_first.get() as usize);
		let (left, right, reducer) = self.base.split_at(base_index);

		let right = Self {
			base: right,
			item: self.item.clone(),
			clone_first: true.into(),
		};
		self.base = left;
		(self, right, reducer)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			item: self.item,
			clone_first: self.clone_first.get(),
		}
	}
}
impl<C, T, H> UnindexedConsumer<T, H> for IntersperseConsumer<C, T>
where
	C: UnindexedConsumer<T, H>,
	T: Clone + Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		let left = Self {
			base: self.base.split_off_left(),
			item: self.item.clone(),
			clone_first: self.clone_first.clone(),
		};
		self.clone_first.set(true);
		left
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] interspersing elements and yielding them.
struct IntersperseFolder<F, T> {
	/// The [`Folder`] which gets fed with the addition of interspersed `item`s.
	base: F,
	/// The `item` that will be used to intersperse.
	item: T,
	/// Whether the first item should be a clone of `item` or from the iterator.
	clone_first: bool,
}

impl<F, T> Folder<T> for IntersperseFolder<F, T>
where
	F: Folder<T>,
	T: Clone,
{
	type Result = F::Result;

	fn consume(mut self, item: T) -> Self {
		if self.clone_first {
			self.base = self.base.consume(self.item.clone());
			if self.base.full() {
				return self;
			}
		} else {
			self.clone_first = true;
		}
		self.base = self.base.consume(item);
		self
	}

	fn consume_iter<I>(self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		Self {
			base: self.base.consume_iter(iter.into_iter().flat_map(|item| {
				let first = if self.clone_first {
					Some(self.item.clone())
				} else {
					None
				};
				first.into_iter().chain(core::iter::once(item))
			})),
			item: self.item,
			clone_first: self.clone_first,
		}
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
