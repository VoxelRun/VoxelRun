//! Where the [`Map`] adaptor is contained

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator, ThreadPoolHandle,
	},
	iterator_into_iter_impl,
};

/// [`Map`] is an iterator that transforms items of the underlying iterator
///
/// This struct is created by the [`map`](ParallelIterator::map) on [`ParallelIterator`]
#[derive(Debug)]
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Map<I, F, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The base iterator
	base: I,
	/// The operation to map on each element
	map_op: F,

	/// The handle to the threadnpool
	_handle: PhantomData<H>,
}

impl<I, F, H> Map<I, F, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Map<I, F, H>`]. With the [`ParallelIterator`] `base` and
	/// the `map_op` to map on top of it.
	pub fn new(base: I, map_op: F) -> Self {
		Self {
			base,
			map_op,
			_handle: PhantomData,
		}
	}
}

/// The [`Consumer`] for the [`Map`]
struct MapConsumer<'f, C, F> {
	/// the underlying consuer
	base: C,
	/// the map op mapped on each element
	map_op: &'f F,
}

impl<'f, C, F> MapConsumer<'f, C, F> {
	/// Creates a new [`MapConsumer<C, F>`] with `base` being the actual underlying
	/// [`Consumer`] and the `map_op` being the transformation that happens before.
	fn new(base: C, map_op: &'f F) -> Self {
		Self { base, map_op }
	}
}

/// The [`Folder`] for a [`Map`].
struct MapFolder<'f, C, F> {
	/// The base [`Consumer`]
	base: C,
	/// The operation to map
	map_op: &'f F,
}

/// The [`Producer`] for [`Map`]-items. Used in [`IndexedParallelIterator::with_producer`].
struct MapProducer<'f, P, F> {
	/// the base [`Producer`]
	base: P,
	/// the function to map on top of
	map_op: &'f F,
}

impl<I, F, R, H> ParallelIterator<H> for Map<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = F::Output;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_order_consumer = MapConsumer::new(consumer, &self.map_op);
		self.base.drive_unindexed(higher_order_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.base.opt_len()
	}
}

impl<I, F, R, H> IntoParallelIterator<H> for Map<I, F, H>
where
	I: ParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<I, F, R, H> IndexedParallelIterator<H> for Map<I, F, H>
where
	I: IndexedParallelIterator<H>,
	F: Fn(I::Item) -> R + Send + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_order_consumer = MapConsumer::new(consumer, &self.map_op);
		self.base.drive(higher_order_consumer)
	}

	fn len(&self) -> usize {
		self.base.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		return self.base.with_producer(Callback {
			callback,
			map_op: self.map_op,
		});

		/// Callback that maps itself on top of `callback` executes `map_op`
		/// for each element.
		struct Callback<CB, F> {
			/// the previous callback
			callback: CB,
			/// the operation the map executes
			map_op: F,
		}

		impl<T, F, R, CB, H> ProducerCallback<T, H> for Callback<CB, F>
		where
			CB: ProducerCallback<R, H>,
			F: Fn(T) -> R + Sync,
			R: Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: crate::iter::plumbing::Producer<H, Item = T>,
			{
				let producer = MapProducer {
					base: producer,
					map_op: &self.map_op,
				};
				self.callback.callback(producer)
			}
		}
	}
}

impl<'f, T, F, R, C> Folder<T> for MapFolder<'f, C, F>
where
	F: Fn(T) -> R,
	C: Folder<F::Output>,
{
	type Result = C::Result;

	fn consume(self, item: T) -> Self {
		let mapped = (self.map_op)(item);
		Self {
			base: self.base.consume(mapped),
			map_op: self.map_op,
		}
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.base = self.base.consume_iter(iter.into_iter().map(self.map_op));
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

impl<'f, T, F, R, C, H> Consumer<T, H> for MapConsumer<'f, C, F>
where
	R: Send,
	F: Fn(T) -> R + Sync,
	C: Consumer<F::Output, H>,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = MapFolder<'f, C::Folder, F>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			MapConsumer::new(left, self.map_op),
			MapConsumer::new(right, self.map_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		MapFolder {
			base: self.base.into_folder(),
			map_op: self.map_op,
		}
	}
}

impl<'f, T, F, R, C, H> UnindexedConsumer<T, H> for MapConsumer<'f, C, F>
where
	F: Fn(T) -> R + Sync,
	C: UnindexedConsumer<F::Output, H>,
	R: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		MapConsumer::new(self.base.split_off_left(), self.map_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

impl<'f, P, F, R, H> Producer<H> for MapProducer<'f, P, F>
where
	P: Producer<H>,
	F: Fn(P::Item) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = F::Output;

	type IntoIter = core::iter::Map<P::IntoIter, &'f F>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(
			MapProducer {
				base: left,
				map_op: self.map_op,
			},
			MapProducer {
				base: right,
				map_op: self.map_op,
			},
		)
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<G>(self, folder: G) -> G
	where
		G: Folder<Self::Item>,
	{
		let higher_order_folder = MapFolder {
			base: folder,
			map_op: self.map_op,
		};

		self.base.fold_with(higher_order_folder).base
	}

	fn into_iter(self) -> Self::IntoIter {
		self.base.into_iter().map(self.map_op)
	}
}
