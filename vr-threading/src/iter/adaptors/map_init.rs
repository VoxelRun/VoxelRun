//! Module in which the [`MapInit`] parallel iterator adaptor is contained.

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

use super::map_with::{MapWithFolder, MapWithIter};

/// [`MapInit`] is an iterator that transforms the elements of an underlying iterator.
///
/// This struct is created through the [`map_init`](ParallelIterator::map_init) method
/// on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct MapInit<I, INIT, T, F, R, H>
where
	I: ParallelIterator<H>,
	INIT: Fn() -> T + Sync + Send,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	/// The parallel iterator being mapped.
	pi: I,
	/// The initialiser function for `T`
	init: INIT,
	/// The mapping operation.
	map_op: F,
	/// The handle to the current executor.
	_handle: PhantomData<H>,
}

impl<I, INIT, T, F, R, H> MapInit<I, INIT, T, F, R, H>
where
	I: ParallelIterator<H>,
	INIT: Fn() -> T + Sync + Send,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`MapInit`].
	pub fn new(pi: I, init: INIT, map_op: F) -> Self {
		Self {
			pi,
			init,
			map_op,
			_handle: PhantomData,
		}
	}
}

impl<I, INIT, T, F, R, H> ParallelIterator<H> for MapInit<I, INIT, T, F, R, H>
where
	I: ParallelIterator<H>,
	INIT: Fn() -> T + Sync + Send,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = R;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_consumer = MapInitConsumer::new(consumer, &self.init, &self.map_op);
		self.pi.drive_unindexed(higher_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.pi.opt_len()
	}
}

impl<I, INIT, T, F, R, H> IndexedParallelIterator<H> for MapInit<I, INIT, T, F, R, H>
where
	I: IndexedParallelIterator<H>,
	INIT: Fn() -> T + Sync + Send,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_consumer = MapInitConsumer::new(consumer, &self.init, &self.map_op);
		self.pi.drive(higher_consumer)
	}

	fn len(&self) -> usize {
		self.pi.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.pi.with_producer(Callback {
			callback,
			init: self.init,
			map_op: self.map_op,
		});

		/// A [`Callback`] mapping a [`Producer`] to another producer [`Producer`].
		struct Callback<CB, INIT, F> {
			/// The previous [`Callback`]
			callback: CB,
			/// The initialiser for the first function param.
			init: INIT,
			/// The mapping operation.
			map_op: F,
		}

		impl<CB, INIT, U, T, F, R, H> ProducerCallback<T, H> for Callback<CB, INIT, F>
		where
			CB: ProducerCallback<R, H>,
			INIT: Fn() -> U + Sync,
			F: Fn(&mut U, T) -> R + Sync,
			R: Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				self.callback.callback(MapInitProducer {
					base: producer,
					map_op: &self.map_op,
					init: &self.init,
				})
			}
		}
	}
}

impl<I, INIT, T, F, R, H> IntoParallelIterator<H> for MapInit<I, INIT, T, F, R, H>
where
	I: ParallelIterator<H>,
	INIT: Fn() -> T + Sync + Send,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Consumer`] that takes an initialiser function `init` and a mapping function
/// `map_op` and transforms the previous element and feeds it into the `base`.
struct MapInitConsumer<'f, C, INIT, F> {
	/// The `base` consumer
	base: C,
	/// The initialiser function.
	init: &'f INIT,
	/// The mapping operation.
	map_op: &'f F,
}

impl<'f, C, INIT, F> MapInitConsumer<'f, C, INIT, F> {
	/// Creates a new [`MapInitConsumer`].
	///
	/// Expecting 
	/// - `init` to be [`Sync`] and a funtion which takes no arguments and 
	///    produces a type `U`
	/// - `map_op` to be [`Sync`] and a function which takes a `&mut U` and 
	///    type `T` and returns a type `R`.
	/// - `base` to be a consumer of an element of type `R`
	/// - `R` to be [`Send`]
	fn new(base: C, init: &'f INIT, map_op: &'f F) -> Self {
		Self { base, init, map_op }
	}
}

impl<'f, T, INIT, U, R, C, F, H> Consumer<T, H> for MapInitConsumer<'f, C, INIT, F>
where
	C: Consumer<R, H>,
	INIT: Fn() -> U + Sync,
	F: Fn(&mut U, T) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = MapWithFolder<'f, C::Folder, U, F>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.init, self.map_op),
			Self::new(right, self.init, self.map_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			map_op: self.map_op,
			item: (self.init)(),
		}
	}
}

impl<'f, T, INIT, U, R, C, F, H> UnindexedConsumer<T, H> for MapInitConsumer<'f, C, INIT, F>
where
	C: UnindexedConsumer<R, H>,
	INIT: Fn() -> U + Sync,
	F: Fn(&mut U, T) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.init, self.map_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Producer`] which maps another producer items with the help of a initialiser 
/// for a value.
struct MapInitProducer<'f, P, INIT, F> {
	/// The base [`Producer`] being mapped.
	base: P,
	/// The initialiser function
	init: &'f INIT,
	/// The mapping operation.
	map_op: &'f F,
}

impl<'f, P, INIT, U, F, R, H> Producer<H> for MapInitProducer<'f, P, INIT, F>
where
	P: Producer<H>,
	INIT: Fn() -> U + Sync,
	F: Fn(&mut U, P::Item) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = R;

	type IntoIter = MapWithIter<'f, P::IntoIter, U, F>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(
			Self {
				base: left,
				init: self.init,
				map_op: self.map_op,
			},
			Self {
				base: right,
				init: self.init,
				map_op: self.map_op,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		Self::IntoIter {
			base: self.base.into_iter(),
			map_op: self.map_op,
			item: (self.init)(),
		}
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<G>(self, folder: G) -> G
	where
		G: crate::iter::plumbing::Folder<Self::Item>,
	{
		let higher_folder = MapWithFolder {
			base: folder,
			item: (self.init)(),
			map_op: self.map_op,
		};

		self.base.fold_with(higher_folder).base
	}
}
