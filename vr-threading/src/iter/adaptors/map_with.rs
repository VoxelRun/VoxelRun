//! Module in which the [`MapWith`] parallel iterator adaptor is contained.

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`MapWith`] is an iterator that transforms the elements of an underlying
/// iterator.
///
/// This struct is created by the [`map_with`](ParallelIterator::map_with) method
/// on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct MapWith<I, H, T, F, R>
where
	I: ParallelIterator<H>,
	T: Send + Clone,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	/// The [`ParallelIterator`] being mapped
	base: I,
	/// The pre-initialised item.
	item: T,
	/// The operation that maps items to other items.
	map_op: F,
	/// A handle to the executing threadpool.
	_hanldle: PhantomData<H>,
}

impl<I, H, T, F, R> MapWith<I, H, T, F, R>
where
	I: ParallelIterator<H>,
	T: Send + Clone,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`MapWith`].
	pub fn new(base: I, item: T, map_op: F) -> Self {
		Self {
			base,
			item,
			map_op,
			_hanldle: PhantomData,
		}
	}
}

impl<I, H, T, F, R> ParallelIterator<H> for MapWith<I, H, T, F, R>
where
	I: ParallelIterator<H>,
	T: Send + Clone,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = R;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_consumer = MapWithConsumer::new(consumer, self.item, &self.map_op);
		self.base.drive_unindexed(higher_consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		self.base.opt_len()
	}
}

impl<I, H, T, F, R> IndexedParallelIterator<H> for MapWith<I, H, T, F, R>
where
	I: IndexedParallelIterator<H>,
	T: Send + Clone,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		let higher_consumer = MapWithConsumer::new(consumer, self.item, &self.map_op);
		self.base.drive(higher_consumer)
	}

	fn len(&self) -> usize {
		self.base.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.base.with_producer(Callback {
			callback,
			item: self.item,
			map_op: self.map_op,
		});

		/// A callback which adds in the [`MapWithProducer`] into the producer chain
		struct Callback<CB, U, F> {
			/// The previous callback.
			callback: CB,
			/// The item given to each map operation
			item: U,
			/// the mapping operation
			map_op: F,
		}

		impl<T, U, F, R, CB, H> ProducerCallback<T, H> for Callback<CB, U, F>
		where
			CB: ProducerCallback<R, H>,
			U: Send + Clone,
			F: Fn(&mut U, T) -> R + Sync,
			R: Send,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				let producer = MapWithProducer {
					base: producer,
					item: self.item,
					map_op: &self.map_op,
				};
				self.callback.callback(producer)
			}
		}
	}
}

impl<I, H, T, F, R> IntoParallelIterator<H> for MapWith<I, H, T, F, R>
where
	I: ParallelIterator<H>,
	T: Send + Clone,
	F: Fn(&mut T, I::Item) -> R + Sync + Send,
	R: Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Producer`] which maps elements to other elements using a function.
struct MapWithProducer<'f, P, U, F> {
	/// The [`Producer`] producing the items being mapped.
	base: P,
	/// The item given to each mapping function.
	item: U,
	/// The function mapping the elements.
	map_op: &'f F,
}

impl<'f, P, U, F, R, H> Producer<H> for MapWithProducer<'f, P, U, F>
where
	P: Producer<H>,
	U: Send + Clone,
	F: Fn(&mut U, P::Item) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Item = R;

	type IntoIter = MapWithIter<'f, P::IntoIter, U, F>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.base.split_at(index);
		(
			Self {
				base: left,
				item: self.item.clone(),
				map_op: self.map_op,
			},
			Self {
				base: right,
				item: self.item,
				map_op: self.map_op,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		Self::IntoIter {
			base: self.base.into_iter(),
			item: self.item,
			map_op: self.map_op,
		}
	}

	fn min_len(&self) -> usize {
		self.base.min_len()
	}

	fn max_len(&self) -> usize {
		self.base.max_len()
	}

	fn fold_with<G>(self, folder: G) -> G
	where
		G: Folder<Self::Item>,
	{
		let higher_folder = MapWithFolder {
			base: folder,
			item: self.item,
			map_op: self.map_op,
		};
		self.base.fold_with(higher_folder).base
	}
}

/// A [`Consumer`] which maps elements to other elements.
struct MapWithConsumer<'f, C, U, F> {
	/// The previous [`Consumer`] being fed.
	base: C,
	/// The item to give each mapping operation
	item: U,
	/// the mapping operation.
	map_op: &'f F,
}

impl<'f, C, U, F> MapWithConsumer<'f, C, U, F> {
	/// Creates a new [`MapWithConsumer`].
	///
	/// Expecting `base` to be a [`Consumer`], `item` to be [`Send`] and [`Clone`],
	/// and `map_op` to be function with `Fn(&mut item_type, T) -> R`.
	fn new(base: C, item: U, map_op: &'f F) -> Self {
		Self { base, item, map_op }
	}
}

impl<'f, T, U, R, C, F, H> Consumer<T, H> for MapWithConsumer<'f, C, U, F>
where
	C: Consumer<R, H>,
	U: Send + Clone,
	F: Fn(&mut U, T) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = MapWithFolder<'f, C::Folder, U, F>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self::new(left, self.item.clone(), self.map_op),
			Self::new(right, self.item, self.map_op),
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			item: self.item,
			map_op: self.map_op,
		}
	}
}

impl<'f, T, U, R, C, F, H> UnindexedConsumer<T, H> for MapWithConsumer<'f, C, U, F>
where
	C: UnindexedConsumer<R, H>,
	U: Send + Clone,
	F: Fn(&mut U, T) -> R + Sync,
	R: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self::new(self.base.split_off_left(), self.item.clone(), self.map_op)
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] mapping elements to other elements.
pub(super) struct MapWithFolder<'f, B, U, F> {
	/// The `base` [`Folder`].
	pub(super) base: B,
	/// The item given to each mapping operation.
	pub(super) item: U,
	/// The mapping operation.
	pub(super) map_op: &'f F,
}

impl<'f, T, U, R, B, F> Folder<T> for MapWithFolder<'f, B, U, F>
where
	B: Folder<R>,
	F: Fn(&mut U, T) -> R,
{
	type Result = B::Result;

	fn consume(mut self, item: T) -> Self {
		let mapped_item = (self.map_op)(&mut self.item, item);
		self.base = self.base.consume(mapped_item);
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		let mapped_iter = iter.into_iter().map(|x| (self.map_op)(&mut self.item, x));
		self.base = self.base.consume_iter(mapped_iter);
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

/// [`Iterator`] which gives each mapping iteration an item.
pub(super) struct MapWithIter<'f, I, U, F> {
	/// The base [`Iterator`].
	pub(super) base: I,
	/// The item being given.
	pub(super) item: U,
	/// The mapping operation.
	pub(super) map_op: &'f F,
}

impl<'f, I, U, F, R> Iterator for MapWithIter<'f, I, U, F>
where
	I: Iterator,
	F: Fn(&mut U, I::Item) -> R + Sync,
	R: Send,
{
	type Item = R;

	fn next(&mut self) -> Option<Self::Item> {
		let item = self.base.next()?;
		Some((self.map_op)(&mut self.item, item))
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.base.size_hint()
	}
}

impl<'f, I, U, F, R> DoubleEndedIterator for MapWithIter<'f, I, U, F>
where
	I: DoubleEndedIterator,
	F: Fn(&mut U, I::Item) -> R + Sync,
	R: Send,
{
	fn next_back(&mut self) -> Option<Self::Item> {
		let item = self.base.next_back()?;
		Some((self.map_op)(&mut self.item, item))
	}
}

impl<'f, I, U, F, R> ExactSizeIterator for MapWithIter<'f, I, U, F>
where
	I: ExactSizeIterator,
	F: Fn(&mut U, I::Item) -> R + Sync,
	R: Send,
{
}
