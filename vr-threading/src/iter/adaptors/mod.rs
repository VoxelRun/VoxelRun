//! Module containing the different iterator adaptor implementations

pub mod chain;
pub mod cloned;
pub mod copied;
pub mod enumerate;
pub mod filter;
pub mod filter_map;
pub mod flat_map;
pub mod flat_map_iter;
pub mod flatten;
pub mod flatten_iter;
pub mod inspect;
pub mod intersperse;
pub mod map;
pub mod map_with;
pub mod map_init;
pub mod skip;
pub mod skip_any;
pub mod skip_any_while;
pub mod zip;
