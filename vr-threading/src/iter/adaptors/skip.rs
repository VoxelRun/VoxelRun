//! Module in which the [`Skip`] parallel iterator adaptor is contained.

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{bridge, bridge_producer_consumer, noop::NoopConsumer, ProducerCallback},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`Skip`] is an iterator that skips over the first `n` elements. This struct
/// is created by the [`skip`](IndexedParallelIterator::skip) method on
/// [`IndexedParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Skip<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The iterator from which items must be skipped.
	base: I,
	/// The amount of items to skip.
	n: usize,
	/// The handle to the executor.
	_handle: PhantomData<H>,
}

impl<I, H> Skip<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`Skip`].
	pub fn new(base: I, n: usize) -> Self {
		Self {
			base,
			n,
			_handle: PhantomData,
		}
	}
}

impl<I, H> ParallelIterator<H> for Skip<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.len())
	}
}

impl<I, H> IndexedParallelIterator<H> for Skip<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.base.len() - self.n
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		return self.base.with_producer(Callback {
			callback,
			n: self.n,
		});

		/// A callback which ignores the first `n` elements of a producer.
		struct Callback<CB> {
			/// The previous callback.
			callback: CB,
			/// how many items to ignore.
			n: usize,
		}

		impl<T, CB, H> ProducerCallback<T, H> for Callback<CB>
		where
			CB: ProducerCallback<T, H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			// TODO!: use scope here, once scope can be included in ThreadPoolHandle
			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: crate::iter::plumbing::Producer<H, Item = T>,
			{
				let Self { callback, n } = self;
				let (before_skip, after_skip) = producer.split_at(n);
				// run the skipped part seperately for side effects.
				// Panics will still be propagated.
				bridge_producer_consumer(n, before_skip, NoopConsumer);
				callback.callback(after_skip)
			}
		}
	}
}

impl<I, H> IntoParallelIterator<H> for Skip<I, H>
where
	I: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}
