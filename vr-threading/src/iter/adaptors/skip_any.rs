//! Module in which the [`SkipAny`] parallel iterator adaptor is contained.

use core::{marker::PhantomData, sync::atomic::AtomicUsize};

use vr_core::utils::math::checked_decrement;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`SkipAny`] is an iterator that skips over `n` elements from anywhere in the
/// original [`ParallelIterator`] `I`. This struct is created by the
/// [`skip_any`](ParallelIterator::skip_any) method on [`ParallelIterator`].
pub struct SkipAny<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// The base iterator.
	base: I,
	/// Amount of elements to skip.
	count: usize,
	/// handle to the underlying executor.
	_handle: PhantomData<H>,
}

impl<I, H> SkipAny<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Creates a new [`SkipAny`].
	pub fn new(base: I, count: usize) -> Self {
		Self {
			base,
			count,
			_handle: PhantomData,
		}
	}
}

impl<I, H> ParallelIterator<H> for SkipAny<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_consumer = SkipAnyConsumer {
			base: consumer,
			count: &AtomicUsize::new(self.count),
		};
		self.base.drive_unindexed(higher_consumer)
	}

	/// no length as length must be index equivalent
	fn opt_len(&self) -> Option<usize> {
		None
	}
}
impl<I, H> IntoParallelIterator<H> for SkipAny<I, H>
where
	I: ParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Consumer`] that skips the first `n` elements.
struct SkipAnyConsumer<'f, C> {
	/// The base [`Consumer`]
	base: C,
	/// variable keeping track of how many items are left to skip
	count: &'f AtomicUsize,
}

impl<'f, T, C, H> Consumer<T, H> for SkipAnyConsumer<'f, C>
where
	C: Consumer<T, H>,
	T: Send,
	H: ThreadPoolHandle,
{
	type Folder = SkipAnyFolder<'f, C::Folder>;

	type Result = C::Result;

	type Reducer = C::Reducer;

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self { base: left, ..self },
			Self {
				base: right,
				..self
			},
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			count: self.count,
		}
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}

impl<'f, T, C, H> UnindexedConsumer<T, H> for SkipAnyConsumer<'f, C>
where
	C: UnindexedConsumer<T, H>,
	T: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		SkipAnyConsumer {
			base: self.base.split_off_left(),
			..*self
		}
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] skipping `n` elements.
struct SkipAnyFolder<'f, F> {
	/// The base [`Folder`].
	base: F,
	/// Variable keeping track of how many items were already skipped
	count: &'f AtomicUsize,
}

impl<'f, T, F> Folder<T> for SkipAnyFolder<'f, F>
where
	F: Folder<T>,
{
	type Result = F::Result;

	fn consume(mut self, item: T) -> Self {
		if checked_decrement(self.count).is_none() {
			self.base = self.base.consume(item);
		}
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.base = self.base.consume_iter(
			iter.into_iter()
				.skip_while(move |_| checked_decrement(self.count).is_some()),
		);
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
