//! Module in which the [`SkipAnyWhile`] parallel iterator adaptor is contained.
use core::{
	marker::PhantomData,
	sync::atomic::{AtomicBool, Ordering},
};

use crate::{
	iter::{
		plumbing::{Consumer, Folder, UnindexedConsumer},
		IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// [`SkipAnyWhile`] is an iterator that skips over elements from anywhere in
/// `I` until the callback returns `false`. This struct is created by
/// [`skip_any_while`](ParallelIterator::skip_any_while) on [`ParallelIterator`].
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct SkipAnyWhile<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	/// The base [`ParallelIterator`]
	base: I,
	/// The predicate to check if skipping is done or not.
	predicate: P,
	/// Handle to the executor.
	_handle: PhantomData<H>,
}

impl<I, P, H> SkipAnyWhile<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	/// Creates a new [`SkipAnyWhile`].
	pub fn new(base: I, predicate: P) -> Self {
		Self {
			base,
			predicate,
			_handle: PhantomData,
		}
	}
}

impl<I, P, H> ParallelIterator<H> for SkipAnyWhile<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	type Item = I::Item;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let higher_consumer = SkipAnyWhileConsumer {
			base: consumer,
			predicate: &self.predicate,
			skipping: &AtomicBool::new(true),
		};
		self.base.drive_unindexed(higher_consumer)
	}
}

impl<I, P, H> IntoParallelIterator<H> for SkipAnyWhile<I, P, H>
where
	I: ParallelIterator<H>,
	P: Fn(&I::Item) -> bool + Sync + Send,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A [`Consumer`] which skips all elements until a `predicate` is `false`.
struct SkipAnyWhileConsumer<'p, C, P> {
	/// The base [`Consumer`].
	base: C,
	/// The `predicate` to check.
	predicate: &'p P,
	/// The we are still skipping or not.
	skipping: &'p AtomicBool,
}

impl<'p, T, C, P, H> Consumer<T, H> for SkipAnyWhileConsumer<'p, C, P>
where
	C: Consumer<T, H>,
	P: Fn(&T) -> bool + Sync,
	H: ThreadPoolHandle,
{
	type Result = C::Result;

	type Reducer = C::Reducer;

	type Folder = SkipAnyWhileFolder<'p, C::Folder, P>;

	fn full(&self) -> bool {
		self.base.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left, right, reducer) = self.base.split_at(index);
		(
			Self { base: left, ..self },
			Self {
				base: right,
				..self
			},
			reducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			base: self.base.into_folder(),
			predicate: self.predicate,
			skipping: self.skipping,
		}
	}
}

impl<'p, T, C, P, H> UnindexedConsumer<T, H> for SkipAnyWhileConsumer<'p, C, P>
where
	C: UnindexedConsumer<T, H>,
	P: Fn(&T) -> bool + Sync,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self {
			base: self.base.split_off_left(),
			..*self
		}
	}

	fn to_reducer(&self) -> Self::Reducer {
		self.base.to_reducer()
	}
}

/// A [`Folder`] that skips elements until a `predicate` returns `false`.
struct SkipAnyWhileFolder<'p, F, P> {
	/// The base [`Folder`].
	base: F,
	/// The `predicate` to check.
	predicate: &'p P,
	/// Is this in skipping mode or are we past that.
	skipping: &'p AtomicBool,
}

/// Function determinign whether an element and its successors should be skipped 
/// or not.
fn skip<T, F>(item: &T, skipping: &AtomicBool, predicate: &F) -> bool
where
	F: Fn(&T) -> bool,
{
	if !skipping.load(Ordering::Relaxed) {
		return false;
	}

	if predicate(item) {
		return true;
	}

	skipping.store(false, Ordering::Relaxed);
	false
}

impl<'p, T, F, P> Folder<T> for SkipAnyWhileFolder<'p, F, P>
where
	F: Folder<T>,
	P: Fn(&T) -> bool + 'p,
{
	type Result = F::Result;

	fn consume(mut self, item: T) -> Self {
		if !skip(&item, self.skipping, self.predicate) {
			self.base = self.base.consume(item);
		}
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.base = self.base.consume_iter(
			iter.into_iter()
				.skip_while(move |x| skip(x, self.skipping, self.predicate)),
		);
		self
	}

	fn complete(self) -> Self::Result {
		self.base.complete()
	}

	fn full(&self) -> bool {
		self.base.full()
	}
}
