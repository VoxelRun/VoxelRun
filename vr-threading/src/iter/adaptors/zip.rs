//! Where the [`Zip`] adaptor is contained
use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{bridge, Consumer, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator, ThreadPoolHandle,
	},
	iterator_into_iter_impl,
};
/// An iterator that iterates two other iterators simultaneously.

/// This `struct` is created by [`IndexedParallelIterator::zip`]
/// See their documentation for more.
#[derive(Debug)]
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Zip<A, B, H>
where
	H: ThreadPoolHandle,
{
	/// One side of the iterator
	iter_a: A,
	/// The other side of the iterator
	iter_b: B,
	/// The [`ThreadPoolHandle`]
	_handle: PhantomData<H>,
}

impl<A, B, H> Zip<A, B, H>
where
	A: IndexedParallelIterator<H>,
	B: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	/// Create a new [`Zip`], zipping between `A` and `B`.
	pub fn new(iter_a: A, iter_b: B) -> Self {
		Self {
			iter_a,
			iter_b,
			_handle: PhantomData,
		}
	}
}

impl<A, B, H> ParallelIterator<H> for Zip<A, B, H>
where
	A: IndexedParallelIterator<H>,
	B: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	type Item = (A::Item, B::Item);

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.len())
	}
}

impl<A, B, H> IntoParallelIterator<H> for Zip<A, B, H>
where
	A: IndexedParallelIterator<H>,
	B: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<A, B, H> IndexedParallelIterator<H> for Zip<A, B, H>
where
	A: IndexedParallelIterator<H>,
	B: IndexedParallelIterator<H>,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		Ord::min(self.iter_a.len(), self.iter_b.len())
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		return self.iter_a.with_producer(CallbackA {
			callback,
			b_producer: self.iter_b,
		});

		/// The callback that binds the Producer of `B`
		struct CallbackA<CB, B> {
			/// The preivous callback
			callback: CB,
			/// The producer of `B`
			b_producer: B,
		}

		/// The callback that binds the producer of `A`
		struct CallbackB<CB, A> {
			/// The preivous callback
			callback: CB,
			/// The producer of `A`
			a_producer: A,
		}

		impl<CB, T, B, H> ProducerCallback<T, H> for CallbackA<CB, B>
		where
			B: IndexedParallelIterator<H>,
			CB: ProducerCallback<(T, B::Item), H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				self.b_producer.with_producer(CallbackB {
					a_producer: producer,
					callback: self.callback,
				})
			}
		}

		impl<CB, T, A, H> ProducerCallback<T, H> for CallbackB<CB, A>
		where
			A: Producer<H>,
			CB: ProducerCallback<(A::Item, T), H>,
			H: ThreadPoolHandle,
		{
			type Output = CB::Output;

			fn callback<P>(self, producer: P) -> Self::Output
			where
				P: Producer<H, Item = T>,
			{
				self.callback.callback(ZipProducer {
					iter_a: self.a_producer,
					iter_b: producer,
				})
			}
		}
	}
}

/// The [`Producer`] for [`Zip`]
struct ZipProducer<A, B> {
	/// The first side of the [`Zip`]
	iter_a: A,
	/// The second half of the [`Zip`]
	iter_b: B,
}

impl<A, B, H> Producer<H> for ZipProducer<A, B>
where
	A: Producer<H>,
	B: Producer<H>,
	H: ThreadPoolHandle,
{
	type Item = (A::Item, B::Item);

	type IntoIter = core::iter::Zip<A::IntoIter, B::IntoIter>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (a_left, a_right) = self.iter_a.split_at(index);
		let (b_left, b_right) = self.iter_b.split_at(index);

		(
			Self {
				iter_a: a_left,
				iter_b: b_left,
			},
			Self {
				iter_a: a_right,
				iter_b: b_right,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		self.iter_a.into_iter().zip(self.iter_b.into_iter())
	}

	fn min_len(&self) -> usize {
		Ord::max(self.iter_a.min_len(), self.iter_b.min_len())
	}

	fn max_len(&self) -> usize {
		Ord::min(self.iter_a.max_len(), self.iter_b.max_len())
	}
}
