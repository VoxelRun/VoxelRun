//! Utilities for implementing collect

use core::marker::PhantomData;

use vr_core::utils::SendPtr;

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Reducer, UnindexedConsumer},
		IndexedParallelIterator, ParallelIterator,
	},
	ThreadPoolHandle,
};

/// Collects the result of the exact iterator into the specified vector.
///
/// This is called by [`IndexedParallelIterator::collect_into_vec`].
pub fn collect_into_vec<I, T, H>(pi: I, v: &mut Vec<T>)
where
	I: IndexedParallelIterator<H, Item = T>,
	T: Send,
	H: ThreadPoolHandle,
{
	v.truncate(0);
	let len = pi.len();
	collect_with_vec(v, len, |consumer| pi.drive(consumer))
}

/// Collects the results of the iterator into the specified vector.
///
/// Technically, this only works for [`IndexedParallelIterator`], but this is
/// faking a bit of specialisation until Rust can do that natively. Callers are
/// using [`opt_len`](ParallelIterator::opt_len) to find the length before
/// calling this, and only exact iterators will return anything but [`None`]
/// there.
///
/// Since the type system doesn't understand that contract, *any*
/// [`ParallelIterator`] is allowed here, and [`VecConsumer`] has to implement
/// [`UnindexedConsumer`]. That implementation panics [`unreachable!`] in case
/// there's a bug where we actually do try to use this unindexed.
pub fn special_extend<I, T, H>(pi: I, len: usize, v: &mut Vec<T>)
where
	I: ParallelIterator<H, Item = T>,
	T: Send,
	H: ThreadPoolHandle,
{
	collect_with_vec(v, len, |consumer| pi.drive_unindexed(consumer))
}

/// Create a consumer on the slice of memory we are collecting into.
///
/// The consumer needs to be used inside the scope function, and the complete
/// collect result will be passed back.
///
/// This method will verify the collect result, and panic if the slice was not
/// fully written into. Otherwise, in the successful case, the vector is complete
/// with the collection result.
fn collect_with_vec<T, F>(vec: &mut Vec<T>, len: usize, scope_fn: F)
where
	T: Send,
	F: FnOnce(VecConsumer<'_, T>) -> VecResult<'_, T>,
{
	vec.reserve(len);
	let result = scope_fn(VecConsumer::appender(vec, len));

	let actual_writes = result.len();
	debug_assert_eq!(
		actual_writes, len,
		"expected {} total writes, but got {}",
		len, actual_writes
	);
	result.release_ownership();
	let new_len = vec.len() + len;

	// Safety:
	//	- Extra memory was reserved
	//	- The `VecConsumer` and the number of actual_writes guarantee that all
	//	  new items are valid
	unsafe {
		vec.set_len(new_len);
	}
}

/// A consumer that puts all the results into a slice.
///
/// In memory this basically represents a raw slice.
struct VecConsumer<'data, T>
where
	T: Send,
{
	/// The pointer to the start of the slice.
	start: SendPtr<T>,
	/// The length of the slice
	len: usize,
	/// This is just a raw mutable reference to a slice of `T`, tell the compiler such
	_marker: PhantomData<&'data mut T>,
}

impl<'data, T> VecConsumer<'data, T>
where
	T: Send + 'data,
{
	/// Creates a new [`VecConsumer<T>`]. The start of the target slice being
	/// `start` and the length being `len`.
	///
	/// # Safety
	///
	/// - `start`: Must be a valid pointer
	/// - `len`: Must be valid in the context of the pointer, meaning:
	///   a) The whole memory span from `start` to `start + len` must be valid
	///      memory owned by the pointer
	/// - No other party is allowed to interfere with this memory
	unsafe fn new(start: *mut T, len: usize) -> Self {
		Self {
			start: SendPtr::new(start),
			len,
			_marker: PhantomData,
		}
	}

	/// Create a collector for `len` items in the unsed capacity of the vector.
	pub fn appender(vec: &mut Vec<T>, len: usize) -> Self {
		let start = vec.len();
		debug_assert!(vec.capacity() - start >= len);
		// Safety: We already made sure to have additional space allocated.
		// The pointer is derived from `Vec` directly, not through `Deref`,
		// so it has provenance over the whole allocation.
		unsafe { VecConsumer::new(vec.as_mut_ptr().add(start), len) }
	}
}

impl<'data, T, H> Consumer<T, H> for VecConsumer<'data, T>
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	type Result = VecResult<'data, T>;

	type Reducer = VecReducer;

	type Folder = VecResult<'data, T>;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let Self { start, len, .. } = self;
		// Safety: Index is validated, the lenght was validated beforhand
		unsafe {
			debug_assert!(index <= len);
			(
				Self::new(start.get(), index),
				Self::new(start.get().add(index), len - index),
				VecReducer,
			)
		}
	}

	fn into_folder(self) -> Self::Folder {
		Self::Folder {
			start: self.start,
			total_len: self.len,
			initialized_len: 0,
			invariant_lifetime: PhantomData,
		}
	}
}

/// Pretend to be unindexed for [`special_extend`], but this should never
/// actually get used that way...
impl<'data, T, H> UnindexedConsumer<T, H> for VecConsumer<'data, T>
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		unreachable!("VecConsumer must be indexed")
	}

	fn to_reducer(&self) -> Self::Reducer {
		VecReducer
	}
}

/// [`VecResult`] represents an initialized part of the target slice.
///
/// This is a proxy owner of the elements in the slice; when it drops, the
/// elements will be dropped, unless its ownership is released before then.
#[must_use = "unused result of a reduction"]
struct VecResult<'data, T> {
	/// This pointer retains provenance over the entire source array, so that it
	/// is mergeable using a [`VecReducer`]
	start: SendPtr<T>,
	/// The total length of this part of the source array
	total_len: usize,
	/// The initialized elements after [`start`](VecResult::start)
	initialized_len: usize,
	/// Lifetime invariance guarantees that the data flows from consumer to result.
	invariant_lifetime: PhantomData<&'data mut &'data [T]>,
}

impl<'data, T> VecResult<'data, T> {
	/// Returns the length of this [`VecResult<T>`].
	pub fn len(&self) -> usize {
		self.initialized_len
	}

	/// Release the ownership of the underlying slice so it will not be cleaned
	/// up upon dropping this.
	pub fn release_ownership(mut self) -> usize {
		let ret = self.initialized_len;
		self.initialized_len = 0;
		ret
	}
}

impl<'data, T> Drop for VecResult<'data, T> {
	fn drop(&mut self) {
		// Safety: If this was moved, `VecResult::release_ownership`, would have
		// been called
		unsafe {
			core::ptr::drop_in_place(core::slice::from_raw_parts_mut(
				self.start.get(),
				self.initialized_len,
			))
		}
	}
}

// Safety: !Send is a lint for mutable slices and pointers. Here it is insured by
// ourselves
unsafe impl<'data, T> Send for VecResult<'data, T> where T: Send {}

impl<'data, T> Folder<T> for VecResult<'data, T>
where
	T: Send + 'data,
{
	type Result = Self;

	fn consume(mut self, item: T) -> Self {
		debug_assert!(
			self.initialized_len < self.total_len,
			"too many values pushed to consumer"
		);

		// Safety: We own this, the creation guarantees validness.
		unsafe {
			self.start.get().add(self.initialized_len).write(item);
			self.initialized_len += 1;
		}
		self
	}

	fn complete(self) -> Self::Result {
		self
	}

	fn full(&self) -> bool {
		false
	}
}

/// A [`VecReducer`] combines adjacent chunks; the result must always be
/// contiguous so that it is one combined slice.
struct VecReducer;

impl<'data, T> Reducer<VecResult<'data, T>> for VecReducer {
	fn reduce(
		self,
		mut left: VecResult<'data, T>,
		right: VecResult<'data, T>,
	) -> VecResult<'data, T> {
		// Safety:
		//	- Merges the slices if they are adjacent and in left to right order,
		//	- Else drop the right piece now and total length will end up short
		//	  in the end, when the correctness of the collected result is asserted.
		//
		//	- left and right are guaranteed to be valid
		unsafe {
			let left_end = left.start.get().add(left.initialized_len);
			if left_end == right.start.get() {
				left.total_len += right.total_len;
				left.initialized_len += right.release_ownership()
			}
			left
		}
	}
}
