//! The functionality for the [`for_each`](ParallelIterator::for_each) iterator
//! consumer that operates like [`for_each`](core::iter::Iterator::for_each)
//! but in parallel.

use crate::{
	iter::{
		plumbing::{noop::NoopReducer, Consumer, Folder, UnindexedConsumer},
		ParallelIterator,
	},
	ThreadPoolHandle,
};

/// Execute the given closure for all Items of the `iter`ator
pub fn for_each<I, F, T, H>(iter: I, op: &F)
where
	H: ThreadPoolHandle,
	I: ParallelIterator<H, Item = T>,
	F: Fn(T) + Sync,
	T: Send,
{
	let consumer = ForEachConsumer { op };
	iter.drive_unindexed(consumer)
}

/// The consumer that consumes the elements in parallel
pub struct ForEachConsumer<'f, F> {
	/// the underlying closure that should be executed on each item
	op: &'f F,
}

impl<'f, F, T, H> Consumer<T, H> for ForEachConsumer<'f, F>
where
	H: ThreadPoolHandle,
	F: Fn(T) + Sync,
{
	type Result = ();
	type Reducer = NoopReducer;
	type Folder = ForEachConsumer<'f, F>;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, _index: usize) -> (Self, Self, Self::Reducer) {
		(
			UnindexedConsumer::<T, H>::split_off_left(&self),
			self,
			NoopReducer,
		)
	}

	fn into_folder(self) -> Self::Folder {
		self
	}
}

impl<'f, F, T> Folder<T> for ForEachConsumer<'f, F>
where
	F: Fn(T) + Sync,
{
	type Result = ();

	fn consume(self, item: T) -> Self {
		(self.op)(item);
		self
	}

	fn complete(self) -> Self::Result {}

	fn full(&self) -> bool {
		false
	}

	fn consume_iter<I>(self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		iter.into_iter().for_each(self.op);
		self
	}
}

impl<'f, F, T, H> UnindexedConsumer<T, H> for ForEachConsumer<'f, F>
where
	H: ThreadPoolHandle,
	F: Fn(T) + Sync,
{
	fn split_off_left(&self) -> Self {
		ForEachConsumer { op: self.op }
	}

	fn to_reducer(&self) -> Self::Reducer {
		NoopReducer
	}
}
