//! Module containing the different iterator consumer implemenations
pub mod for_each;
pub mod unzip;

pub(crate) mod collect_utils;
