//! The functionality for the [`unzip`](ParallelIterator::unzip) iterator
//! consumer that operates like [`unzip`](core::iter::Iterator::unzip)
//! but in parallel.

use crate::{
	iter::{
		plumbing::{Consumer, Folder, Reducer, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelExtend, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// Unzips the items of a [`ParallelIterator`] into a pair of arbitrary [`ParallelExtend`]
/// cotnainers.
///
/// This is called by [`ParallelIterator::unzip`]
pub fn unzip<I, A, B, FromA, FromB, H>(pi: I) -> (FromA, FromB)
where
	I: ParallelIterator<H, Item = (A, B)>,
	FromA: Default + Send + ParallelExtend<A>,
	FromB: Default + Send + ParallelExtend<B>,
	A: Send,
	B: Send,
	H: ThreadPoolHandle,
{
	execute(pi, Unzip)
}

#[allow(unused)] // => TODO: will be used for unzip_into_vecs
/// Unzips an [`IndexedParallelIterator`] into two arbitrary [`Consumer`]s.
pub fn unzip_indexed<I, A, B, CA, CB, H>(pi: I, left: CA, right: CB) -> (CA::Result, CB::Result)
where
	I: IndexedParallelIterator<H, Item = (A, B)>,
	CA: Consumer<A, H>,
	CB: Consumer<B, H>,
	A: Send,
	B: Send,
	H: ThreadPoolHandle,
{
	let consumer = UnzipConsumer {
		op: &Unzip,
		left,
		right,
	};
	pi.drive(consumer)
}

/// This trait abstracts the different ways we can "unzip" one parallel iterator
/// into two distinct consumers, which we can handle almost identically apart from
/// how to process the individual items.
trait UnzipOp<T>: Send + Sync {
	/// The type of item expected by the left consumer.
	type Left: Send;

	/// The type of item expected by the right consumer.
	type Right: Send;

	/// Consumes one item and feeds it to one or both of the underlying folders.
	fn consume<FA, FB>(&self, item: T, left: FA, right: FB) -> (FA, FB)
	where
		FA: Folder<Self::Left>,
		FB: Folder<Self::Right>;

	/// Reports whether this op may support indexed consumers
	/// - e.g. `true` for `unzip` where the item is passed through directly
	/// - e.g. `false` for `partition` where the sorting is not yet known.
	fn indexable() -> bool {
		false
	}
}

/// An [`UnzipOp`] that splits a tuple directly into two consumers.
struct Unzip;

impl<A, B> UnzipOp<(A, B)> for Unzip
where
	A: Send,
	B: Send,
{
	type Left = A;
	type Right = B;

	fn consume<FA, FB>(&self, item: (A, B), left: FA, right: FB) -> (FA, FB)
	where
		FA: Folder<Self::Left>,
		FB: Folder<Self::Right>,
	{
		(left.consume(item.0), right.consume(item.1))
	}

	fn indexable() -> bool {
		true
	}
}

/// Runs an unzip-like operation into default [`ParallelExtend`] collections.
fn execute<I, OP, FromA, FromB, H>(pi: I, op: OP) -> (FromA, FromB)
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	FromA: Default + Send + ParallelExtend<OP::Left>,
	FromB: Default + Send + ParallelExtend<OP::Right>,
	H: ThreadPoolHandle,
{
	let mut a = FromA::default();
	let mut b = FromB::default();
	execute_into(&mut a, &mut b, pi, op);
	(a, b)
}

/// Runs an unzip-like operation into [`ParallelExtend`] collections.
fn execute_into<I, OP, FromA, FromB, H>(a: &mut FromA, b: &mut FromB, pi: I, op: OP)
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	FromA: Default + Send + ParallelExtend<OP::Left>,
	FromB: Default + Send + ParallelExtend<OP::Right>,
	H: ThreadPoolHandle,
{
	let iter = UnzipA { base: pi, op, b };
	a.par_extend(iter);
}

/// A fake iterator to intercep the [`Consumer`] for type `A`
struct UnzipA<'b, I, OP, FromB> {
	/// The Base [`ParallelIterator`]
	base: I,
	/// The [`UnzipOp`] that is responsible for distributing items
	op: OP,
	/// The mutable reference to the other result that is not of type `FromA`
	b: &'b mut FromB,
}

impl<'b, I, OP, FromB, H> IntoParallelIterator<H> for UnzipA<'b, I, OP, FromB>
where
	FromB: Send + ParallelExtend<OP::Right>,
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

/// A fake iterator to intercept the `Consumer` for type `B`.
struct UnzipB<'r, I, OP, CA, H>
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	CA: UnindexedConsumer<OP::Left, H>,
	CA::Result: 'r,
	H: ThreadPoolHandle,
{
	/// The Base [`ParallelIterator`]
	base: I,
	/// The [`UnzipOp`] that is responsible for distributing items
	op: OP,
	/// The consumer for the `A` type
	left_consumer: CA,
	/// The result of the `A` type
	left_result: &'r mut Option<CA::Result>,
}

impl<'r, I, OP, CA, H> IntoParallelIterator<H> for UnzipB<'r, I, OP, CA, H>
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	CA: UnindexedConsumer<OP::Left, H>,
	H: ThreadPoolHandle,
{
	iterator_into_iter_impl!();
}

impl<'b, I, OP, FromB, H> ParallelIterator<H> for UnzipA<'b, I, OP, FromB>
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	FromB: Send + ParallelExtend<OP::Right>,
	H: ThreadPoolHandle,
{
	type Item = OP::Left;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let mut result = None;
		{
			let iter = UnzipB {
				base: self.base,
				op: self.op,
				left_consumer: consumer,
				left_result: &mut result,
			};

			self.b.par_extend(iter);
		}

		result.expect("unzip consumers didn't execute!")
	}

	fn opt_len(&self) -> Option<usize> {
		if OP::indexable() {
			self.base.opt_len()
		} else {
			None
		}
	}
}

impl<'r, I, OP, CA, H> ParallelIterator<H> for UnzipB<'r, I, OP, CA, H>
where
	I: ParallelIterator<H>,
	OP: UnzipOp<I::Item>,
	CA: UnindexedConsumer<OP::Left, H>,
	H: ThreadPoolHandle,
{
	type Item = OP::Right;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		let consumer = UnzipConsumer {
			op: &self.op,
			left: self.left_consumer,
			right: consumer,
		};

		let result = self.base.drive_unindexed(consumer);
		*self.left_result = Some(result.0);
		result.1
	}
}

/// A [`Consumer`] for [`UnzipOp`]s
struct UnzipConsumer<'a, OP, CA, CB> {
	/// The [`UnzipOp`] in question.
	op: &'a OP,
	/// The left half of the zip.
	left: CA,
	/// The right half of the zip.
	right: CB,
}

/// A [`Reducer`] for an [`UnzipOp`]
struct UnzipReducer<RA, RB> {
	/// The left half, or the [`Reducer`] for `A`
	left: RA,
	/// The right half, or the [`Reducer`] for `B`
	right: RB,
}

/// A [`Folder`] for an [`UnzipOp`]
struct UnzipFolder<'a, OP, FA, FB> {
	/// The [`UnzipOp`]
	op: &'a OP,
	/// The left half [`Folder`], or the [`Folder`] for `A`.
	left: FA,
	/// The right half [`Folder`], or the [`Folder`] for `B`.
	right: FB,
}

impl<'a, T, OP, CA, CB, H> Consumer<T, H> for UnzipConsumer<'a, OP, CA, CB>
where
	OP: UnzipOp<T>,
	CA: Consumer<OP::Left, H>,
	CB: Consumer<OP::Right, H>,
	H: ThreadPoolHandle,
{
	type Result = (CA::Result, CB::Result);

	type Reducer = UnzipReducer<CA::Reducer, CB::Reducer>;

	type Folder = UnzipFolder<'a, OP, CA::Folder, CB::Folder>;

	fn full(&self) -> bool {
		self.left.full() && self.right.full()
	}

	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer) {
		let (left1, left2, left_reducer) = self.left.split_at(index);
		let (right1, right2, right_reducer) = self.right.split_at(index);
		(
			UnzipConsumer {
				op: self.op,
				left: left1,
				right: right1,
			},
			UnzipConsumer {
				op: self.op,
				left: left2,
				right: right2,
			},
			UnzipReducer {
				left: left_reducer,
				right: right_reducer,
			},
		)
	}

	fn into_folder(self) -> Self::Folder {
		UnzipFolder {
			op: self.op,
			left: self.left.into_folder(),
			right: self.right.into_folder(),
		}
	}
}

impl<'a, T, OP, CA, CB, H> UnindexedConsumer<T, H> for UnzipConsumer<'a, OP, CA, CB>
where
	OP: UnzipOp<T>,
	CA: UnindexedConsumer<OP::Left, H>,
	CB: UnindexedConsumer<OP::Right, H>,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		UnzipConsumer {
			op: self.op,
			left: self.left.split_off_left(),
			right: self.right.split_off_left(),
		}
	}

	fn to_reducer(&self) -> Self::Reducer {
		UnzipReducer {
			left: self.left.to_reducer(),
			right: self.right.to_reducer(),
		}
	}
}

impl<'a, T, OP, FA, FB> Folder<T> for UnzipFolder<'a, OP, FA, FB>
where
	OP: UnzipOp<T>,
	FA: Folder<OP::Left>,
	FB: Folder<OP::Right>,
{
	type Result = (FA::Result, FB::Result);

	fn consume(self, item: T) -> Self {
		let (left, right) = self.op.consume(item, self.left, self.right);
		UnzipFolder {
			op: self.op,
			left,
			right,
		}
	}

	fn complete(self) -> Self::Result {
		(self.left.complete(), self.right.complete())
	}

	fn full(&self) -> bool {
		self.left.full() && self.right.full()
	}
}

impl<A, B, RA, RB> Reducer<(A, B)> for UnzipReducer<RA, RB>
where
	RA: Reducer<A>,
	RB: Reducer<B>,
{
	fn reduce(self, left: (A, B), right: (A, B)) -> (A, B) {
		(
			self.left.reduce(left.0, right.0),
			self.right.reduce(left.1, right.1),
		)
	}
}
