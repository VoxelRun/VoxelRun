//! Implementations to extend a collections using [`ParallelExtend`]

use core::hash::{BuildHasher, Hash};
use std::{
	borrow::Cow,
	collections::{BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, LinkedList, VecDeque},
	ffi::{OsStr, OsString},
};

use crate::ThreadPoolHandle;

use super::{
	consumers::collect_utils::special_extend,
	plumbing::{noop::NoopConsumer, Consumer, Folder, Reducer, UnindexedConsumer},
	IntoParallelIterator, ParallelExtend, ParallelIterator,
};

/// A type that represents to optional values, similar to [`Result`] but both
/// values are valid
pub enum Either<T, U> {
	/// The first possible outcome
	Left {
		/// The underlying value
		val: T,
	},
	/// The second possible outcome
	Right {
		/// The underlying value
		val: U,
	},
}

/// Extend a collection with a [`ParallelIterator`], preallocating the necessary
/// size.
macro_rules! extend_reserved {
	($self:ident, $par_iter:ident, $len:ident) => {{
		let vecs = fast_collect($par_iter);
		$self.reserve($len(&vecs));
		extend!($self with vecs)
	}};
	($self:ident, $par_iter:ident) => {
		extend_reserved!($self, $par_iter, len)
	}
}

/// Extend a collection with a [`ParallelIterator`], if the size of the collection
/// can be changed beforehand, use [`extend_reserved`] instead
macro_rules! extend {
	($self:ident with $vecs:ident) => {
		match $vecs {
			Either::Left { val: vec } => $self.extend(vec),
			Either::Right { val: vecs } => for vec in vecs {
				$self.extend(vec)
			}
		}
	};

	($self:ident, $par_iter:ident) => {{
		let vecs = fast_collect($par_iter);
		extend!($self with vecs)
	}};
}

/// Calculate the length of the structure given back by [`fast_collect`] in
/// items.
fn len<T>(vecs: &Either<Vec<T>, LinkedList<Vec<T>>>) -> usize {
	match vecs {
		Either::Left { val } => val.len(),
		Either::Right { val } => val.iter().map(Vec::len).sum(),
	}
}

/// Calculate the length of the structure given back by [`fast_collect`] over strings
fn string_len<T>(vecs: &Either<Vec<T>, LinkedList<Vec<T>>>) -> usize
where
	T: AsRef<str>,
{
	match vecs {
		Either::Left { val: vec } => vec.iter().map(AsRef::as_ref).map(str::len).sum(),
		Either::Right { val: vecs } => vecs.iter().flatten().map(AsRef::as_ref).map(str::len).sum(),
	}
}

/// Calculate the length of the structure given back by [`fast_collect`] over OSstrings.
fn osstring_len<T>(vecs: &Either<Vec<T>, LinkedList<Vec<T>>>) -> usize
where
	T: AsRef<OsStr>,
{
	match vecs {
		Either::Left { val: vec } => vec.iter().map(AsRef::as_ref).map(OsStr::len).sum(),
		Either::Right { val: vecs } => vecs
			.iter()
			.flatten()
			.map(AsRef::as_ref)
			.map(OsStr::len)
			.sum(),
	}
}

/// Collect a [`ParallelIterator`] into either a [`Vec`] if its an
/// [`IndexedParallelIterator`](super::IndexedParallelIterator) or a [`LinkedList<Vec>`].
pub fn fast_collect<I, T, H>(par_iter: I) -> Either<Vec<T>, LinkedList<Vec<T>>>
where
	I: IntoParallelIterator<H, Item = T>,
	T: Send,
	H: ThreadPoolHandle,
{
	let par_iter = par_iter.into_par_iter();
	match par_iter.opt_len() {
		Some(len) => {
			let mut vec = Vec::new();
			special_extend(par_iter, len, &mut vec);
			Either::Left { val: vec }
		}
		None => Either::Right {
			val: par_iter.drive_unindexed(ListVecConsumer),
		},
	}
}

/// A [`Consumer`] that reduces a [`ParallelIterator<Item = T>`] down to a [`LinkedList<Vec<T>>`]
struct ListVecConsumer;

/// A folder that merges all items on a [`Vec`] and completes by putting the
/// [`Vec`] in a [`LinkedList`]
struct ListVecFolder<T> {
	/// The [`Vec`] that will used in the list
	vec: Vec<T>,
}

/// A [`Reducer`] that merges two [`LinkedList`] into one, left to right
struct ListReducer;

impl<T> Reducer<LinkedList<T>> for ListReducer {
	fn reduce(self, mut left: LinkedList<T>, mut right: LinkedList<T>) -> LinkedList<T> {
		left.append(&mut right);
		left
	}
}

impl<T, H> Consumer<T, H> for ListVecConsumer
where
	T: Send,
	H: ThreadPoolHandle,
{
	type Result = LinkedList<Vec<T>>;

	type Reducer = ListReducer;

	type Folder = ListVecFolder<T>;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, _index: usize) -> (Self, Self, Self::Reducer) {
		(Self, Self, ListReducer)
	}

	fn into_folder(self) -> Self::Folder {
		ListVecFolder { vec: Vec::new() }
	}
}

impl<T, H> UnindexedConsumer<T, H> for ListVecConsumer
where
	T: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self
	}

	fn to_reducer(&self) -> Self::Reducer {
		ListReducer
	}
}

impl<T> Folder<T> for ListVecFolder<T> {
	type Result = LinkedList<Vec<T>>;

	fn consume(mut self, item: T) -> Self {
		self.vec.push(item);
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.vec.extend(iter);
		self
	}

	fn complete(self) -> Self::Result {
		let mut list = LinkedList::new();
		if !self.vec.is_empty() {
			list.push_back(self.vec);
		}
		list
	}

	fn full(&self) -> bool {
		false
	}
}

impl<T> ParallelExtend<T> for Vec<T>
where
	T: Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: super::IntoParallelIterator<H, Item = T>,
		H: crate::ThreadPoolHandle,
	{
		let iter = par_iter.into_par_iter();
		match iter.opt_len() {
			Some(len) => special_extend(iter, len, self),
			None => {
				let list: LinkedList<Vec<T>> = iter.drive_unindexed(ListVecConsumer);
				self.reserve(list.iter().map(Vec::len).sum());
				for mut other in list {
					self.append(&mut other)
				}
			}
		}
	}
}

impl<'a, T> ParallelExtend<&'a T> for Vec<T>
where
	T: 'a + Copy + Send + Sync,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		self.par_extend(par_iter.into_par_iter().copied())
	}
}

/// Collapses all unit items from a [`ParallelIterator`] into one.
impl ParallelExtend<()> for () {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = ()>,
		H: ThreadPoolHandle,
	{
		par_iter.into_par_iter().drive_unindexed(NoopConsumer)
	}
}

impl<T> ParallelExtend<T> for LinkedList<T>
where
	T: Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		let mut list = par_iter.into_par_iter().drive_unindexed(ListConsumer);
		self.append(&mut list);
	}
}

impl<'a, T> ParallelExtend<&'a T> for LinkedList<T>
where
	T: 'a + Send + Copy + Sync,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		self.par_extend(par_iter.into_par_iter().copied())
	}
}

impl<T> ParallelExtend<T> for VecDeque<T>
where
	T: Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<'a, T> ParallelExtend<&'a T> for VecDeque<T>
where
	T: 'a + Send + Sync + Copy,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<T> ParallelExtend<T> for BinaryHeap<T>
where
	T: Send + Ord,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<'a, T> ParallelExtend<&'a T> for BinaryHeap<T>
where
	T: Send + Ord + Sync + Copy,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<T> ParallelExtend<T> for BTreeSet<T>
where
	T: Send + Ord,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		extend!(self, par_iter)
	}
}

impl<'a, T> ParallelExtend<&'a T> for BTreeSet<T>
where
	T: 'a + Send + Ord + Copy + Sync,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		extend!(self, par_iter)
	}
}

impl<K, V> ParallelExtend<(K, V)> for BTreeMap<K, V>
where
	K: Send + Ord,
	V: Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = (K, V)>,
		H: ThreadPoolHandle,
	{
		extend!(self, par_iter)
	}
}

impl<'a, K, V> ParallelExtend<(&'a K, &'a V)> for BTreeMap<K, V>
where
	K: 'a + Send + Ord + Copy + Sync,
	V: 'a + Send + Copy + Sync,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = (&'a K, &'a V)>,
		H: ThreadPoolHandle,
	{
		extend!(self, par_iter)
	}
}

impl<T, S> ParallelExtend<T> for HashSet<T, S>
where
	T: Eq + Hash + Send,
	S: BuildHasher + Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<'a, T, S> ParallelExtend<&'a T> for HashSet<T, S>
where
	T: 'a + Eq + Hash + Send + Copy + Sync,
	S: BuildHasher + Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a T>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<K, V, S> ParallelExtend<(K, V)> for HashMap<K, V, S>
where
	K: Eq + Hash + Send,
	V: Send,
	S: BuildHasher + Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = (K, V)>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

impl<'a, K, V, S> ParallelExtend<(&'a K, &'a V)> for HashMap<K, V, S>
where
	K: 'a + Copy + Eq + Hash + Send + Sync,
	V: 'a + Copy + Send + Sync,
	S: BuildHasher + Send,
{
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = (&'a K, &'a V)>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter)
	}
}

/// A [`Consumer`] that produces a [`LinkedList`]
struct ListConsumer;

/// A [`Folder`] that folds into a [`LinkedList`]
struct ListFolder<T> {
	/// The [`LinkedList`] being built.
	list: LinkedList<T>,
}

impl<T, H> Consumer<T, H> for ListConsumer
where
	T: Send,
	H: ThreadPoolHandle,
{
	type Result = LinkedList<T>;

	type Reducer = ListReducer;

	type Folder = ListFolder<T>;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, _index: usize) -> (Self, Self, Self::Reducer) {
		(Self, Self, ListReducer)
	}

	fn into_folder(self) -> Self::Folder {
		ListFolder {
			list: LinkedList::new(),
		}
	}
}

impl<T, H> UnindexedConsumer<T, H> for ListConsumer
where
	T: Send,
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self
	}

	fn to_reducer(&self) -> Self::Reducer {
		ListReducer
	}
}

impl<T> Folder<T> for ListFolder<T> {
	type Result = LinkedList<T>;

	fn consume(mut self, item: T) -> Self {
		self.list.push_back(item);
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = T>,
	{
		self.list.extend(iter);
		self
	}

	fn complete(self) -> Self::Result {
		self.list
	}

	fn full(&self) -> bool {
		false
	}
}

impl ParallelExtend<char> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = char>,
		H: ThreadPoolHandle,
	{
		let list = par_iter.into_par_iter().drive_unindexed(ListStringConsumer);
		self.reserve(list.iter().map(String::len).sum());
		self.extend(list);
	}
}

impl<'a> ParallelExtend<&'a char> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a char>,
		H: ThreadPoolHandle,
	{
		self.par_extend(par_iter.into_par_iter().copied())
	}
}

impl<'a> ParallelExtend<&'a str> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a str>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, string_len)
	}
}

impl ParallelExtend<Box<str>> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = Box<str>>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, string_len)
	}
}

impl<'a> ParallelExtend<Cow<'a, str>> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = Cow<'a, str>>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, string_len)
	}
}

impl ParallelExtend<String> for String {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = String>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, string_len)
	}
}

impl<'a> ParallelExtend<&'a OsStr> for OsString {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = &'a OsStr>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, osstring_len)
	}
}

impl<'a> ParallelExtend<Cow<'a, OsStr>> for OsString {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = Cow<'a, OsStr>>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, osstring_len)
	}
}

impl ParallelExtend<OsString> for OsString {
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = OsString>,
		H: ThreadPoolHandle,
	{
		extend_reserved!(self, par_iter, osstring_len)
	}
}

/// A [`Consumer`] that results in a [`LinkedList<String>`]
struct ListStringConsumer;

impl<H> Consumer<char, H> for ListStringConsumer
where
	H: ThreadPoolHandle,
{
	type Result = LinkedList<String>;

	type Reducer = ListReducer;

	type Folder = ListStringFolder;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, _index: usize) -> (Self, Self, Self::Reducer) {
		(Self, Self, ListReducer)
	}

	fn into_folder(self) -> Self::Folder {
		ListStringFolder {
			string: String::new(),
		}
	}
}

impl<H> UnindexedConsumer<char, H> for ListStringConsumer
where
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self
	}

	fn to_reducer(&self) -> Self::Reducer {
		ListReducer
	}
}

/// A [`Folder`] that collect values into a [`LinkedList<String>`]
struct ListStringFolder {
	/// The [`String`] being filled up
	string: String,
}

impl Folder<char> for ListStringFolder {
	type Result = LinkedList<String>;

	fn consume(mut self, item: char) -> Self {
		self.string.push(item);
		self
	}

	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = char>,
	{
		self.string.extend(iter);
		self
	}
	fn complete(self) -> Self::Result {
		let mut list = LinkedList::new();
		if !self.string.is_empty() {
			list.push_back(self.string);
		}
		list
	}

	fn full(&self) -> bool {
		false
	}
}
