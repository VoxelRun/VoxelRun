//! Implementations for [`FromParallelIterator`] for standard collections
use core::hash::{BuildHasher, Hash};
use std::{
	borrow::Cow,
	collections::{BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, LinkedList, VecDeque},
	ffi::{OsStr, OsString},
};

use crate::ThreadPoolHandle;

use super::{
	plumbing::noop::NoopConsumer, FromParallelIterator, IntoParallelIterator, ParallelExtend,
	ParallelIterator,
};

/// Collect into a collection by [extending](ParallelExtend) it with a
/// [`ParallelIterator`]
fn collect_extended<C, I, H>(par_iter: I) -> C
where
	I: IntoParallelIterator<H>,
	H: ThreadPoolHandle,
	C: ParallelExtend<I::Item> + Default,
{
	let mut collection = C::default();
	collection.par_extend(par_iter);
	collection
}

impl<'a, C, T> FromParallelIterator<T> for Cow<'a, C>
where
	C: ToOwned + ?Sized,
	C::Owned: FromParallelIterator<T>,
	T: Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		Cow::Owned(C::Owned::from_par_iter(par_iter))
	}
}

impl<T> FromParallelIterator<T> for Vec<T>
where
	T: Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: super::IntoParallelIterator<H, Item = T>,
		H: crate::ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<T> FromParallelIterator<T> for VecDeque<T>
where
	T: Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<T> FromParallelIterator<T> for BinaryHeap<T>
where
	T: Ord + Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<T> FromParallelIterator<T> for BTreeSet<T>
where
	T: Ord + Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<K, V> FromParallelIterator<(K, V)> for BTreeMap<K, V>
where
	K: Ord + Send,
	V: Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = (K, V)>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<T, S> FromParallelIterator<T> for HashSet<T, S>
where
	T: Eq + Hash + Send,
	S: BuildHasher + Send + Default,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<K, V, S> FromParallelIterator<(K, V)> for HashMap<K, V, S>
where
	K: Eq + Hash + Send,
	V: Send,
	S: BuildHasher + Send + Default,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = (K, V)>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<T> FromParallelIterator<T> for LinkedList<T>
where
	T: Send,
{
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: super::IntoParallelIterator<H, Item = T>,
		H: crate::ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl FromParallelIterator<char> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = char>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<'a> FromParallelIterator<&'a char> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = &'a char>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<'a> FromParallelIterator<&'a str> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = &'a str>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<'a> FromParallelIterator<Cow<'a, str>> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = Cow<'a, str>>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl FromParallelIterator<Box<str>> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = Box<str>>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl FromParallelIterator<String> for String {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = String>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<'a> FromParallelIterator<&'a OsStr> for OsString {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = &'a OsStr>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl<'a> FromParallelIterator<Cow<'a, OsStr>> for OsString {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = Cow<'a, OsStr>>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl FromParallelIterator<OsString> for OsString {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = OsString>,
		H: ThreadPoolHandle,
	{
		collect_extended(par_iter)
	}
}

impl FromParallelIterator<()> for () {
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = ()>,
		H: ThreadPoolHandle,
	{
		par_iter.into_par_iter().drive_unindexed(NoopConsumer)
	}
}
