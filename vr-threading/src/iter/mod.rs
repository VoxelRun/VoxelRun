//! [`ParallelIterator`] utilties, TBA
use core::ops::RangeBounds;
use std::collections::LinkedList;

use crate::{handle::DefaultHandle, ThreadPoolHandle};

use self::{
	adaptors::{
		chain::Chain, cloned::Cloned, copied::Copied, enumerate::Enumerate, filter::Filter,
		filter_map::FilterMap, flat_map::FlatMap, flat_map_iter::FlatMapIter, flatten::Flatten,
		flatten_iter::FlattenIter, inspect::Inspect, intersperse::Intersperse, map::Map,
		map_init::MapInit, map_with::MapWith, skip::Skip, skip_any::SkipAny,
		skip_any_while::SkipAnyWhile, zip::Zip,
	},
	consumers::{collect_utils, for_each, unzip},
	plumbing::{Consumer, ProducerCallback, UnindexedConsumer},
};

mod adaptors;
mod consumers;
mod extend;
mod from_par_iter;
pub mod plumbing;

/// Parallel version of the [standard iterator trait](core::iter::Iterator).
///
/// The combinators on this trait are available for **all** types that implement
/// [`ParallelIterator`]. Additional methods can be found on the
/// [`IndexedParallelIterator`] trait; those methods are only available for
/// iterators which know their size in advance and can be accessed in an
/// arbitrary order (so, e.g., after invoking `filter`, those methods become
/// unavailable).
///
/// `H` describes which threadpool should be used for executing the parallel
/// iterators should be executed on. For more information take a look at
/// [`ThreadPoolHandle`].
pub trait ParallelIterator<H>
where
	H: ThreadPoolHandle,
	Self: Sized + Send,
{
	/// The type of item that this parallel iterator produces. For example,
	/// if you use the [`for_each`](ParallelIterator::for_each) method, this is
	/// the type of item that your closure will be invoked with.
	type Item: Send;

	/// Method that is used to define the behaviour of this parallel iterator.
	/// You should not need to call this directly.
	///
	/// This method causes `self` to start producing items and feeds them to
	/// the `consumer` one by one. It may split the produced items and the
	/// consumer before doing so that they may be processed in parallel.
	///
	/// Somewhat analogous to [`next`](Iterator::next) on normal [`Iterator`]s.
	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>;

	/// Intersperses clones of an element between items of this iterator.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IndexedParallelIterator, IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let result = [1, 2, 3]
	///             .into_default_par_iter()
	///             .intersperse(-1)
	///             .collect::<Vec<_>>();
	///         assert_eq!(&result, &[1, -1, 2, -1, 3])
	///     })
	///     .expect("This should not fail")
	///
	/// ```
	fn intersperse(self, item: Self::Item) -> Intersperse<Self, H>
	where
		Self::Item: Clone,
	{
		Intersperse::new(self, item)
	}

	/// Executes an `op` on each item produced by the iterator, in parallel.
	///
	/// # Examples
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator}, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     (0..100).into_default_par_iter().for_each(|x| println!("{x:?}"));
	/// }).expect("Should not fail");
	/// ```
	/// The items will be processed in parallel so the order of this programs
	/// output will be arbitrary.
	fn for_each<F>(self, op: F)
	where
		F: Fn(Self::Item) + Sync + Send,
	{
		for_each::for_each(self, &op)
	}

	/// Returns the number of items produced by this iterator, if known
	/// statically. This can be used by consumers to trigger special fast paths.
	/// Therefore, if `Some(_)` is returned, this iterator must only use the
	/// (indexed) [`Consumer`] methods when driving a consumer, such as
	/// [`split_at`](Consumer::split_at). Calling
	/// [`UnindexedConsumer::split_off_left`] or other [`UnindexedConsumer`]
	/// methods — or returning an inaccurate value — may result in panics.
	fn opt_len(&self) -> Option<usize> {
		None
	}

	/// Applies `op` to each item of this iterator, producing a new iterator
	/// with the results.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let results = (0..10)
	///         .into_default_par_iter()
	///         .map(|n| n * n)
	///         .collect::<Vec<_>>();
	///     assert_eq!(&results, &[0, 1, 4, 9, 16, 25, 36, 49, 64, 81])
	/// }).expect("This should not fail")
	/// ```
	fn map<F, R>(self, op: F) -> Map<Self, F, H>
	where
		F: Fn(Self::Item) -> R + Sync + Send,
		R: Send,
	{
		Map::new(self, op)
	}

	/// Applies `map_op` to the given init value with each item of this iterator,
	/// producing a new iterator with the results.
	///
	/// The `init` value will be cloned only as needed to be paired with the group
	/// of items in each job. It does not require the type to be [`Sync`].
	///
	/// # Examples
	/// ```
	///
	/// use std::sync::mpsc::channel;
	///
	/// use vr_threading::{
	///     iter::{IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let (sender, receiver) = channel();
	///         let a = (0..5)
	///             .into_default_par_iter()
	///             .map_with(sender, |s, x| {
	///                 s.send(x).unwrap();
	///                 x
	///             })
	///             .collect::<Vec<_>>();
	///         let mut b = receiver.iter().collect::<Vec<_>>();
	///         b.sort();
	///         assert_eq!(a, b)
	///     })
	///     .expect("This should not fail");
	/// ```
	fn map_with<T, F, R>(self, item: T, map_op: F) -> MapWith<Self, H, T, F, R>
	where
		F: Fn(&mut T, Self::Item) -> R + Sync + Send,
		T: Send + Clone,
		R: Send,
	{
		MapWith::new(self, item, map_op)
	}

	/// Applies `map_op` to a value returned by `init` with each item of this
	/// iterator, producing a new iterator with the result.
	///
	/// The `init` function will be called only as needed for a value to be
	/// paired with the group of items in each job. There is no contraint
	/// on the returned type at all.
	///
	/// # Examples
	///
	/// ```
	/// use rand::Rng;
	/// use vr_threading::{
	///     iter::{IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let a = (0..1_000_000i32)
	///             .into_default_par_iter()
	///             .map_init(
	///                 || rand::thread_rng(),
	///                 |rng, x| if rng.gen() { -x } else { x },
	///             )
	///             .collect::<Vec<_>>();
	///
	///         // This could fail... eventually...
	///         assert!(a.iter().any(|&x| x < 0));
	///         assert!(a.iter().any(|&x| x > 0));
	///     })
	///     .expect("This should not fail");
	/// ```
	fn map_init<INIT, T, F, R>(self, init: INIT, map_op: F) -> MapInit<Self, INIT, T, F, R, H>
	where
		INIT: Fn() -> T + Sync + Send,
		F: Fn(&mut T, Self::Item) -> R + Sync + Send,
		R: Send,
	{
		MapInit::new(self, init, map_op)
	}

	/// Applies `filter_op` to each item of this iterator, producing a new
	/// iterator with only the items that gave `true` results.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{IntoDefaultParallelRefIterator, ParallelIterator}, threadpool};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let source = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
	///     let result = source.default_par_iter().filter(|&i| i % 3 == 0).copied().collect::<Vec<_>>();
	///     assert_eq!(&result, &[0, 3, 6, 9]);
	/// }).expect("This should not fail");
	/// ```
	fn filter<P>(self, filter_op: P) -> Filter<Self, P, H>
	where
		P: Fn(&Self::Item) -> bool + Send + Sync,
	{
		Filter::new(self, filter_op)
	}

	/// Applies `filter_map_op` to each item of this iterator, producing a new
	/// iterator with only the items that gave a `Some(_)` results. The new
	/// iterator will yield over the new items mapped.
	///
	/// # Examples
	/// ```
	///
	/// use vr_threading::{iter::{IntoDefaultParallelRefIterator, ParallelIterator}, threadpool};
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let source = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
	///     let result = source.default_par_iter().filter_map(|&i| if i % 3 == 0 { Some(i / 3) } else { None }).collect::<Vec<_>>();
	///     assert_eq!(&result, &[0, 1, 2, 3]);
	/// }).expect("This should not fail");
	/// ```
	fn filter_map<P, R>(self, filter_map_op: P) -> FilterMap<Self, P, R, H>
	where
		P: Fn(Self::Item) -> Option<R> + Send + Sync,
		R: Send,
	{
		FilterMap::new(self, filter_map_op)
	}

	/// Does something with each element of an iterator, passing the value on.
	///
	/// When using iterators, you will often chain several of them together.  
	/// While working on such code, you might want to check out what is happening
	/// at various parts in the pipeline. To do that, insert a call to
	/// [`inspect`](ParallelIterator::inspect).
	///
	/// It is more common for [`inspect`](ParallelIterator::inspect) to be used
	/// as a debugging tool than to exist in your final code, but applications
	/// may find it useful in certain situations when errors need to be logged
	/// before being discarded.
	///
	/// # Examples
	/// ```
	/// use core::{sync::atomic::{AtomicUsize, Ordering}, num::NonZeroUsize};
	///
	/// use vr_threading::{
	///     iter::{IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool,
	/// };
	///
	/// threadpool::ThreadPoolBuilder::new()
	///     .num_threads(NonZeroUsize::new(1).unwrap())
	///     .spawn(|| {
	///         let sum = AtomicUsize::new(0);
	///         let other_sum = AtomicUsize::new(0);
	///         (0..10usize)
	///             .into_default_par_iter()
	///             .inspect(|&i| {
	///                 sum.fetch_add(i, Ordering::Relaxed);
	///                 println!("Vale before modification was {i}")
	///             })
	///             .map(|i| i * 2)
	///             .for_each(|i| {
	///                 other_sum.fetch_add(i, Ordering::Relaxed);
	///                 println!("Value after modification was {i}");
	///             });
	///
	///         let (sum, other_sum) = (
	///             sum.load(Ordering::Relaxed),
	///             other_sum.load(Ordering::Relaxed),
	///         );
	///
	///         assert_eq!(2 * sum, other_sum);
	///     })
	///     .expect("This should not fail")
	/// ```
	///
	/// This will print:
	/// ```norust
	/// Vale before modification was 0
	/// Value after modification was 0
	/// Vale before modification was 1
	/// Value after modification was 2
	/// Vale before modification was 2
	/// Value after modification was 4
	/// Vale before modification was 3
	/// Value after modification was 6
	/// Vale before modification was 4
	/// Value after modification was 8
	/// Vale before modification was 5
	/// Value after modification was 10
	/// Vale before modification was 6
	/// Value after modification was 12
	/// Vale before modification was 7
	/// Value after modification was 14
	/// Vale before modification was 8
	/// Value after modification was 16
	/// Vale before modification was 9
	/// Value after modification was 18
	/// ```
	///
	/// Keep in mind, the output is only in order as the threadpool only uses
	/// one thread. The order of the prints is a bit random with multiple threads.
	/// Whilst an item flows through the iterator on one thread, on another thread
	/// another item passes through the same chain, causing their outputs to be mixed
	/// in some cases.
	fn inspect<F>(self, inspect_op: F) -> Inspect<Self, F, H>
	where
		F: Fn(&Self::Item) + Sync + Send,
	{
		Inspect::new(self, inspect_op)
	}

	/// Takes two iterators and creates a new iterator over both.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let results = (0..5)
	///         .into_default_par_iter()
	///         .chain(5..10)
	///         .collect::<Vec<_>>();
	///     assert_eq!(&results, &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	/// }).expect("This should not fail")
	/// ```
	fn chain<I>(self, other: I) -> Chain<Self, I::Iter, H>
	where
		I: IntoParallelIterator<H, Item = Self::Item>,
	{
		Chain::new(self, other.into_par_iter())
	}

	/// Creates an iterator which copies all its items. This may be useful when
	/// you have an iterator over `&T`, but you need `T`, and that type implements
	/// [`Copy`].
	///
	/// # Examples
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator, IntoDefaultParallelRefIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let source = vec![10, 1, 2, 4, 5 , 6, 9];
	///     let results = source
	///         .default_par_iter()
	///         .copied()
	///         .collect::<Vec<_>>();
	///     assert_eq!(source, results)
	/// }).expect("This should not fail")
	/// ```
	fn copied<'a, T>(self) -> Copied<Self, H>
	where
		T: 'a + Copy + Send,
		Self: ParallelIterator<H, Item = &'a T>,
	{
		Copied::new(self)
	}

	/// Creates an iterator which clones all its items, this may be useful when
	/// you have an iterator over `&T`, but when you need `T`, and that type
	/// implements [`Clone`].
	///
	/// # Examples
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator, IntoDefaultParallelRefIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let source = vec![10, 1, 2, 4, 5 , 6, 9];
	///     let results = source
	///         .default_par_iter()
	///         .cloned()
	///         .collect::<Vec<_>>();
	///     assert_eq!(source, results)
	/// }).expect("This should not fail")
	/// ```
	fn cloned<'a, T>(self) -> Cloned<Self, H>
	where
		T: 'a + Copy + Send,
		Self: ParallelIterator<H, Item = &'a T>,
	{
		Cloned::new(self)
	}

	/// An adaptor that flattens parallel-iterable `Items` into one large iterator.
	///
	/// See also [`flatten_iter`](ParallelIterator::flatten_iter).
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::iter::{IntoDefaultParallelRefIterator, ParallelIterator};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let source = vec![vec![1, 2], vec![3, 4], vec![5, 6, 7], vec![8, 9, 10]];
	///         let results = source.default_par_iter().flatten().copied().collect::<Vec<_>>();
	///         assert_eq!(&results, &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	///     })
	///     .expect("This should not fail")
	/// ```
	fn flatten(self) -> Flatten<Self, H>
	where
		Self::Item: IntoParallelIterator<H>,
	{
		Flatten::new(self)
	}

	/// An adaptor that flattens serial-iterable [`Item`]s into one large iterator.
	///
	/// See also [`flatten`](ParallelIterator::flatten).
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::iter::{
	///     IntoDefaultParallelRefIterator, ParallelIterator,
	/// };
	/// vr_threading::threadpool::ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let source = vec![vec![1, 2], vec![3, 4], vec![5, 6, 7], vec![8, 9, 10]];
	///         let results = source.default_par_iter().flatten_iter().copied().collect::<Vec<_>>();
	///         assert_eq!(&results, &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	///     })
	///     .expect("This should not fail")
	/// ```
	fn flatten_iter(self) -> FlattenIter<Self, H>
	where
		Self::Item: IntoIterator,
		<Self::Item as IntoIterator>::Item: Send,
	{
		FlattenIter::new(self)
	}

	/// Applies `map_op` to each item of this iterator to get nested parallel
	/// iterable items, producing a new parallel iterator that flattens these back
	/// into one.
	///
	/// See also [`flat_map_iter`](ParallelIterator::flat_map_iter).
	///
	/// # Examples
	/// ```
	///
	/// use vr_threading::{
	///     iter::{IntoDefaultParallelRefIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let source = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]];
	///         let result = source
	///             .default_par_iter()
	///             .copied()
	///             .flat_map(|v| v)
	///             .collect::<Vec<_>>();
	///         assert_eq!(&result, &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	/// })
	/// .expect("This should not fail");
	/// ```
	fn flat_map<F, R>(self, map_op: F) -> FlatMap<Self, F, R, H>
	where
		F: Fn(Self::Item) -> R + Send + Sync,
		R: IntoParallelIterator<H>,
	{
		FlatMap::new(self, map_op)
	}

	/// Applies `map_op` to each item of this iterator to get nested serially
	/// iterable items, producing a new parallel iterator that flattens these back
	/// into one.
	///
	/// # [`flat_map_iter`](ParallelIterator::flat_map_iter) vs [`flat_map`](ParallelIterator::flat_map)
	///
	/// These two methods are similar but behave slightly differently. With
	/// [`flat_map`](ParallelIterator::flat_map), each of the nested iterators
	/// must be a parallel iterator, and they will be further split op with
	/// nested parallelism. Whith [`flat_map_iter`](ParallelIterator::flat_map_iter),
	/// each nested iterator is a sequential [`Iterator`], and we only parallelise
	/// _between_ them, while the items produced by each nested iterator are
	/// processed sequentially.
	///
	/// When choosing between these methods, consider whether nested parallelism
	/// suits the potential iterators at hand. If there's little computation
	/// involved, or its length is much less than the outer parallel iterator,
	/// then it may perform better to avoid the overhead of parallelism, just
	/// flattening sequentially with [`flat_map_iter`](ParallelIterator::flat_map_iter).
	/// If there is a lot of computation, potentially outwheighing the outer
	/// parallel iterator, then the nested parallelism of
	/// [`flat_map`](ParallelIterator::flat_map) may be worthwhile.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IntoDefaultParallelRefIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let source = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]];
	///         let result = source
	///             .default_par_iter()
	///             .copied()
	///             .flat_map_iter(|v| v)
	///             .collect::<Vec<_>>();
	///         assert_eq!(&result, &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	///     })
	///     .expect("This should not fail");
	/// ```
	fn flat_map_iter<F, R>(self, map_op: F) -> FlatMapIter<Self, F, R, H>
	where
		F: Fn(Self::Item) -> R + Send + Sync,
		R: IntoIterator,
		R::Item: Send,
	{
		FlatMapIter::new(self, map_op)
	}

	/// Creates an iterator that skips `n` abritrary elements from the iterator.
	///
	/// This is similar to [`IndexedParallelIterator::skip`] without being
	/// constrained to the "first" `n` of the original iterator order. The remaining
	/// items will still maintain their relative order where that is visible in
	/// [`collect`](ParallelIterator::collect), [`reduce`](ParallelIterator::reduce),
	/// and similar outputs.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IndexedParallelIterator, IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let result = (0..100)
	///             .into_default_par_iter()
	///             .filter(|&x| x % 2 == 0)
	///             .skip_any(5)
	///             .collect::<Vec<_>>();
	///         assert_eq!(result.len(), 45)
	///     })
	///     .expect("This should not fail")
	///
	/// ```
	fn skip_any(self, n: usize) -> SkipAny<Self, H> {
		SkipAny::new(self, n)
	}

	/// Creates an iterator that skips arbitrary elements from the original
	/// iterator until the given `predicate` returns `false`.
	///
	/// The `predicate` may be anything --- e. g. it could be checking a fact
	/// about the item, a global condidition unrelated to the item itself, or
	/// some combination thereof.
	///
	/// If parallel calls to the `predicate` race and give different results,
	/// then the `true` results will skip those particular items, while respecting
	/// the `false` results from elsewhere to skip any further items.
	///
	/// This is similar to [`Iterator::skip_while`] without being constrained
	/// to the original iterators order. The remaining items will still maintain
	/// their relative order where that is visible in [`collect`](ParallelIterator::collect),
	/// [`reduce`](ParallelIterator::reduce), and similar operations.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IndexedParallelIterator, IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let result = (1..100)
	///             .into_default_par_iter()
	///             .skip_any_while(|&x| x < 50)
	///             .collect::<Vec<_>>();
	///         assert!(result.len() >= 50);
	///         assert!(result.windows(2).all(|w| w[0] < w[1]))
	///     })
	///     .expect("This should not fail")
	/// ```
	fn skip_any_while<P>(self, predicate: P) -> SkipAnyWhile<Self, P, H>
	where
		P: Fn(&Self::Item) -> bool + Send + Sync,
	{
		SkipAnyWhile::new(self, predicate)
	}

	/// Creates a fresh collection holding all the items produced by the
	/// iterator, appended in the iterators intrinsic order.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let results = (0..5)
	///         .into_default_par_iter()
	///         .chain(5..10)
	///         .collect::<Vec<_>>();
	///     assert_eq!(&results, &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	/// }).expect("This should not fail")
	/// ```
	fn collect<R>(self) -> R
	where
		Self: IntoParallelIterator<H>,
		R: FromParallelIterator<<Self as IntoParallelIterator<H>>::Item>,
	{
		R::from_par_iter::<_, H>(self)
	}

	/// Collects this iterator into a linked list of vectors.
	///
	/// This is useful when you need to condense a parallel iterator into a
	/// collection, but have no specific requirements for what collection
	/// should be. If you plan to store the collection longer-term. [`Vec<T>`]
	/// is, as always, likely the best default, choice, despite the overhead
	/// that comes from concatenating each vector. Or, if this is an
	/// [`IndexedParallelIterator`], you should also prefer to just collect to
	/// a [`Vec<T>`].
	///
	/// Internally, most [`FromParallelIterator`]/[`ParallelExtend`]
	/// implementations use this strategy; each job collecting their cunk of the
	/// iterator to a [`Vec<T>`] and those chunks getting merged into a [`LinkedList`],
	/// before then extending the collection with each vector. This is a very
	/// efficient way to collect an unindexed parallel iterator, without much itermediate
	/// data movement.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let result = (0..100)
	///         .into_default_par_iter()
	///         .collect_vec_list();
	///     let total_len = result.into_iter().flatten().count();
	///     assert_eq!(total_len, 100);
	/// }).expect("This should not fail")
	/// ```
	fn collect_vec_list(self) -> LinkedList<Vec<<Self as IntoParallelIterator<H>>::Item>>
	where
		Self: IntoParallelIterator<H>,
	{
		match extend::fast_collect(self) {
			extend::Either::Left { val } => {
				let mut list = LinkedList::new();
				list.push_back(val);
				list
			}
			extend::Either::Right { val } => val,
		}
	}

	/// Unzips the items of a prallel iterator into a pair of arbitrary [`ParallelExtend`]
	/// containers.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IndexedParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let results: (Vec<_>, Vec<_>) = (0..5)
	///         .into_default_par_iter()
	///         .zip(5..10)
	///         .unzip();
	///     assert_eq!(&results.0, &[0, 1, 2, 3, 4]);
	///     assert_eq!(&results.1, &[5, 6, 7, 8, 9]);
	/// }).expect("This should not fail")
	/// ```
	fn unzip<FromA, FromB, A, B>(self) -> (FromA, FromB)
	where
		Self: ParallelIterator<H, Item = (A, B)>,
		FromA: Default + Send + ParallelExtend<A>,
		FromB: Default + Send + ParallelExtend<B>,
		A: Send,
		B: Send,
	{
		unzip::unzip(self)
	}
}

/// An specialisation of [`ParallelIterator`] that supports "random access" to
/// its data, meaning that you can split it at arbitrary indices and draw data
/// from those points.
///
/// **Note:** Cannot be implemented for [`u64`], [`i64`], [`u128`], or [`i128`]
/// ranges as the length of those ranges cannot be represented as an [`usize`] on
/// some/most platforms
pub trait IndexedParallelIterator<H>: ParallelIterator<H>
where
	H: ThreadPoolHandle,
	Self: Sized + Send,
{
	/// Method that is used to define the behaviour of this parallel iterator.
	/// You should not need to call this directly.
	///
	/// This method causes `self` to start producing items and feeds them to
	/// the `consumer` one by one. It may split the produced items and the
	/// consumer before doing so that they may be processed in parallel.
	/// If a split does happen, it will inform the consumer of the index where
	/// the split should occur (unlike [`ParallelIterator::drive_unindexed`])
	///
	/// Somewhat analogous to [`next`](Iterator::next) on normal [`Iterator`]s.
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>;

	/// Produces the exact count of how many items this iterator will produce,
	/// presuming no panic occurs.
	fn len(&self) -> usize;

	/// Method used to define the behaviour of this parallel iterator. You
	/// should not need to all this directly.
	///
	/// This method converts the iterator into a producer `P` and then invokes
	/// [`ProducerCallback::callback`] with `P`. Note that the type of this
	/// producer is not defined as part of the API, since
	/// [`callback`](ProducerCallback::callback) must be defined generically for
	/// all producers. This allows the producer type to contain references; it
	/// also means that parallel iterators can adjust that type without causing
	/// a breaking change.
	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>;

	/// Checks whether [`len`](IndexedParallelIterator::len) is `0`
	fn is_empty(&self) -> bool {
		self.len() == 0
	}

	/// Creates an iterator which gives the current iteration count as well as the
	/// next value.
	///
	/// The iterator returned yields pairs `(i, val)`, where `i` is the current
	/// index of iteration and `val` is the value returned by the iterator.
	///
	/// `enumerate()` keeps its count as a [`usize`]. If you want to count by
	/// a different sized integer, the [`zip`](IndexedParallelIterator::zip)
	/// function provides similar functionality.
	///
	/// # Overflow behaviour
	///
	/// This method will not overflow, as the len returned by
	/// [`len`](IndexedParallelIterator::len) should be correct and fittable inside
	/// a [`usize`].
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IndexedParallelIterator, IntoDefaultParallelIterator, ParallelIterator}, threadpool
	/// };
	///
	/// threadpool::ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let source = vec![1, 135, 123,3, 25];
	///         let result: Vec<_> = source
	///             .into_default_par_iter()
	///             .enumerate()
	///             .collect();
	///         assert_eq!(&result, &[(0, 1), (1, 135), (2, 123), (3, 3), (4, 25)]);
	///     }).expect("This should not fail")
	/// ```
	fn enumerate(self) -> Enumerate<Self, H> {
		Enumerate::new(self)
	}

	/// Creates an iterator that skips the first `n` elements.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{
	///     iter::{IndexedParallelIterator, IntoDefaultParallelIterator, ParallelIterator},
	///     threadpool::ThreadPoolBuilder,
	/// };
	///
	/// ThreadPoolBuilder::new()
	///     .spawn(|| {
	///         let result = (0..100)
	///             .into_default_par_iter()
	///             .skip(95)
	///             .collect::<Vec<_>>();
	///         assert_eq!(&result, &[95, 96, 97, 98, 99])
	///     })
	///     .expect("This should not fail")
	/// ```
	fn skip(self, n: usize) -> Skip<Self, H> {
		Skip::new(self, n)
	}

	/// Takes two iterators and connects their items into a tuple `(A, B)`.
	///
	/// # Examples
	///
	/// ```
	/// use vr_threading::{iter::{ParallelIterator, IndexedParallelIterator, IntoDefaultParallelIterator}};
	///
	/// vr_threading::threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let results = (0..5)
	///         .into_default_par_iter()
	///         .zip(0..5)
	///         .collect::<Vec<_>>();
	///     assert_eq!(&results, &[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)])
	/// }).expect("This should not fail")
	/// ```
	fn zip<I>(self, other: I) -> Zip<Self, I::Iter, H>
	where
		I: IntoParallelIterator<H, Item = Self::Item>,
		I::Iter: IndexedParallelIterator<H>,
	{
		Zip::new(self, other.into_par_iter())
	}

	/// Collects an [`IndexedParallelIterator`] into a [`Vec`]. This is mainly
	/// useful when implementing [`FromParallelIterator`] and [`ParallelExtend`].
	fn collect_into_vec(self) -> Vec<Self::Item> {
		let mut vec = Vec::new();
		collect_utils::collect_into_vec(self, &mut vec);
		vec
	}
}

/// [`ParallelDrainRange`] creates a [`ParallelIterator`] that moves a range of
/// items from a collection while retaining the original capacity.
pub trait ParallelDrainRange<H, Idx = usize>
where
	H: ThreadPoolHandle,
{
	/// The draining [`ParallelIterator`] type that will be created
	type Iter: ParallelIterator<H, Item = Self::Item>;
	/// The items the drain will yield. This is usually teh same as [`IntoParallelIterator::Item`]
	type Item: Send;

	/// Returns a draining parallel iterator over a range of the collection.
	///
	/// When the iterator is dropped, all items in the range are removed, even
	/// if the iterator was not fully consumed. if the iterator is leaked, e. g.,
	/// using [`core::mem::forget`], it is unspecified how many items are removed.
	fn par_drain<R>(self, range: R) -> Self::Iter
	where
		R: RangeBounds<Idx>;
}

/// [`FromParallelIterator`] describes the creation of a collection from a
/// [`ParallelIterator`]. By implementing [`FromParallelIterator`] you define
/// how it will be created from a [`ParallelIterator`].
///
/// This trait is used indirectly through the [`ParallelIterator::collect`]
/// method.
pub trait FromParallelIterator<T>
where
	T: Send,
{
	/// Creates an instance of the collection from the parallel iterator
	/// `par_iter`.
	fn from_par_iter<I, H>(par_iter: I) -> Self
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle;
}

/// [`ParallelExtend`] extends an existing collection with items from a
/// [`ParallelIterator`]
pub trait ParallelExtend<T> {
	/// Extends an instance of the collection with the elements drawn from
	/// the parallel iterator `par_iter`.
	fn par_extend<I, H>(&mut self, par_iter: I)
	where
		I: IntoParallelIterator<H, Item = T>,
		H: ThreadPoolHandle;
}

/// [`IntoParallelIterator`] implements the conversion to a [`ParallelIterator`].
///
/// By implementing [`IntoParallelIterator`] for a type, you define how it will
/// be transformed into an iterator. This is a parallel version of the standard
/// libaries [`IntoIterator`] trait.
pub trait IntoParallelIterator<H>
where
	H: ThreadPoolHandle,
{
	/// The [`ParallelIterator`] that will be created.
	type Iter: ParallelIterator<H, Item = Self::Item>;
	/// The type of item that the parallel iterator will produce.
	type Item: Send;
	/// Converts `self` into a parallel iterator.
	fn into_par_iter(self) -> Self::Iter;
}

/// Converts a type into a prallel iterator with the [`DefaultHandle`]
///
/// Created as using the [`IntoParallelIterator`] is very annoying.
pub trait IntoDefaultParallelIterator: IntoParallelIterator<DefaultHandle>
where
	Self: Sized,
{
	/// Converts `self` into a parallel iterator.
	fn into_default_par_iter(self) -> Self::Iter {
		self.into_par_iter()
	}
}
impl<T> IntoDefaultParallelIterator for T where T: IntoParallelIterator<DefaultHandle> {}

/// [`IntoParallelRefIterator`] implements the conversion  to a
/// [`ParallelIterator`], providing shared references to the data.
///
/// This is a parallel version of the `iter()` method defined by various
/// collections.
///
/// /// This trait is automatically implemented
/// `for I where &I: IntoParallelIterator`. In most cases, users
/// will want to implement [`IntoParallelIterator`] rather than implement
/// this trait directly.
pub trait IntoParallelRefIterator<'data, H>
where
	H: ThreadPoolHandle,
{
	/// The type of the parallel iterator that will be returned.
	type Iter: ParallelIterator<H, Item = Self::Item>;
	/// The type of item that the parallel iterator will produce.
	/// This will typically be an `&'data T` reference type.
	type Item: Send + 'data;

	/// Gets a parallel iterator referring to `self`
	fn par_iter(&'data self) -> Self::Iter;
}

impl<'data, I: 'data + ?Sized, H> IntoParallelRefIterator<'data, H> for I
where
	H: ThreadPoolHandle,
	&'data I: IntoParallelIterator<H>,
	<&'data I as IntoParallelIterator<H>>::Item: 'data,
{
	type Iter = <&'data I as IntoParallelIterator<H>>::Iter;
	type Item = <&'data I as IntoParallelIterator<H>>::Item;

	fn par_iter(&'data self) -> Self::Iter {
		self.into_par_iter()
	}
}

/// [`IntoParallelRefMutIterator`] implements the conversion to a
/// [`ParallelIterator`] providing mutable references to the data.
///
/// This is a parallel version of the `iter_mut()` method defined by various
/// collections.
///
/// This trait is automatically implemented
/// `for I where &mut I: IntoParallelIterator`. In most cases, users will want
/// to implement [`IntoParallelIterator`] rather than implementing this trait
/// directly.
pub trait IntoParallelRefMutIterator<'data, H>
where
	H: ThreadPoolHandle,
{
	/// The type of the parallel iterator that will be returned.
	type Iter: ParallelIterator<H, Item = Self::Item>;
	/// The type of item that the parallel iterator will produce.
	/// This will typically be an `&'data T` reference type.
	type Item: Send + 'data;

	/// Gets a parallel iterator referring to `self` mutably
	fn par_iter_mut(&'data mut self) -> Self::Iter;
}

impl<'data, I: 'data + ?Sized, H> IntoParallelRefMutIterator<'data, H> for I
where
	H: ThreadPoolHandle,
	&'data mut I: IntoParallelIterator<H>,
	<&'data mut I as IntoParallelIterator<H>>::Item: 'data,
{
	type Iter = <&'data mut I as IntoParallelIterator<H>>::Iter;
	type Item = <&'data mut I as IntoParallelIterator<H>>::Item;

	fn par_iter_mut(&'data mut self) -> Self::Iter {
		self.into_par_iter()
	}
}

/// Converts a type into a parallel iterator with the [`DefaultHandle`]
///
/// Created as using the [`IntoParallelRefIterator`] directly is very annoying.
pub trait IntoDefaultParallelRefIterator<'data>:
	IntoParallelRefIterator<'data, DefaultHandle>
{
	/// Converts `self` into a parallel iterator.
	fn default_par_iter(&'data self) -> Self::Iter {
		self.par_iter()
	}
}

impl<'data, T> IntoDefaultParallelRefIterator<'data> for T where
	T: IntoParallelRefIterator<'data, DefaultHandle>
{
}

/// Converts a type into a prallel iterator with the [`DefaultHandle`]
///
/// Created as using the [`IntoParallelRefMutIterator`] directly is very annoying.
pub trait IntoDefaultParallelRefMutIterator<'data>:
	IntoParallelRefMutIterator<'data, DefaultHandle>
{
	/// Converts `self` into a parallel iterator.
	fn default_par_iter_mut(&'data mut self) -> Self::Iter {
		self.par_iter_mut()
	}
}

impl<'data, T> IntoDefaultParallelRefMutIterator<'data> for T where
	T: IntoParallelRefMutIterator<'data, DefaultHandle>
{
}

/// The body of an [`IntoParallelIterator`] implementation of a [`ParallelIterator`]
#[macro_export]
macro_rules! iterator_into_iter_impl {
	() => {
		type Iter = Self;

		type Item = <Self as ParallelIterator<H>>::Item;

		fn into_par_iter(self) -> Self::Iter {
			self
		}
	};
}
