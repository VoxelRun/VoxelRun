//! Implements an [`Empty`] [`ParallelIterator`]
use core::marker::PhantomData;

use crate::{
	iter::{IndexedParallelIterator, ParallelIterator},
	ThreadPoolHandle,
};

use super::{Folder, Producer};

/// Creates an [`Empty`] [`ParallelIterator`] with item type `T`
pub fn empty<T>() -> Empty<T>
where
	T: Send,
{
	Empty {
		_marker: PhantomData,
	}
}

/// An empty [`ParallelIterator`] with item type `T`
#[derive(Debug, Clone, Copy)]
pub struct Empty<T>
where
	T: Send,
{
	/// Item type marker
	_marker: PhantomData<T>,
}

/// An empty [`ParallelIterator`] with item type `T`
struct EmptyProducer<T>
where
	T: Send,
{
	/// Item type marker
	_marker: PhantomData<T>,
}

impl<T, H> ParallelIterator<H> for Empty<T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: super::UnindexedConsumer<Self::Item, H>,
	{
		self.drive(consumer)
	}
}

impl<T, H> IndexedParallelIterator<H> for Empty<T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: super::Consumer<Self::Item, H>,
	{
		consumer.into_folder().complete()
	}

	fn len(&self) -> usize {
		0
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: super::ProducerCallback<Self::Item, H>,
	{
		callback.callback(EmptyProducer {
			_marker: PhantomData,
		})
	}
}

impl<T, H> Producer<H> for EmptyProducer<T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	type IntoIter = core::iter::Empty<T>;

	fn split_at(self, index: usize) -> (Self, Self) {
		debug_assert_eq!(index, 0);
		(
			self,
			EmptyProducer {
				_marker: PhantomData,
			},
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		core::iter::empty()
	}
}
