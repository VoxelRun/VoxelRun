//! Utilities for declaring iterator [`Producer`]s, [`Consumer`]s and bridging
//! them to execute them in parallel.
use core::marker::PhantomData;

use crate::ThreadPoolHandle;

use super::IndexedParallelIterator;

pub mod empty;
pub mod noop;

/// An iterator [`Producer`] for indexed iterators, effectively a "splitable
/// [`IntoIterator`]". That is, a producer that can be converted into an
/// iterator at any time. What makes a [`Producer`] special is that, _before_
/// we convert to an iterator, we can also **split** it at a particular point
/// using the [`split_at`](Producer::split_at) method. This will yield up to
/// two producers, one producing the items before that point and the other
/// after that point. Notably, those two producers can consequently be split
/// further, or converted into iterators to be consumed.
///
/// # Note
///
/// You might expect Producer to extend the IntoIterator trait. However,
/// [rust-lang/rust#20671][20671] prevents us from declaring the
/// [`DoubleEndedIterator`] and ExactSizeIterator constraints on a required
/// [`IntoIterator`]` trait, so we
/// inline IntoIterator here until that issue is fixed.
///
/// [20671]: https://github.com/rust-lang/rust/issues/20671
pub trait Producer<H>
where
	H: ThreadPoolHandle,
	Self: Send + Sized,
{
	/// The type of item that will be produced by this producer once it is
	/// converted into an iterator.
	type Item;
	/// The type of iterator this [`Producer`] will become
	type IntoIter: Iterator<Item = Self::Item> + DoubleEndedIterator + ExactSizeIterator;

	/// Split into two producers; one produces items `0..index`, the other
	/// `index..N`. Index must be less than or equal to `N`.
	fn split_at(self, index: usize) -> (Self, Self);

	/// Convert `self` into an iterator; at this point, no more parallel splits
	/// are possible.
	fn into_iter(self) -> Self::IntoIter;

	/// The minimum number of items that will be processed sequentially.
	/// Defaults to $1$, which means that we will split all the way down to a
	/// single item.
	fn min_len(&self) -> usize {
		1
	}

	/// The maximum number of items that will be processed sequentially.
	/// Defaults to `usize::MAX`, which means that there is no upper limit
	/// for the length of a sequentially processed action. Theoretically this
	/// would mean that it would be possible to choose not to split at all.
	fn max_len(&self) -> usize {
		usize::MAX
	}

	/// Iterate the producer, feeding each element to `folder`, and stop
	/// when the [`Folder`] is full (or all elements have been consumed).
	///
	/// The provided implementation is sufficient for most iterables
	fn fold_with<F>(self, folder: F) -> F
	where
		F: Folder<Self::Item>,
	{
		folder.consume_iter(self.into_iter())
	}
}

/// Type analogous to [`FnOnce`] but [`FnOnce`] has boundaries here which
/// do not suffice for the use case as each
/// [`with_producer`](super::IndexedParallelIterator::with_producer) will have
/// a different lifetime, so this may not have an associated lifetime.
pub trait ProducerCallback<T, H>
where
	H: ThreadPoolHandle,
{
	/// analogous to [`FnOnce::Output`]
	type Output;

	/// Invokes the callback with the given producer as argument. The key point
	/// of this trait is that this method is generic over `P`, and hence
	/// implementors must be defined for any producer.
	fn callback<P>(self, producer: P) -> Self::Output
	where
		P: Producer<H, Item = T>;
}

/// A variant on [`Producer`] which does not know its exact length or cannot
/// represent it in a [`usize`]. ُُThese producers act like ordinary producers
/// except that they cannot be told to split at a particular point. Instead,
/// you just ask them to split 'somewhere'.
///
/// (In principle, [`Producer`] could extend this trait; however, it does not
/// because to do so would require producers to carry their own length with them.)
pub trait UnindexedProducer<H>
where
	H: ThreadPoolHandle,
	Self: Send + Sized,
{
	/// The type of item returned by this producer.
	type Item;

	/// Split at the approximate middle into a new producer if possible,
	/// otherwise return [`None`].
	fn split(self) -> (Self, Option<Self>);
	/// Iterate the producer, feeding each element to `folder`, and stop when
	/// the folder is full (or all elements have been consumed).
	fn fold_with<F>(self, folder: F) -> F
	where
		F: Folder<Self::Item>;
}

/// A consumer is effectively a
/// [generialised "fold" operation](core::iter::Iterator::fold), and tin fact
/// each consumer will eventually be converted into a [`Folder`]. What makes a
/// consumer special is that, like a [`Producer`], it can be **split** into
/// multiple consumers using the [`split_at`](Consumer::split_at) method.
/// When a consumer is split, it produces two more consumers, as well as a
/// **reducer**. The two consumers can be fed items independently, and when
/// they are done the reducer is used to combine their two results into one.
pub trait Consumer<Item, H>
where
	H: ThreadPoolHandle,
	Self: Send + Sized,
{
	/// The type of result that this consumer will ultimately produce.
	type Result: Send;
	/// The type of reducer that is produced if this consumer is split.
	type Reducer: Reducer<Self::Result>;
	/// The type of folder that this consumer can be converted into.
	type Folder: Folder<Item, Result = Self::Result>;

	/// Hint whether this [`Consumer`] would like to stop processing further
	/// items, e. g. if a search has been completed.
	fn full(&self) -> bool;

	/// Divide the consumer into two consumers, one processing items `0..index`
	/// and one processing items from `index..` Also prodcues a reducer which
	/// can be used to combine the results at the end.
	fn split_at(self, index: usize) -> (Self, Self, Self::Reducer);

	/// Convert the consumer into a folder that can consume items sequentially,
	/// eventually producing a final result
	fn into_folder(self) -> Self::Folder;
}

/// A stateless [`Consumer`] can be freely copied. These consumers can be used
/// like regular consumers, but they also support a
/// [`split_off_left`](UnindexedConsumer::split_off_left) method that does not
/// take an index to split, but simply splits at some arbitrary point
/// ([`for_each`](super::ParallelIterator::for_each), for example, produces an
/// unindexed consumer).
pub trait UnindexedConsumer<I, H>: Consumer<I, H>
where
	H: ThreadPoolHandle,
{
	/// Splits off a "left" consumer and returns it. The `self` consumer should
	/// then be used to consume the "right" portion of the data. (The ordering
	/// matters for methods like `find_first (TBA)` — values produced by the
	/// returned value are given precedence over values produced by `self`.)
	/// Once the left and right halves have been fully consumed, the results
	/// should be reduced with [`to_reducer`](UnindexedConsumer::to_reducer).
	fn split_off_left(&self) -> Self;

	/// Creates a reducer that can be used to combine the results from a split
	/// consumer.
	fn to_reducer(&self) -> Self::Reducer;
}

/// The [`Folder`] trait encapsulates the
/// [standard fold operation](core::iter::Iterator::fold). It can be fed many
/// items using the [`consume`](Folder::consume) method. At the end once all
/// items have been consumed, it can be converted (using
/// [`complete`](Folder::complete)) into a final value.
pub trait Folder<Item>: Sized {
	/// The result type ultimately produced by the folder.
	type Result;
	/// Consume the next item and return enw sequential state.
	/// (Basically [`Iterator::next`])
	fn consume(self, item: Item) -> Self;
	/// Finish consuming items, output the final result.
	fn complete(self) -> Self::Result;
	/// Hint whether thi [`Folder`] would like to stop processing further items,
	/// e. g. if a search has been completed. A [`Folder`], that does not end its
	/// iteration early, before all items have been viewed, will never be full.
	fn full(&self) -> bool;

	/// Consume items from the iterator until full, and return new sequential
	/// state.
	///
	/// The method is **optional**. The default simply iteratores over `iter`,
	/// invoking [`consume`](Folder::consume) and checking after each iteration
	/// whether [`full`](Folder::full) returns false.
	///
	/// The main reason to override it is if you can provide a more specialised,
	/// efficient implementation.
	fn consume_iter<I>(mut self, iter: I) -> Self
	where
		I: IntoIterator<Item = Item>,
	{
		for item in iter {
			self = self.consume(item);
			if self.full() {
				break;
			}
		}
		self
	}
}

/// The reducer is the final step of a [`Consumer`] — after a consumer has
/// been split into two parts, and each of those parts has been fully processed,
/// we are left with two results. The reducer is then used to combine those
/// two results into one.
pub trait Reducer<Result> {
	/// Reduce two final results into one; this is executed after a split
	fn reduce(self, left: Result, right: Result) -> Result;
}

/// A type that keeps track of how many times an iterator was split on the
/// current thread.
#[derive(Debug, Clone, Copy)]
pub struct Splitter<H>
where
	H: ThreadPoolHandle,
{
	/// The amount of splits left.
	splits: usize,
	/// Handle to the threadpool so that the type param has a use.
	_type: PhantomData<H>,
}

impl<H> Splitter<H>
where
	H: ThreadPoolHandle,
{
	/// Create a new [`Splitter`]. It will be split the same amount of times
	/// on the current thread as the current [`ThreadPoolHandle`] says it has
	/// threads.
	pub fn new() -> Self {
		Self {
			splits: H::current_num_threads().into(),
			_type: PhantomData,
		}
	}

	/// Try to split the splitter, if it was moved onto another thread
	/// `stolen` must be set.
	pub fn try_split(&mut self, stolen: bool) -> bool {
		let Splitter { splits, .. } = *self;
		if stolen {
			self.splits = Ord::max(H::current_num_threads().into(), self.splits / 2);
			return true;
		}
		if splits > 0 {
			self.splits /= 2;
			return true;
		}
		false
	}
}

impl<H> Default for Splitter<H>
where
	H: ThreadPoolHandle,
{
	fn default() -> Self {
		Self::new()
	}
}

/// A [`Splitter`] that takes the remaining length of the iterator into account
#[derive(Debug, Clone, Copy)]
pub struct LengthSplitter<H>
where
	H: ThreadPoolHandle,
{
	/// The underlying splitter
	inner: Splitter<H>,
	/// The minimum length of a sequentially processed segment. Usually just
	/// $1$, take a look at [`Producer::min_len`].
	min: usize,
}

impl<H> LengthSplitter<H>
where
	H: ThreadPoolHandle,
{
	/// Create a new [`LengthSplitter`], it will be split until the `min` slice
	/// length is reached, at least until the splitted length is less than
	/// the `max` slice length and under all else conditions the current number
	/// of threads times on the current thread.
	pub fn new(min: usize, max: usize, len: usize) -> Self {
		let mut splitter = Self {
			inner: Splitter::<H>::default(),
			min: Ord::max(1, min),
		};

		let min_splits = len / Ord::max(1, max);

		if min_splits > splitter.inner.splits {
			splitter.inner.splits = min_splits;
		}

		splitter
	}

	/// Try to split this splitter, `len` is the length of the Iterator, `stolen`
	/// indicates whether the Splitter is invoked on another thread.
	pub fn try_split(&mut self, len: usize, stolen: bool) -> bool {
		len / 2 >= self.min && self.inner.try_split(stolen)
	}
}

/// Bridge a [`Producer`] with a [`Consumer`] and returns the result of the
/// [`Consumer`]. It will split the closure into sensible chunks and execute
/// them in parallel. It may be preferrable to call [`bridge`] which wraps this
/// function.
///
/// The chunks size is determined by the producers [`min_len`](Producer::min_len),
/// as well as [`max_len`](Producer::max_len) and the split amount checked by
/// a [`LengthSplitter`].
pub fn bridge_producer_consumer<P, C, H>(len: usize, producer: P, consumer: C) -> C::Result
where
	H: ThreadPoolHandle,
	P: Producer<H>,
	C: Consumer<P::Item, H>,
{
	let splitter = LengthSplitter::<H>::new(producer.min_len(), producer.max_len(), len);
	return helper(len, false, splitter, producer, consumer);

	/// The helper that will actually be executed in parallel on multipe threads.
	fn helper<P, C, H>(
		len: usize,
		migrated: bool,
		mut splitter: LengthSplitter<H>,
		producer: P,
		consumer: C,
	) -> C::Result
	where
		H: ThreadPoolHandle,
		P: Producer<H>,
		C: Consumer<P::Item, H>,
	{
		// The consumer will not process anymore items.
		if consumer.full() {
			return consumer.into_folder().complete();
		}

		// The splitter was successful and the length can be split into halves
		if splitter.try_split(len, migrated) {
			let mid = len / 2;
			let (left_producer, right_producer) = producer.split_at(mid);
			let (left_consumer, right_consumer, reducer) = consumer.split_at(mid);
			let (left_result, right_result) = H::join_context(
				|context| {
					helper(
						// the length is mid
						mid,
						context.migrated(),
						splitter,
						left_producer,
						left_consumer,
					)
				},
				|context| {
					helper(
						// if the len was uneven mid would be less then len / mid
						len - mid,
						context.migrated(),
						splitter,
						right_producer,
						right_consumer,
					)
				},
			);

			return reducer.reduce(left_result, right_result);
		}

		// just sequentially process as not splitable
		producer.fold_with(consumer.into_folder()).complete()
	}
}

/// A variant of [`bridge_producer_consumer`] that accepts
/// [`UnindexedProducer`]s. This will split the task to sensible segments
/// determined by a [`Splitter`].
pub fn bridge_unindexed<P, C, H>(producer: P, consumer: C) -> C::Result
where
	H: ThreadPoolHandle,
	P: UnindexedProducer<H>,
	C: UnindexedConsumer<P::Item, H>,
{
	let splitter = Splitter::new();
	return helper(false, splitter, producer, consumer);

	/// The helper that will actually be executed in parallel on multipe threads.
	fn helper<P, C, H>(
		migrated: bool,
		mut splitter: Splitter<H>,
		producer: P,
		consumer: C,
	) -> C::Result
	where
		H: ThreadPoolHandle,
		P: UnindexedProducer<H>,
		C: UnindexedConsumer<P::Item, H>,
	{
		// The consumer will not process anymore items
		if consumer.full() {
			return consumer.into_folder().complete();
		}

		// splitting works and we execute on other threads.
		if splitter.try_split(migrated) {
			return match producer.split() {
				(left_producer, Some(right_producer)) => {
					let (reducer, left_consumer, right_consumer) =
						(consumer.to_reducer(), consumer.split_off_left(), consumer);
					let (left_result, right_result) = H::join_context(
						|context| {
							helper(context.migrated(), splitter, left_producer, left_consumer)
						},
						|context| {
							helper(context.migrated(), splitter, right_producer, right_consumer)
						},
					);
					reducer.reduce(left_result, right_result)
				}
				// Splitting doesn't change anything
				(producer, None) => producer.fold_with(consumer.into_folder()).complete(),
			};
		}
		// Process in parallel as splitting is not sensible
		producer.fold_with(consumer.into_folder()).complete()
	}
}

/// This helper bridges a [`ParallelIterator`](super::IndexedParallelIterator)
/// with a [`Consumer`]. It wraps [`bridge_producer_consumer`].
///
/// This is useful when implementing your own parallel iterators: it is often
/// used as the defintion of the
/// [`drive_unindexed`](super::ParallelIterator::drive_unindexed) and the
/// [`drive`](super::IndexedParallelIterator::drive) methods.
pub fn bridge<I, C, H>(par_iter: I, consumer: C) -> C::Result
where
	I: IndexedParallelIterator<H>,
	C: Consumer<I::Item, H>,
	H: ThreadPoolHandle,
{
	let len = par_iter.len();
	return par_iter.with_producer(Callback { len, consumer });

	/// The callback that will be bridged
	struct Callback<C> {
		/// the length of the iterator.
		len: usize,
		/// the consumer that will be used.
		consumer: C,
	}

	impl<C, I, H> ProducerCallback<I, H> for Callback<C>
	where
		C: Consumer<I, H>,
		H: ThreadPoolHandle,
	{
		type Output = C::Result;
		fn callback<P>(self, producer: P) -> Self::Output
		where
			P: Producer<H, Item = I>,
		{
			bridge_producer_consumer(self.len, producer, self.consumer)
		}
	}
}
