//! Types which fulfill the property of not doing anything for parallel iterators

use crate::ThreadPoolHandle;

use super::{Consumer, Folder, Reducer, UnindexedConsumer};

/// A reducer that doesn't combine two items
pub struct NoopReducer;

impl Reducer<()> for NoopReducer {
	fn reduce(self, _left: (), _right: ()) {}
}

/// A Consumer that does nothing, other than progressing the iterators, doing
/// nothing with the items
pub struct NoopConsumer;

impl<H, T> Consumer<T, H> for NoopConsumer
where
	H: ThreadPoolHandle,
{
	type Result = ();

	type Reducer = NoopReducer;

	type Folder = Self;

	fn full(&self) -> bool {
		false
	}

	fn split_at(self, _index: usize) -> (Self, Self, Self::Reducer) {
		(Self, Self, NoopReducer)
	}

	fn into_folder(self) -> Self::Folder {
		self
	}
}

impl<H, T> UnindexedConsumer<T, H> for NoopConsumer
where
	H: ThreadPoolHandle,
{
	fn split_off_left(&self) -> Self {
		Self
	}

	fn to_reducer(&self) -> Self::Reducer {
		NoopReducer
	}
}

impl<T> Folder<T> for NoopConsumer {
	type Result = ();

	fn consume(self, _item: T) -> Self {
		self
	}

	fn complete(self) -> Self::Result {}

	fn full(&self) -> bool {
		false
	}
}
