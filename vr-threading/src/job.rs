//! Job utilities for sending jobs accross threads

use core::any::Any;

use vr_core::{
	cell::UnsafeCell,
	unwind::{self, AbortIfPanic},
};

use crate::sync::latch::Latch;

/// The trait each Job implementor must implement.
pub trait Job {
	/// Execute the job
	///
	/// this represents the Job object, it is currently a placeholder untils
	/// arbitrary self types is stabilised
	///
	/// takes in a `*const ()` as this needs to be opque for [`JobRef`] to be
	/// able to be stored in collections, essentially equal to `*const Self`.
	///
	/// # Safety
	///
	/// Each job **must** only be called once.
	///
	/// this must be a valid reference to the job object
	unsafe fn execute(this: *const ());
}

/// A [`Job`] trait object
pub struct JobRef {
	/// The referenced job
	data: *const (),
	/// The [`Job`]-execute function reference.
	function: unsafe fn(this: *const ()),
}

// Safety: This must be set as we plan to transfer them accross threads
unsafe impl Send for JobRef {}
// Safety: This must be set as we plan to transfer them accross threads
unsafe impl Sync for JobRef {}

impl JobRef {
	/// Constructs a new [`JobRef`].
	///
	/// # Safety
	///
	/// While the construction of a [`JobRef`] is technically not unsafe, if
	/// you plan to execute the job, which is most likely, you should guarantee
	/// that the new [`JobRef`] will not outlive `job`.
	pub unsafe fn new<J>(job: *const J) -> Self
	where
		J: Job,
	{
		Self {
			data: job as _,
			function: <J as Job>::execute,
		}
	}

	/// Execute the underlying job
	///
	/// # Safety
	///
	/// It is the callers responsibility to guarantee that the Job referenced
	/// by this [`JobRef`] is still alive when executing this function.
	pub unsafe fn execute(&self) {
		// Safety: See documentation safety section and [`Job::execute`]
		unsafe { (self.function)(self.data) }
	}

	/// A unique identifier to this job whih is opaquely comparable to itself
	pub fn id(&self) -> impl Eq {
		(self.function, self.data)
	}
}

/// The possible results of a job
pub enum JobResult<T> {
	/// The job was not yet executed
	None,
	/// The job was executed successfully
	Success {
		/// The return value of the job's function
		return_val: T,
	},
	/// The job panicked
	Panic {
		/// The panic payload
		payload: Box<dyn Any + Send>,
	},
}

impl<T> JobResult<T> {
	/// Call the function, assuming it was moved between threads
	fn call<F>(function: F) -> Self
	where
		F: FnOnce(bool) -> T,
	{
		match unwind::halt_unwinding(|| function(true)) {
			Ok(value) => Self::Success { return_val: value },
			Err(payload) => Self::Panic { payload },
		}
	}

	/// Conver the [`JobResult`] into the return value of the function that
	/// was called
	///
	/// # Safety
	///
	/// Assumes the Job is finished / called
	///
	/// # Panics
	///
	/// Panics if the job panicked, continuing unwinding.
	pub unsafe fn into_result(self) -> T {
		match self {
			JobResult::None => unreachable!(),
			JobResult::Success { return_val } => return_val,
			JobResult::Panic { payload } => unwind::resume_unwinding(payload),
		}
	}
}

/// A job that will be owned by a stack slot. This means that when it
/// executes it need not free any heap data, the cleanup occurs when
/// the stack frame is later popped.  The function parameter indicates
/// `true` if the job was stolen -- executed on a different thread.
///
/// The StackJob also expects to only be executed on one thread concurrently
pub struct StackJob<L, F, T>
where
	L: Latch + Sync,
	F: Send + FnOnce(bool) -> T,
	T: Send,
{
	/// The latch signaling the status of this job
	pub latch: L,
	/// The function this job will execute.
	function: UnsafeCell<Option<F>>,
	/// The result of said [`function`](StackJob::function).
	result: UnsafeCell<JobResult<T>>,
}

impl<L, F, T> StackJob<L, F, T>
where
	L: Latch + Sync,
	F: Send + FnOnce(bool) -> T,
	T: Send,
{
	/// Creates a new [`StackJob`].
	pub fn new(job: F, latch: L) -> Self {
		Self {
			latch,
			function: UnsafeCell::new(Some(job)),
			result: UnsafeCell::new(JobResult::None),
		}
	}

	/// Constructs a new [`JobRef`] from a [`StackJob`].
	///
	/// # Safety
	///
	/// While the construction of a [`JobRef`] is technically not unsafe, if
	/// you plan to execute the job, which is most likely, you should guarantee
	/// that the new [`JobRef`] will not outlive `job`.
	pub unsafe fn as_job_ref(&self) -> JobRef {
		// Safety: Checked by caller
		unsafe { JobRef::new(self) }
	}

	/// Runs the [`StackJob`] inline, not deferring to the [`execute`](JobRef::execute)
	/// function.
	///
	/// stolen indicates if this thread owns the job or not.
	///
	/// # Panics
	///
	/// Panics if the provided function is executed more than once, or it panics
	pub fn run_inline(self, stolen: bool) -> T {
		self.function.with_mut(|function| {
			// Safety: This pointer is guaranteed to be valid as it's location
			// is never modified since inception
			let function = unsafe {
				(*function)
					.take()
					.expect("Trying to execute StackJob more than once")
			};
			function(stolen)
		})
	}

	/// Get the result of the job
	///
	/// # Safety
	///
	/// The job must have already been executed for this to work.
	///
	/// # Panics
	///
	/// The function will panic if the job panicked
	pub unsafe fn into_result(self) -> T {
		// Safety: guaranteed by caller
		unsafe { self.result.into_inner().into_result() }
	}
}

impl<L, F, T> Job for StackJob<L, F, T>
where
	L: Latch + Sync,
	F: Send + FnOnce(bool) -> T,
	T: Send,
{
	unsafe fn execute(this: *const ()) {
		// Safety: The execute requirements allow us to assume that this job will only
		// be executed once and on one thread.
		unsafe {
			let this = &*(this as *const Self); // cast to &StackJob
			let abort_guard = AbortIfPanic {
				target: "ThreadPool",
			};
			this.function.with_mut(|function| {
				let function = (*function)
					.take()
					.expect("Trying to execute StackJob more than once");
				this.result.with_mut(|result| {
					*result = JobResult::call(function);
				});
			});

			this.latch.set();
			core::mem::forget(abort_guard);
		}
	}
}

/// A job that lives on the heap. It will live indefinetely until executed.
pub struct HeapJob<F>
where
	F: FnOnce() + Send,
{
	/// The underlying function
	function: F,
}

impl<F> HeapJob<F>
where
	F: Send + FnOnce(),
{
	/// Create a new [`HeapJob`]
	pub fn new(f: F) -> Box<Self> {
		Box::new(Self { function: f })
	}

	/// Convert to a [`JobRef`]
	///
	/// # Safety
	///
	/// The [`JobRef`] must not outlive the lifetime of the underlying function
	pub unsafe fn into_job_ref(self: Box<Self>) -> JobRef {
		// Safety: Ensured by caller
		unsafe { JobRef::new(Box::into_raw(self)) }
	}

	/// Convert to a static [`JobRef`]
	pub fn into_static_job_ref(self: Box<Self>) -> JobRef
	where
		F: 'static,
	{
		// Safety: If the function is static this is safe as the job ref can't
		// outlive a static lifetime
		unsafe { self.into_job_ref() }
	}
}

impl<F> Job for HeapJob<F>
where
	F: Send + FnOnce(),
{
	unsafe fn execute(this: *const ()) {
		// Safety: Ensured by executor:
		//	- each job ref is only called once
		//	- JobRefs must not outlive their references
		unsafe {
			let this = Box::from_raw(this as *mut Self);
			(this.function)()
		}
	}
}
