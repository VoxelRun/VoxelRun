//! The threading modules for the VoxelRun game
//!
//! This crate provides the following functionality to make it easy to paralellize
//! code. The provided threadpool itself is lightweight and based on the
//! "work-stealing" techique of scheduling.
//!
//! # How to use this crate
//!
//! ## Creating a threadpool
//!
//! You can create a Threadpool using the [`ThreadPoolBuilder`](threadpool::ThreadPoolBuilder)
//!
//! ```
//! use vr_threading::threadpool::ThreadPoolBuilder;
//!
//! ThreadPoolBuilder::new()
//!     .spawn(|| println!("I am in the threadpool"))
//!     .expect("Something went wrong when creating the threadpool");
//! ```
//!
//! ## Creating Tasks
//!
//! You can divide you work into parallelizable tasks using
//! - [`join`](handle::DefaultHandle::join)
//! - [`join_context`](handle::DefaultHandle::join)
//! - [`scope`](handle::DefaultHandle::scope)
//! - [`task`](handle::DefaultHandle::task)

pub mod array;
pub mod handle;
pub mod iter;
mod job;
mod random;
pub mod range;
mod registry;
pub mod slice;
mod sync;
pub mod vec;

#[cfg(test)]
mod test;

pub mod threadpool;
use std::thread::available_parallelism;

pub use registry::scope::Scope;

/// Provides calling contexts for executing functions.
pub struct Context {
	/// Was the function migrated
	migrated: bool,
	/// Unimplement [`Send`] and [`Sync`]
	_marker: core::marker::PhantomData<*mut ()>,
}

impl Context {
	/// Creates a new [`Context`].
	///
	/// migrated states whether the context was migrated between threads
	pub fn new(migrated: bool) -> Self {
		Self {
			migrated,
			_marker: core::marker::PhantomData,
		}
	}

	/// Check if the task was transfered between threads.
	pub fn migrated(&self) -> bool {
		self.migrated
	}
}

#[allow(rustdoc::private_intra_doc_links)]
/// The type for the [`PanicHandler`] in the [thread registry](`registry::Registry`).
/// For more details, look at
/// [`ThreadPoolBuilder::panic_handler`](crate::threadpool::ThreadPoolBuilder::panic_handler)
pub type PanicHandler = dyn Fn(Box<dyn core::any::Any + Send>) + Send + Sync;

/// Trait representing a handle to a threadpool. Used to provide a universal
/// interface to differen threadpool implementations.
pub trait ThreadPoolHandle: Send + Sync + Copy {
	/// Takes two closures and _potentially_ runs them in parallel. It returns
	/// a pair of the results from those closures.
	///
	/// Conceptually, [`join`](ThreadPoolHandle::join) is similar to spawning
	/// two threads, one executing each of the closures.
	///
	/// # Examples
	/// A threadpool agnostic quick-sort implementation.
	/// ```
	/// use vr_threading::ThreadPoolHandle;
	///
	/// fn partition<T>(v: &mut [T]) -> usize
	/// where
	///     T: PartialOrd + Send
	/// {
	///     let pivot = v.len() -  1;
	///     let mut i = 0;
	///     for j in 0..pivot {
	///         if v[j] < v[pivot] {
	///              v.swap(i, j);
	///              i += 1;
	///         }
	///     }
	///     v.swap(i, pivot);
	///     i
	/// }
	///
	/// fn quick_sort<T, H>(v: &mut [T])
	/// where
	///     H: ThreadPoolHandle,
	///     T: PartialOrd + Send
	/// {
	///     if v.len() > 1 {
	///          let mid = partition(v);
	///          let (lo, hi) = v.split_at_mut(mid);
	///          H::join(|| quick_sort::<_, H>(lo), || quick_sort::<_, H>(hi));
	///     }
	/// }
	/// ```
	fn join<F1, F2, T1, T2>(task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce() -> T1,
		T1: Send,
		F2: Send + FnOnce() -> T2,
		T2: Send;

	/// Identical to [`join`](ThreadPoolHandle::join), except that the closures
	/// have a parameter that provides context for the way the closure has
	/// been called, especially indicating whether they're executing on a
	/// different thread than where [`join_context`](ThreadPoolHandle::join_context)
	/// was called. This will occur if the second job is stolen by a different
	/// thread.
	fn join_context<F1, F2, T1, T2>(task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce(Context) -> T1,
		F2: Send + FnOnce(Context) -> T2,
		T2: Send,
		T1: Send;

	/// Create a task an eventually executes it on the threadpool. Just like
	/// a standard thread, this task is not tied to the current stack frame,
	/// and hence it cannot hold any references other than those with a `'static`
	/// lifetime.
	///
	/// Since tasks created with this function cannot hold references into the
	/// enclosing stack frame, you almost certainly want to use a `move` closure
	/// as their argument (otherwise, the closure will typically hold references
	/// to any variables from the enclosing function that you happen to use).
	///
	/// This API is no guaranteed order of execution, given that other threads
	/// may steal tasks at any time.
	///
	/// The Threadpool will **always** outlive the task.
	///
	/// # Examples
	///
	/// a threadpool agnostic method that creates a new task to increment a
	/// global counter and returns immediately. It is left to the threadpool
	/// to then schedule that task.
	/// ```
	/// use vr_threading::ThreadPoolHandle;
	/// use std::sync::atomic::{AtomicUsize, Ordering};
	///
	/// static GLOBAL_COUNTER: AtomicUsize = AtomicUsize::new(0);
	///
	/// fn increment_global_counter<H>()
	/// where
	///     H: ThreadPoolHandle
	/// {
	///     H::task(|| {
	///         GLOBAL_COUNTER.fetch_add(1, Ordering::SeqCst);
	///     })
	/// }
	///
	/// ```
	fn task<F>(task: F)
	where
		F: 'static + FnOnce() + Send;

	/// get the number of threads on the current threadpool
	fn current_num_threads() -> core::num::NonZeroUsize {
		available_parallelism().unwrap()
	}
}
