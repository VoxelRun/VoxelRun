//! A module for the Random number generator for the victim selection process
//! when stealing.

use core::{hash::Hasher, sync::atomic::Ordering};
use std::hash::DefaultHasher;

use core::{cell::Cell, sync::atomic::AtomicUsize};

/// [xorshift*] is a fast pseudorandom number generator which will
/// even tolerate weak seeding, as long as it's not zero.
///
/// [xorshift*]: https://en.wikipedia.org/wiki/Xorshift#xorshift*
pub struct XorShift64Star {
	/// The internal state of the RNG
	state: Cell<u64>,
}

impl XorShift64Star {
	/// Creates a new [`XorShift64Star`].
	pub fn new() -> Self {
		// Any non-zero seed will do -- this uses the hash of a global counter.
		let mut seed = 0;
		while seed == 0 {
			let mut hasher = DefaultHasher::new();
			/// Counter to use for different seeds for the hasher.
			static COUNTER: AtomicUsize = AtomicUsize::new(0);
			hasher.write_usize(COUNTER.fetch_add(1, Ordering::Relaxed));
			seed = hasher.finish();
		}

		XorShift64Star {
			state: Cell::new(seed),
		}
	}

	/// Generate a random 64 bit integer
	pub fn next(&self) -> u64 {
		let mut x = self.state.get();
		debug_assert_ne!(x, 0);
		x ^= x >> 12;
		x ^= x << 25;
		x ^= x >> 27;
		self.state.set(x);
		x.wrapping_mul(0x2545_f491_4f6c_dd1d)
	}
}
