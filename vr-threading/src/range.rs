//! Parallel iterator types for ranges, the type for values created by `a..b`
//! expressions You will rarely need to interact with this module directly
//! unless you have need to name one of the iterator types.
use core::{
	marker::PhantomData,
	ops::{Range, RangeInclusive},
};

use crate::{
	iter::{
		plumbing::{
			bridge, bridge_unindexed, empty::empty, Consumer, Folder, Producer, ProducerCallback,
			UnindexedConsumer, UnindexedProducer,
		},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	iterator_into_iter_impl, ThreadPoolHandle,
};

/// The [`ParallelIterator`] type for [`Range<T>`]
pub struct Iter<T, H>
where
	H: ThreadPoolHandle,
{
	/// The underlying range
	range: Range<T>,
	/// The handle to the threadpool
	_handle: PhantomData<H>,
}

impl<T, H> IntoParallelIterator<H> for Range<T>
where
	H: ThreadPoolHandle,
	Iter<T, H>: ParallelIterator<H>,
{
	type Iter = Iter<T, H>;
	type Item = <Iter<T, H> as ParallelIterator<H>>::Item;

	fn into_par_iter(self) -> Self::Iter {
		Iter {
			range: self,
			_handle: PhantomData,
		}
	}
}

impl<T, H> IntoParallelIterator<H> for Iter<T, H>
where
	H: ThreadPoolHandle,
	Iter<T, H>: ParallelIterator<H>,
{
	iterator_into_iter_impl!();
}

/// A [`Producer`] which produces items of a range
struct IterProducer<T> {
	/// The underlying range
	range: Range<T>,
}

/// The length of an inindexed range, `L` should be an integer type which can
/// represent the length, not necessary if the length can be expressend through
/// a [`usize`].
trait UnindexedRangeLen<L> {
	/// Get the length with type `L`
	fn unindexed_len(&self) -> L;
}

/// Implements [`IndexedParallelIterator`] for a [`Range`] of type $t. To be
/// more precise, it Implements [`IndexedParallelIterator`] for [`Iter<$t>`],
/// which consequently implements [`IntoParallelIterator`]
macro_rules! indexed_range_impl {
	($t:ty) => {
		impl<H> ParallelIterator<H> for Iter<$t, H>
		where
			H: ThreadPoolHandle,
		{
			type Item = $t;
			fn drive_unindexed<C>(self, consumer: C) -> C::Result
			where
				C: UnindexedConsumer<Self::Item, H>,
			{
				bridge(self, consumer)
			}

			fn opt_len(&self) -> Option<usize> {
				Some(self.len())
			}
		}

		impl<H> IndexedParallelIterator<H> for Iter<$t, H>
		where
			H: ThreadPoolHandle,
		{
			fn drive<C>(self, consumer: C) -> C::Result
			where
				C: Consumer<Self::Item, H>,
			{
				bridge(self, consumer)
			}
			fn len(&self) -> usize {
				self.range.size_hint().0
			}

			fn with_producer<CB>(self, callback: CB) -> CB::Output
			where
				CB: ProducerCallback<Self::Item, H>,
			{
				callback.callback(IterProducer { range: self.range })
			}
		}

		impl<H> Producer<H> for IterProducer<$t>
		where
			H: ThreadPoolHandle,
		{
			type Item = <Range<$t> as Iterator>::Item;
			type IntoIter = Range<$t>;

			fn into_iter(self) -> Self::IntoIter {
				self.range
			}

			fn split_at(self, index: usize) -> (Self, Self) {
				assert!(
					index <= self.range.len(),
					"assertion failed with index: {index} and length: {}",
					self.range.len()
				);
				let mid = self.range.start.wrapping_add(index as $t);
				let left = self.range.start..mid;
				let right = mid..self.range.end;
				(IterProducer { range: left }, IterProducer { range: right })
			}
		}
	};
}

/// Implements [`ParallelIterator`] for a [`Range`] of type $t, the length of
/// which is expressed through a type `len_t`. This is necessary as the length
/// of the range may not be describable through a [`usize`].
///
/// Otherwise analogous to [`indexed_range_impl`] but just implements
/// [`ParallelIterator`] (not [`IndexedParallelIterator`]) and also [`UnindexedRangeLen`]
/// for `$len_t`
macro_rules! unindexed_range_impl {
	($t:ty, $len_t:ty) => {
		impl UnindexedRangeLen<$len_t> for Range<$t> {
			fn unindexed_len(&self) -> $len_t {
				let &Range { start, end } = self;
				if end > start {
					end.wrapping_sub(start) as $len_t
				} else {
					0
				}
			}
		}

		impl<H> ParallelIterator<H> for Iter<$t, H>
		where
			H: ThreadPoolHandle,
		{
			type Item = $t;
			fn drive_unindexed<C>(self, consumer: C) -> C::Result
			where
				C: UnindexedConsumer<Self::Item, H>,
			{
				#[allow(unused)]
				fn offset(start: $t) -> impl Fn(usize) -> $t {
					move |i| start.wrapping_add(i as $t)
				}
				if let Some(len) = self.opt_len() {
					(0..len)
						.into_par_iter()
						.map(offset(self.range.start))
						.drive(consumer)
				} else {
					bridge_unindexed(IterProducer { range: self.range }, consumer)
				}
			}

			fn opt_len(&self) -> Option<usize> {
				usize::try_from(self.range.unindexed_len()).ok()
			}
		}

		impl<H> UnindexedProducer<H> for IterProducer<$t>
		where
			H: ThreadPoolHandle,
		{
			type Item = $t;

			fn split(mut self) -> (Self, Option<Self>) {
				let index = self.range.unindexed_len() / 2;
				if index > 0 {
					let mid = self.range.start.wrapping_add(index as $t);
					let right = mid..self.range.end;
					self.range.end = mid;
					(self, Some(IterProducer { range: right }))
				} else {
					(self, None)
				}
			}

			fn fold_with<F>(self, folder: F) -> F
			where
				F: Folder<Self::Item>,
			{
				folder.consume_iter(self.range)
			}
		}
	};
}

indexed_range_impl!(u8);
indexed_range_impl!(u16);
indexed_range_impl!(u32);
indexed_range_impl!(usize);
indexed_range_impl!(i8);
indexed_range_impl!(i16);
indexed_range_impl!(i32);
indexed_range_impl!(isize);

unindexed_range_impl!(u64, u64);
unindexed_range_impl!(i64, u64);
unindexed_range_impl!(u128, u128);
unindexed_range_impl!(i128, u128);

/// Executs a `method` for an [`IterInclusive`] on an iterated range.
macro_rules! char_to_iter_range {
    ($self:ident . $method:ident ( $($arg:expr),* )) => {{
		let start = $self.range.start as u32;
		let end = $self.range.end as u32;
		if start < 0xd800 && 0xe000 < end {
			(start..0xd800)
				.into_par_iter()
				.chain(0xe000..end)
				// Safety: we know this is valid as the user can't input a character
				// > 0x0x10ffff and the only other gap was checked
				.map(|codepoint| unsafe { char::from_u32_unchecked(codepoint) })
				.$method($($arg),*)
		} else {
		    (start..end)
				.into_par_iter()
				// Safety: we know this is valid as the user can't input a character
				// > 0x0x10ffff and the only other gap was checked
				.map(|codepoint| unsafe { char::from_u32_unchecked(codepoint) })
				.$method($($arg),*)
		}
    }};
}

impl<H> ParallelIterator<H> for Iter<char, H>
where
	H: ThreadPoolHandle,
{
	type Item = char;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		char_to_iter_range!(self.drive_unindexed(consumer))
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.len())
	}
}

impl<H> IndexedParallelIterator<H> for Iter<char, H>
where
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		char_to_iter_range!(self.drive(consumer))
	}

	fn len(&self) -> usize {
		let start = self.range.start as u32;
		let end = self.range.end as u32;
		if start < end {
			let mut count = end - start;
			if start < 0xd800 && 0xe000 <= end {
				count -= 0x800
			}
			count as usize
		} else {
			0
		}
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		char_to_iter_range!(self.with_producer(callback))
	}
}

/// Implements [`ParallelIterator`] for inclusive Ranges.
///
/// Analogous to [`Iter`] but for [`RangeInclusive`].
pub struct IterInclusive<T, H>
where
	H: ThreadPoolHandle,
{
	/// The underlying [`RangeInclusive`]
	range: RangeInclusive<T>,
	/// The handle to the threadpool this is in spawned in.
	_handle: PhantomData<H>,
}

impl<T, H> IterInclusive<T, H>
where
	H: ThreadPoolHandle,
	T: Ord + Copy,
{
	/// Get the bounds of a [`RangeInclusive`]
	///
	/// Returns [`Some(_)`](Some) if the range is bounded and not empty, otherwise
	/// [`None`]
	fn bounds(&self) -> Option<(T, T)> {
		let start = *self.range.start();
		let end = *self.range.end();
		if start <= end && self.range == (start..=end) {
			Some((start, end))
		} else {
			None
		}
	}
}

impl<T, H> IntoParallelIterator<H> for IterInclusive<T, H>
where
	H: ThreadPoolHandle,
	IterInclusive<T, H>: ParallelIterator<H>,
{
	iterator_into_iter_impl!();
}

impl<T, H> IntoParallelIterator<H> for RangeInclusive<T>
where
	H: ThreadPoolHandle,
	IterInclusive<T, H>: ParallelIterator<H>,
{
	type Iter = IterInclusive<T, H>;

	type Item = <IterInclusive<T, H> as ParallelIterator<H>>::Item;

	fn into_par_iter(self) -> Self::Iter {
		IterInclusive {
			range: self,
			_handle: PhantomData,
		}
	}
}

/// Convert this to a normal range iterable and execute the given `method` with
/// the given `args` on it.
macro_rules! to_normal_range {
    ($self:ident . $method:ident ( $($arg:expr),* )) => {
		if let Some((start, end)) = $self.bounds() {
			if let Some(end) = end.checked_add(1) {
				(start..end).into_par_iter().$method($($arg),*)
			} else {
				(start..end).into_par_iter().$method($($arg),*)
			}
		} else {
			empty::<Self::Item>().$method($($arg),*)
		}
    };
}

/// Implement [`ParallelIterator`] for [`IterInclusive<$t>`]`.
macro_rules! unindexed_inclusive_impl {
	($t:ty) => {
		impl<H> ParallelIterator<H> for IterInclusive<$t, H>
		where
			H: ThreadPoolHandle,
		{
			type Item = $t;

			fn drive_unindexed<C>(self, consumer: C) -> C::Result
			where
				C: UnindexedConsumer<Self::Item, H>,
			{
				to_normal_range!(self.drive_unindexed(consumer))
			}
		}
	};
}

/// Implement [`IndexedParallelIterator`] for [`IterInclusive<$t>`]`.
macro_rules! indexed_inclusive_impl {
	($t:ty) => {
		unindexed_inclusive_impl!($t);

		impl<H> IndexedParallelIterator<H> for IterInclusive<$t, H>
		where
			H: ThreadPoolHandle,
		{
			fn drive<C>(self, consumer: C) -> C::Result
			where
				C: Consumer<Self::Item, H>,
			{
				to_normal_range!(self.drive(consumer))
			}

			fn len(&self) -> usize {
				self.range.len()
			}

			fn with_producer<CB>(self, callback: CB) -> CB::Output
			where
				CB: ProducerCallback<Self::Item, H>,
			{
				to_normal_range!(self.with_producer(callback))
			}
		}
	};
}

// For all T: ExactSizeIterator
indexed_inclusive_impl!(u8);
indexed_inclusive_impl!(u16);
indexed_inclusive_impl!(i8);
indexed_inclusive_impl!(i16);

// For all T: !ExactSizeIterator
unindexed_inclusive_impl!(u32);
unindexed_inclusive_impl!(u64);
unindexed_inclusive_impl!(u128);
unindexed_inclusive_impl!(usize);
unindexed_inclusive_impl!(i32);
unindexed_inclusive_impl!(i64);
unindexed_inclusive_impl!(i128);
unindexed_inclusive_impl!(isize);

/// Executs a `method` for an [`IterInclusive`] on an iterated range.
macro_rules! char_to_inclusive_range {
    ($self:ident . $method:ident ( $($arg:expr),* )) => {
		if let Some((start, end)) = $self.bounds() {
			let start = start as u32;
			let end = end as u32;
			if start < 0xd800 && 0xe000 <= end {
				(start..0xd800)
					.into_par_iter()
					.chain(0xe000..end + 1)
					// Safety: we know this is valid as the user can't input a character
					// > 0x0x10ffff and the only other gap was checked
					.map(|codepoint| unsafe { char::from_u32_unchecked(codepoint) })
					.$method($($arg),*)
			} else {
				(start..end + 1)
					.into_par_iter()
					// Safety: we know this is valid as the user can't input a character
					// > 0x0x10ffff and the only other gap was checked
					.map(|codepoint| unsafe { char::from_u32_unchecked(codepoint) })
					.$method($($arg),*)
			}
		} else {
			empty::<char>().$method($($arg),*)
		}
    };
}

impl<H> ParallelIterator<H> for IterInclusive<char, H>
where
	H: ThreadPoolHandle,
{
	type Item = char;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		char_to_inclusive_range!(self.drive_unindexed(consumer))
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.len())
	}
}

impl<H> IndexedParallelIterator<H> for IterInclusive<char, H>
where
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		char_to_inclusive_range!(self.drive(consumer))
	}

	fn len(&self) -> usize {
		if let Some((start, end)) = self.bounds() {
			let start = start as u32;
			let end = end as u32;
			let mut count = end - start;
			if start < 0xd800 && 0xe000 <= end {
				count -= 0x800
			}
			(count + 1) as usize
		} else {
			0
		}
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		char_to_inclusive_range!(self.with_producer(callback))
	}
}
