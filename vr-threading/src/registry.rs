//! Cross-thread management

use core::{num::NonZeroUsize, sync::atomic::Ordering};
use std::thread::available_parallelism;

use vr_core::{
	cell::Cell,
	debug_or_loom_assert,
	sync::{atomic::AtomicUsize, Arc},
	thread,
	unwind::{self, AbortIfPanic},
};

use crossbeam_deque::Injector;

use crate::{
	job::JobRef,
	random::XorShift64Star,
	sync::{
		deque::{Steal, Stealer, Worker},
		latch::{AsCoreLatch, CoreLatch, OnceLatch},
		sleep::Sleep,
	},
	threadpool::{ThreadPoolBuilder, ThreadPoolCreationError},
	PanicHandler,
};

pub mod join;
pub mod scope;
pub mod task;

/// Type representing a Job in the queues of the threadpool
pub type Job = JobRef;

/// The structure managing all threads
pub struct Registry {
	/// The public information of each thread in the threadpool
	thread_infos: Vec<ThreadInfo>,
	/// The global job queue which is an overflow safe for a worker threads own
	/// queue.
	global_queue: Injector<Job>,
	/// Structure managing the sleeping states of the threads in this pool.
	sleep: Sleep,
	/// Function that will be called on panics
	panic_handler: Option<Box<PanicHandler>>,
	/// Number of termination influencing events
	termination_count: AtomicUsize,
}

/// Public facing information of a [`WorkerThread`].
struct ThreadInfo {
	/// The stealer end of the worker's queue
	stealer: Stealer<Job>,
	/// The termination latch for this worker, i. e. The latch the topmost
	/// wait_until loop is waiting for.
	termination: OnceLatch,
}

/// The workers private information
pub struct WorkerThread {
	/// A reference to the registry
	registry: Arc<Registry>,
	/// The Worker end of its own queue
	worker: Worker<Job>,
	/// The index the thread has in [`Registry::thread_infos`]
	index: usize,
	/// The random number generator for picking victims for task robbing
	rng: XorShift64Star,
}

/// Builder for a thread in the threadpool
pub struct ThreadBuilder {
	/// The name the worker
	name: Option<String>,
	/// The stack size of the worker
	stack_size: Option<usize>,
	/// The worker end of it's queue.
	worker: Worker<Job>,
	/// The registry this Thread belongs to.
	registry: Arc<Registry>,
	/// The index it has in the ThreadPool's Vectors
	index: usize,
}

impl ThreadBuilder {
	/// Spawn a new thread using the builder
	pub fn spawn(self) -> Result<thread::JoinHandle<()>, std::io::Error> {
		let mut b = thread::Builder::new();
		if let Some(stack_size) = self.stack_size {
			b = b.stack_size(stack_size);
		}
		if let Some(name) = &self.name {
			b = b.name(name.to_string());
		}
		// Safety: This is a new thread.
		unsafe { b.spawn(|| main_loop(self)) }
	}
}

impl From<ThreadBuilder> for WorkerThread {
	fn from(value: ThreadBuilder) -> Self {
		Self {
			registry: value.registry,
			worker: value.worker,
			index: value.index,
			rng: XorShift64Star::new(),
		}
	}
}

thread_local! {
	/// Sketchy but the worker state is stored on the stack of the worker on entry,
	/// so this is guaranteed to be valid
	#[allow(clippy::thread_local_initializer_can_be_made_const)]
	static WORKER_HANDLE: Cell<*const WorkerThread> = Cell::new(core::ptr::null());
}
impl Registry {
	/// Create a new registry that will execute the main function in the threadpool
	pub fn spawn<F, T>(builder: ThreadPoolBuilder, main: F) -> Result<T, ThreadPoolCreationError>
	where
		F: FnOnce() -> T,
	{
		if !WorkerThread::current().is_null() {
			return Err(ThreadPoolCreationError::AlreadyInThreadPool);
		}

		let available_parallelism = available_parallelism()?;
		let mut thread_infos = vec![];
		let mut workers = vec![];
		let size = builder.num_threads.unwrap_or(available_parallelism).into();
		for _ in 0..size {
			let worker = Worker::new(builder.worker_capacity.unwrap_or(256));
			thread_infos.push(ThreadInfo {
				stealer: worker.stealer(),
				termination: OnceLatch::new(),
			});
			workers.push(worker);
		}

		let registry = Arc::new(Registry {
			thread_infos,
			global_queue: Injector::new(),
			sleep: Sleep::new(size),
			panic_handler: builder.panic_handler,
			termination_count: AtomicUsize::new(1),
		});

		let mut name_func = builder
			.get_thread_name
			.unwrap_or_else(|| Box::new(|n| format!("Worker {n}")));
		let mut workers = workers
			.into_iter()
			.enumerate()
			.map(|(idx, worker)| ThreadBuilder {
				worker,
				registry: registry.clone(),
				name: Some(name_func(idx)),
				index: idx,
				stack_size: builder.stack_size,
			})
			.collect::<Vec<_>>();

		let mut handles = vec![];
		for worker in workers.drain(1..) {
			handles.push(worker.spawn()?)
		}
		let worker = workers.pop().unwrap().into();
		// Safety:
		//	- We know this is null so we aren't overwriting state
		//	- The worker will outlive the threadpool here.
		unsafe {
			WorkerThread::set_current(&worker);
		}

		let ret = unwind::halt_unwinding(main);

		registry.terminate();

		// Safety: We know the registry is still active, otherwise the latch is set
		unsafe { worker.wait_until(&worker.registry.thread_infos[worker.index].termination) };

		for handle in handles {
			handle.join().unwrap();
		}
		// Safety: We are done and all threads funished, the worker will be cleaned
		// up so this is not a dangling reference
		unsafe {
			WorkerThread::unset_current();
		}

		match ret {
			Ok(v) => Ok(v),
			Err(payload) => unwind::resume_unwinding(payload),
		}
	}

	/// Pop a job from the global queue
	pub fn pop_global_job(&self) -> Option<Job> {
		loop {
			match self.global_queue.steal() {
				crossbeam_deque::Steal::Empty => return None,
				crossbeam_deque::Steal::Success(job) => return Some(job),
				crossbeam_deque::Steal::Retry => {}
			}
		}
	}

	/// Pushes a [`Job`] onto the [`Registry::global_queue`], that will be used
	/// if the worker's queue is full or injecting a job from the outside.
	pub fn push_global_queue(&self, job: Job) {
		let queue_was_empty = self.global_queue.is_empty();
		self.global_queue.push(job);
		self.sleep.new_global_jobs(1, queue_was_empty);
	}

	/// Notify a worker that the latch it was waiting on is set
	pub fn notify_latch_set(&self, worker_index: usize) {
		self.sleep.notify_latch_set(worker_index);
	}

	/// Check if the global queue is not empty
	pub fn has_global_jobs(&self) -> bool {
		!self.global_queue.is_empty()
	}

	/// Catch an unwind on a function in this threadpool aborting if no
	/// panic handler is set, and otherwise calling it.
	pub fn catch_unwind<F>(&self, f: F)
	where
		F: FnOnce(),
	{
		if let Err(payload) = unwind::halt_unwinding(f) {
			let abort_guard = AbortIfPanic {
				target: "ThreadPool",
			};
			if let Some(handler) = &self.panic_handler {
				handler(payload);
				core::mem::forget(abort_guard);
			}
		}
	}

	/// Send a termination signal
	pub fn terminate(&self) {
		if self.termination_count.fetch_sub(1, Ordering::AcqRel) - 1 == 0 {
			for (idx, thread_info) in self.thread_infos.iter().enumerate() {
				thread_info.termination.set_and_wake(self, idx);
			}
		}
	}

	/// Increment the number of active loose tasks, so that the registry may wait
	/// until all are completed
	pub fn increment_terminate_count(&self) {
		let previous = self.termination_count.fetch_add(1, Ordering::AcqRel);
		debug_or_loom_assert!(previous != 0, "registry ref count incremented from zero");
		debug_or_loom_assert!(previous != usize::MAX, "overflow in registry ref count")
	}

	/// Get the number of worker threads on this registry
	pub fn num_threads(&self) -> NonZeroUsize {
		NonZeroUsize::new(self.thread_infos.len()).unwrap()
	}
}

impl WorkerThread {
	/// Sets the current of this [`WorkerThread`].
	///
	/// # Panics
	///
	/// Panics in debug or loom builds if the thread local worker handle is not
	/// null meaning it was already set.
	///
	/// # Safety
	///
	/// current must outlive the duration this thread is active.
	pub unsafe fn set_current(current: *const WorkerThread) {
		WORKER_HANDLE.with(|handle: &Cell<*const WorkerThread>| {
			debug_or_loom_assert!(handle.get().is_null());
			handle.set(current)
		})
	}

	/// Get the current worker thread, returns null if not in a thread pool
	pub fn current() -> *const WorkerThread {
		WORKER_HANDLE.with(Cell::get)
	}

	/// Unsets the current of this [`WorkerThread`].
	///
	/// # Panics
	///
	/// Panics in debug or loom builds if the thread local worker handle is
	/// null meaning it was already unset.
	///
	/// # Safety
	///
	/// current must outlive the duration this thread is active.
	pub unsafe fn unset_current() {
		WORKER_HANDLE.with(|handle: &Cell<*const WorkerThread>| {
			debug_or_loom_assert!(!handle.get().is_null());
			handle.set(core::ptr::null());
		})
	}

	/// Push a [`Job`] unto the worker's queue.
	///
	/// Pushes a [`Job`] unto the global queue, the job is being constructed
	/// with the help of the `job_ref`. The return type is the tuple of the
	/// job's owner index and the job id on that thread.
	///
	/// # Safety
	///
	/// The JobRef must be valid as long as it is in the threadpool's queues.
	pub unsafe fn push(&self, job_ref: JobRef) {
		let queue_was_empty = self.worker.is_empty();
		if let Err(job) = self.worker.push(job_ref) {
			self.registry.push_global_queue(job);
		} else {
			self.registry.sleep.new_jobs(1, queue_was_empty);
		}
	}

	/// Get a job from the local queue
	pub fn take_local_job(&self) -> Option<Job> {
		self.worker.pop()
	}

	/// Steal tasks from other threads in the threadpool
	pub fn steal(&self) -> Option<Job> {
		debug_or_loom_assert!(self.worker.is_empty());

		let thread_infos = self.registry.thread_infos.as_slice();
		let num_threads = thread_infos.len();
		if num_threads <= 1 {
			return None;
		}
		loop {
			let mut retry = false;
			let start = self.rng.next() as usize % num_threads;
			let job = (start..num_threads)
				.chain(0..start)
				.filter(|&i| i != self.index)
				.find_map(|victim_index| {
					let victim = &thread_infos[victim_index];
					match victim.stealer.steal() {
						Steal::Success(job) => Some(job),
						Steal::Empty => None,
						Steal::Retry => {
							retry = true;
							None
						}
					}
				});
			if job.is_some() || !retry {
				return job;
			}
		}
	}

	/// Tries to find work by first taking from the local queue and then checking
	/// other threads queues for tasks to steal
	pub fn find_work(&self) -> Option<Job> {
		self.take_local_job()
			.or_else(|| self.steal())
			.or_else(|| self.registry.pop_global_job())
	}

	/// Wait until a latch is set
	///
	/// # Safety
	///
	/// Look at [`wait_until_impl`](WorkerThread::wait_until_impl).
	pub unsafe fn wait_until<L>(&self, latch: &L)
	where
		L: AsCoreLatch + ?Sized,
	{
		let latch = latch.as_core_latch();
		if !latch.probe() {
			// Safety: ensured by caller
			unsafe { self.wait_until_impl(latch) }
		}
	}

	/// wait until a latch is set executing tasks in the meantime.
	///
	/// # Safety
	///
	/// This will steal work from the local and other queues until a latch is set.
	/// The caller is responsible for making sure no jobs lie twice on the queue
	/// or the job contents are not dropped before the latch is set.
	pub unsafe fn wait_until_impl(&self, latch: &CoreLatch) {
		let abort_guard = unwind::AbortIfPanic {
			target: "ThreadPool",
		};

		// Safety: The safety must be ensured by the caller.
		unsafe {
			'outer: while !latch.probe() {
				if let Some(job) = self.take_local_job() {
					// Safety: ensured by caller
					job.execute();
					continue;
				}

				let mut idle_state = self.registry.sleep.start_looking(self.index);
				while !latch.probe() {
					if let Some(job) = self.find_work() {
						// Safety: ensured by caller
						self.registry.sleep.work_found();
						job.execute();
						continue 'outer;
					} else {
						self.registry
							.sleep
							.no_work_found(&mut idle_state, latch, || {
								self.registry.has_global_jobs()
							})
					}
				}

				self.registry.sleep.work_found();
				break;
			}
		}
		core::mem::forget(abort_guard)
	}

	/// Get a reference to the underlying registry
	pub fn registry(&self) -> &Arc<Registry> {
		&self.registry
	}

	/// Get the current worker's index in the registry
	pub fn index(&self) -> usize {
		self.index
	}
}

/// A worker thread's main loop
///
/// # Safety
///
/// This function must be executed **once** for each thread.
pub unsafe fn main_loop(builder: ThreadBuilder) {
	// Safety: Caller ensured
	unsafe {
		let worker_thread = builder.into();
		WorkerThread::set_current(&worker_thread);
		let abort_guard = AbortIfPanic {
			target: "ThreadPool",
		};
		worker_thread
			.wait_until(&worker_thread.registry.thread_infos[worker_thread.index].termination);
		core::mem::forget(abort_guard)
	}
}
