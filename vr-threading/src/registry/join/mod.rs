//! Module containing join implementation
use core::any::Any;

use vr_core::{debug_or_loom_assert, unwind};

use crate::{
	job::StackJob,
	sync::latch::{AsCoreLatch, Latch, SpinLatch},
	Context,
};

use super::WorkerThread;

impl WorkerThread {
	/// Starts two continous execution units and waits until both are finished returning the return
	/// values
	pub fn join<F1, F2, T1, T2>(&self, task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce() -> T1,
		T1: Send,
		F2: Send + FnOnce() -> T2,
		T2: Send,
	{
		/// Wrapper to make `task1` and `task2` context aware
		fn call<T>(f: impl FnOnce() -> T) -> impl FnOnce(Context) -> T {
			move |_| f()
		}

		self.join_context(call(task1), call(task2))
	}

	/// Similar to [`WorkerThread::join`] but provides infomration whether
	/// the closures are executed on another thread or not.
	pub fn join_context<F1, F2, T1, T2>(&self, task1: F1, task2: F2) -> (T1, T2)
	where
		F1: Send + FnOnce(Context) -> T1,
		F2: Send + FnOnce(Context) -> T2,
		T2: Send,
		T1: Send,
	{
		/// Recast because it is being used on the current thread, so it was
		/// not moved.
		fn call_1<T>(f: impl FnOnce(Context) -> T) -> impl FnOnce() -> T {
			move || f(Context::new(false))
		}

		/// At this point we are responsible for owning this so we recast to
		/// a bool.
		fn call_2<T>(f: impl FnOnce(Context) -> T) -> impl FnOnce(bool) -> T {
			move |migrated| f(Context::new(migrated))
		}

		// Safety:
		//	- job_2 is alive for the whole duration of the stack frame, and will
		//	be completed upon exit of the main loop
		//	- the other jobs poped are self-resposible for being kept alive.
		unsafe {
			let job_2 = StackJob::new(call_2(task2), SpinLatch::new(&self.registry, self.index));
			let job_2_ref = job_2.as_job_ref();
			let job_2_id = job_2_ref.id();
			self.push(job_2_ref);

			let result_1 = match unwind::halt_unwinding(call_1(task1)) {
				Ok(res) => res,
				Err(payload) => self.join_recover_from_panic(&job_2.latch, payload),
			};

			while !job_2.latch.probe() {
				if let Some(job) = self.take_local_job() {
					if job_2_id == job.id() {
						let result_2 = job_2.run_inline(false);
						return (result_1, result_2);
					} else {
						job.execute()
					}
				} else {
					self.wait_until(&job_2.latch);
					debug_or_loom_assert!(job_2.latch.probe());
					break;
				}
			}
			(result_1, job_2.into_result())
		}
	}

	/// Recover if job 1 panicked as to not cause UB this wil wait until
	/// job2 was executed.
	pub unsafe fn join_recover_from_panic<L>(
		&self,
		job_2_latch: &L,
		payload: Box<dyn Any + Send>,
	) -> !
	where
		L: AsCoreLatch + ?Sized,
	{
		// Safety: We know this is kept alive by the stackframe of `join_context`
		unsafe { self.wait_until(job_2_latch) };
		unwind::resume_unwinding(payload)
	}
}

#[cfg(test)]
mod tests {
	use core::num::NonZeroUsize;
	use std::sync::Barrier;

	use rand::{distributions::Standard, Rng, SeedableRng};
	use rand_xorshift::XorShiftRng;

	use crate::{handle::DefaultHandle, threadpool::ThreadPoolBuilder, ThreadPoolHandle};

	use super::*;

	#[test]
	#[should_panic(expected = "success")]
	fn panic_propagate_a() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				DefaultHandle::join(|| panic!("success"), || ());
			})
			.unwrap();
	}

	#[test]
	#[should_panic(expected = "success")]
	fn panic_propagate_b() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				DefaultHandle::join(|| (), || panic!("success"));
			})
			.unwrap();
	}

	#[test]
	#[should_panic(expected = "success")]
	fn panic_propagate_both() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				DefaultHandle::join(|| panic!("success"), || panic!("failure"));
			})
			.unwrap();
	}

	#[test]
	fn panic_b_still_executes() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				let mut x = false;
				match unwind::halt_unwinding(|| {
					DefaultHandle::join(|| panic!("success"), || x = true)
				}) {
					Ok(_) => panic!("failed to propagate panic from closure a"),
					Err(_) => assert!(x, "closure b failed to execute"),
				};
			})
			.unwrap();
	}

	#[test]
	fn join_context_neither() {
		ThreadPoolBuilder::new()
			.num_threads(NonZeroUsize::new(1).unwrap())
			.spawn(|| {
				let (a_migrated, b_migrated) =
					DefaultHandle::join_context(|a| a.migrated(), |b| b.migrated());
				assert!(!a_migrated);
				assert!(!b_migrated);
			})
			.unwrap();
	}

	#[test]
	fn join_context_second() {
		ThreadPoolBuilder::new()
			.num_threads(NonZeroUsize::new(2).unwrap())
			.spawn(|| {
				let barrier = Barrier::new(2);
				let (a_migrated, b_migrated) = DefaultHandle::join_context(
					|a| {
						barrier.wait();
						a.migrated()
					},
					|b| {
						barrier.wait();
						b.migrated()
					},
				);

				assert!(!a_migrated);
				assert!(b_migrated);
			})
			.unwrap();
	}

	fn quick_sort<T>(v: &mut [T])
	where
		T: PartialOrd + Send,
	{
		if v.len() <= 1 {
			return;
		}

		let mid = partition(v);
		let (lo, hi) = v.split_at_mut(mid);
		DefaultHandle::join(|| quick_sort(lo), || quick_sort(hi));
	}

	fn partition<T>(v: &mut [T]) -> usize
	where
		T: PartialOrd + Send,
	{
		let pivot = v.len() - 1;
		let mut i = 0;
		for j in 0..pivot {
			if v[j] <= v[pivot] {
				v.swap(i, j);
				i += 1;
			}
		}
		v.swap(i, pivot);
		i
	}

	fn seeded_rng() -> XorShiftRng {
		let mut seed = <XorShiftRng as SeedableRng>::Seed::default();
		(0..).zip(seed.as_mut()).for_each(|(i, x)| *x = i);
		XorShiftRng::from_seed(seed)
	}

	#[test]
	fn sort() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				let rng = seeded_rng();
				let mut data = rng
					.sample_iter(&Standard)
					.take(6 * 1024)
					.collect::<Vec<u32>>();
				let mut sorted_data = data.clone();
				sorted_data.sort();
				quick_sort(&mut data);
				assert_eq!(data, sorted_data);
			})
			.unwrap();
	}
}
