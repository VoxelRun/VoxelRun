//! Scope functionality of the thread pool

use core::{any::Any, marker::PhantomData, mem::ManuallyDrop, sync::atomic::Ordering};

use vr_core::{sync::atomic::AtomicPtr, unwind};

use crate::{
	job::{HeapJob, JobRef},
	sync::latch::{CountLatch, Latch},
};

use super::WorkerThread;

/// Represents a Fork-Join scope which can be used to spawn any number of tasks.
pub struct Scope<'scope> {
	/// if some job panicked, the error will be stored here; it will be propagated
	/// to the one who created the scope.
	panic: AtomicPtr<Box<dyn Any + Send + 'static>>,
	/// latch to track the job counts
	job_completion_latch: CountLatch<'scope>,
	/// You can think of a scope as containing a list of closures to execute,
	/// all of which outlive `'scope`.  They're not actually required to be
	/// `Sync`, but it's still safe to let the `Scope` implement `Sync` because
	/// the closures are only *moved* across threads to be executed.
	#[allow(clippy::type_complexity)]
	_marker: PhantomData<Box<dyn FnOnce(&Scope<'scope>) + Send + Sync + 'scope>>,
}

impl WorkerThread {
	/// Create a new fork-join scope on the [`WorkerThread`]
	/// For more details look at [`crate::handle::DefaultHandle::scope`]
	///
	/// # Panics
	///
	/// If one spawn panicked this will wait for all to complete and then panic
	pub fn scope<'scope, F, T>(&'scope self, task: F) -> T
	where
		F: FnOnce(&Scope<'scope>) -> T,
		T: Send,
	{
		let scope = Scope::<'scope>::new(self);
		scope.complete(|| task(&scope), self)
	}
}

impl<'scope> Scope<'scope> {
	/// Creates a new [`Scope`].
	fn new(owner: &'scope WorkerThread) -> Self {
		Self {
			job_completion_latch: CountLatch::new(owner),
			panic: AtomicPtr::new(core::ptr::null_mut()),
			_marker: PhantomData,
		}
	}

	/// Spawns a job into the fork-join scope `self`. This job will execute
	/// sometime before the fork-join scope completes. The job is specified
	/// as a closure, and this closure receives its own reference to the scope
	/// `self` as argument. This can be used to create new jobs into `self`.
	///
	/// # Examples
	/// ```
	/// use vr_threading::{ThreadPoolHandle, threadpool, handle::DefaultHandle};
	///
	/// threadpool::ThreadPoolBuilder::new().spawn(|| {
	///     let mut value_1 = None;
	///     let mut value_2 = None;
	///     let mut value_3 = None;
	///     DefaultHandle::scope(|s| {
	///         s.spawn(|s1| {
	///             value_1 = Some(1);
	///             s1.spawn(|_| {
	///                 value_2 = Some(2);
	///             });
	///         });
	///
	///         s.spawn(|_| {
	///             value_3 = Some(3)
	///         });
	///     });
	///
	///     assert_eq!(value_1, Some(1));
	///     assert_eq!(value_2, Some(2));
	///     assert_eq!(value_3, Some(3));
	/// }).unwrap();
	/// ```
	///
	/// # See also
	///
	/// The [`scope`](crate::handle::DefaultHandle::scope) function has more
	/// extensive documentation about task spawning.
	pub fn spawn<F>(&self, func: F)
	where
		F: FnOnce(&Scope<'scope>) + Send + 'scope,
	{
		let scope_ptr = ScopePtr { ptr: self };
		// Safety: The scope will outlive this job
		let job = HeapJob::new(move || unsafe {
			// Safety: this job will execute until the scope ends;
			let scope = scope_ptr.as_ref();
			Scope::execute_job(scope, move || func(scope))
		});

		let job_ref = self.heap_job_ref(job);

		let current = WorkerThread::current();
		assert!(
			!current.is_null(),
			"Scope::spawn was executed outside a threadpool"
		);

		// Safety: As long as the threadpool is alive this will never be
		// invalidated
		let current = unsafe { &*current };
		// Safety: this job will execute until the scope ends;
		unsafe { current.push(job_ref) };
	}

	/// Create a job ref from a HeapJob that is ensured to not outlive this
	/// scope.
	fn heap_job_ref<F>(&self, job: Box<HeapJob<F>>) -> JobRef
	where
		F: FnOnce() + Send + 'scope,
	{
		// Safety: Ensured by `Scope::complete`, the reference will not outlive the
		// scope
		unsafe {
			self.job_completion_latch.increment();
			job.into_job_ref()
		}
	}

	/// Execute the initial task of the scope and wait for all spawns to finish
	///
	/// # Panics
	///
	/// If one spawn panicked this will wait for all to complete and then panic
	fn complete<F, T>(&self, func: F, owner: &WorkerThread) -> T
	where
		F: FnOnce() -> T,
	{
		// Safety: self will be valid for the scope.
		let result = unsafe { Self::execute_job_closure(self, func) };
		// Safety: We know all jobs in the queues that live for the scope are alive
		unsafe { owner.wait_until(&self.job_completion_latch) };
		self.maybe_propagate_panic();
		result.unwrap()
	}

	/// Execute a job in the current scope on the current thread
	///
	/// # Safety
	///
	/// `this` must be valid.
	unsafe fn execute_job<F>(this: *const Self, func: F)
	where
		F: FnOnce(),
	{
		// Safety: Ensured by caller
		let _: Option<()> = unsafe { Self::execute_job_closure(this, func) };
	}

	/// Execute a job in the current scope on the current thread, return its
	/// return value.
	///
	/// # Safety
	///
	/// `this` must be valid.
	unsafe fn execute_job_closure<F, T>(this: *const Self, func: F) -> Option<T>
	where
		F: FnOnce() -> T,
	{
		// Safety: Ensured by caller
		unsafe {
			let result = match unwind::halt_unwinding(func) {
				Ok(v) => Some(v),
				Err(err) => {
					(*this).job_panicked(err);
					None
				}
			};

			(*this).job_completion_latch.set();
			result
		}
	}

	/// Set the panic payload of the [`Scope`] on first call
	fn job_panicked(&self, err: Box<dyn Any + Send + 'static>) {
		if self.panic.load(Ordering::Relaxed).is_null() {
			let mut err = ManuallyDrop::new(Box::new(err));
			let err_ptr: *mut Box<dyn Any + Send + 'static> = &mut **err;
			if self
				.panic
				.compare_exchange(
					core::ptr::null_mut(),
					err_ptr,
					Ordering::Release,
					Ordering::Relaxed,
				)
				.is_err()
			{
				let _: Box<Box<_>> = ManuallyDrop::into_inner(err);
			}
		}
	}

	/// If a job panicked propagate the panic
	fn maybe_propagate_panic(&self) {
		let payload = self.panic.swap(core::ptr::null_mut(), Ordering::Relaxed);
		if !payload.is_null() {
			// Safety: the payload is static, when it was set. look at `job_panicked`
			let payload = unsafe { Box::from_raw(payload) };
			unwind::resume_unwinding(*payload);
		}
	}
}

/// Used to capture a scope `&Self` pointer in jobs, without faking a lifetime.
///
/// Unsafe code is still required to dereference the pointer, but that's fine in
/// scope jobs that are guaranteed to execute before the scope ends.
struct ScopePtr<T> {
	/// The underlying pointe to the Self
	ptr: *const T,
}

// Safety: !Send for raw pointers is not for safety, just as a lint
unsafe impl<T: Send> Send for ScopePtr<T> {}

// Safety: !Sync for raw pointers is not for safety, just as a lint
unsafe impl<T: Sync> Sync for ScopePtr<T> {}

impl<T> ScopePtr<T> {
	/// helper to avoid disjoint captures of `scope_ptr.ptr`
	///
	/// # Safety
	///
	/// Ensure no mutable alias, and validness of the inner pointer.
	unsafe fn as_ref(&self) -> &T {
		// Safety: Ensured by caller
		unsafe { &*self.ptr }
	}
}

#[cfg(test)]
mod tests;
