use core::{iter::once, num::NonZeroUsize, sync::atomic::AtomicUsize};
use std::{sync::Mutex, vec};

use rand::{Rng, SeedableRng};
use rand_xorshift::XorShiftRng;

use crate::{handle::DefaultHandle, threadpool::ThreadPoolBuilder};

use super::*;

#[test]
#[should_panic(expected = "success")]
fn panic_propagate_scope() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			DefaultHandle::scope(|_| panic!("success"));
		})
		.unwrap();
}

#[test]
#[should_panic(expected = "success")]
fn panic_propagate_spawn() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			DefaultHandle::scope(|s| s.spawn(|_| panic!("success")));
		})
		.unwrap();
}

#[test]
#[should_panic(expected = "success")]
fn panic_propagate_nested_spawn() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			DefaultHandle::scope(|s| s.spawn(|s| s.spawn(|s| s.spawn(|_| panic!("success")))));
		})
		.unwrap();
}

#[test]
#[should_panic(expected = "success")]
fn panic_propagate_nested_scope() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			DefaultHandle::scope(|s| {
				s.spawn(|_| DefaultHandle::scope(|s| s.spawn(|_| panic!("success"))))
			});
		})
		.unwrap();
}

#[test]
#[cfg_attr(not(panic = "unwind"), ignore)]
fn panic_propagate_still_execute_1() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut x = false;
			let result = unwind::halt_unwinding(|| {
				DefaultHandle::scope(|s| {
					s.spawn(|_| panic!("success"));
					s.spawn(|_| x = true);
				})
			});
			match result {
				Ok(_) => panic!("Failed to propagate panic"),
				Err(_) => assert!(x, "Job b failed to execute"),
			}
		})
		.unwrap();
}

#[test]
#[cfg_attr(not(panic = "unwind"), ignore)]
fn panic_propagate_still_execute_2() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut x = false;
			let result = unwind::halt_unwinding(|| {
				DefaultHandle::scope(|s| {
					s.spawn(|_| x = true);
					s.spawn(|_| panic!("success"));
				})
			});
			match result {
				Ok(_) => panic!("Failed to propagate panic"),
				Err(_) => assert!(x, "Job  failed to execute"),
			}
		})
		.unwrap();
}

#[test]
#[cfg_attr(not(panic = "unwind"), ignore)]
fn panic_propagate_still_execute_3() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut x = false;
			let result = unwind::halt_unwinding(|| {
				DefaultHandle::scope(|s| {
					s.spawn(|_| x = true);
					panic!("success");
				})
			});
			match result {
				Ok(_) => panic!("Failed to propagate panic"),
				Err(_) => assert!(x, "panic after spawn, spawn failed to execute"),
			}
		})
		.unwrap();
}

#[test]
#[cfg_attr(not(panic = "unwind"), ignore)]
fn panic_propagate_still_execute_4() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut x = false;
			let result = unwind::halt_unwinding(|| {
				DefaultHandle::scope(|s| {
					s.spawn(|_| panic!("success"));
					x = true
				})
			});
			match result {
				Ok(_) => panic!("Failed to propagate panic"),
				Err(_) => assert!(x, "panic in spawn tainted scope"),
			}
		})
		.unwrap()
}

#[test]
fn test_order() {
	ThreadPoolBuilder::new()
		.num_threads(NonZeroUsize::new(1).unwrap())
		.spawn(|| {
			let vec = {
				let vec = Mutex::new(vec![]);
				DefaultHandle::scope(|scope| {
					let vec = &vec;
					for i in 0..10 {
						scope.spawn(move |scope| {
							for j in 0..10 {
								scope.spawn(move |_| vec.lock().unwrap().push(i * 10 + j))
							}
						})
					}
				});
				vec.into_inner().unwrap()
			};

			let expected = (0..100).rev().collect::<Vec<_>>();
			assert_eq!(vec, expected);
		})
		.unwrap()
}

#[test]
fn test_nested_order() {
	ThreadPoolBuilder::new()
		.num_threads(NonZeroUsize::new(1).unwrap())
		.spawn(|| {
			let vec = {
				let vec = Mutex::new(vec![]);
				DefaultHandle::scope(|scope| {
					let vec = &vec;
					for i in 0..10 {
						scope.spawn(move |_| {
							DefaultHandle::scope(move |scope| {
								for j in 0..10 {
									scope.spawn(move |_| vec.lock().unwrap().push(i * 10 + j))
								}
							})
						})
					}
				});
				vec.into_inner().unwrap()
			};
			let expected = (0..100).rev().collect::<Vec<_>>();
			assert_eq!(vec, expected);
		})
		.unwrap()
}

fn pseudo_join<F1, F2>(f: F1, g: F2)
where
	F1: FnOnce() + Send,
	F2: FnOnce() + Send,
{
	DefaultHandle::scope(|s| {
		s.spawn(|_| g());
		f()
	});
}

fn quick_sort<T>(v: &mut [T])
where
	T: PartialOrd + Send,
{
	if v.len() <= 1 {
		return;
	}

	let mid = partition(v);
	let (lo, hi) = v.split_at_mut(mid);
	pseudo_join(|| quick_sort(lo), || quick_sort(hi))
}

fn partition<T>(v: &mut [T]) -> usize
where
	T: PartialOrd + Send,
{
	let pivot = v.len() - 1;
	let mut i = 0;
	for j in 0..pivot {
		if v[j] < v[pivot] {
			v.swap(i, j);
			i += 1;
		}
	}
	v.swap(i, pivot);
	i
}

fn is_sorted<T>(v: &[T]) -> bool
where
	T: PartialOrd + Send,
{
	(1..v.len()).all(|i| v[i - 1] <= v[i])
}

#[test]
fn scope_join() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut v = (0..256).rev().collect::<Vec<_>>();
			quick_sort(&mut v);
			assert!(is_sorted(&v))
		})
		.unwrap()
}

#[test]
fn scope_empty() {
	ThreadPoolBuilder::new()
		.spawn(|| DefaultHandle::scope(|_| {}))
		.unwrap()
}

#[test]
fn scope_result() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let x = DefaultHandle::scope(|_| 22);
			assert_eq!(x, 22);
		})
		.unwrap()
}

#[test]
fn scope_two() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let count = &AtomicUsize::new(0);
			DefaultHandle::scope(|s| {
				s.spawn(move |_| {
					count.fetch_add(1, Ordering::SeqCst);
				});
				s.spawn(move |_| {
					count.fetch_add(2, Ordering::SeqCst);
				});
			});
			let v = count.load(Ordering::SeqCst);
			assert_eq!(v, 3)
		})
		.unwrap()
}

fn divide_and_conquer<'scope>(scope: &Scope<'scope>, counter: &'scope AtomicUsize, size: usize) {
	if size > 1 {
		scope.spawn(move |scope| divide_and_conquer(scope, counter, size / 2));
		scope.spawn(move |scope| divide_and_conquer(scope, counter, size / 2));
	} else {
		counter.fetch_add(1, Ordering::SeqCst);
	}
}

fn divide_and_conquer_seq(counter: &AtomicUsize, size: usize) {
	if size > 1 {
		divide_and_conquer_seq(counter, size / 2);
		divide_and_conquer_seq(counter, size / 2);
	} else {
		counter.fetch_add(1, Ordering::SeqCst);
	}
}

#[test]
fn scope_divide_and_conquer() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let parallel_counter = &AtomicUsize::new(0);
			DefaultHandle::scope(|s| s.spawn(|s| divide_and_conquer(s, parallel_counter, 1024)));

			let sequential_counter = &AtomicUsize::new(0);
			divide_and_conquer_seq(sequential_counter, 1024);

			let parallel = parallel_counter.load(Ordering::SeqCst);
			let sequential = sequential_counter.load(Ordering::SeqCst);
			assert_eq!(sequential, parallel);
		})
		.unwrap()
}

struct Tree<T>
where
	T: Send,
{
	val: T,
	children: Vec<Tree<T>>,
}

impl<T> Tree<T>
where
	T: Send,
{
	fn iter(&self) -> vec::IntoIter<&T> {
		once(&self.val)
			.chain(self.children.iter().flat_map(Tree::iter))
			.collect::<Vec<_>>() // seems like it shouldn't be needed ... but prevents overflow
			.into_iter()
	}

	fn update<OP>(&mut self, op: OP)
	where
		OP: Fn(&mut T) + Sync,
		T: Send,
	{
		DefaultHandle::scope(|s| self.update_in_scope(&op, s));
	}

	fn update_in_scope<'scope, OP>(&'scope mut self, op: &'scope OP, scope: &Scope<'scope>)
	where
		OP: Fn(&mut T) + Sync,
	{
		let Tree {
			ref mut val,
			ref mut children,
		} = *self;
		scope.spawn(move |scope| {
			for child in children {
				scope.spawn(move |scope| child.update_in_scope(op, scope));
			}
		});

		op(val);
	}
}

fn random_tree(depth: usize) -> Tree<u32> {
	assert!(depth > 0);

	let mut seed = <XorShiftRng as SeedableRng>::Seed::default();
	(0..).zip(seed.as_mut()).for_each(|(i, x)| *x = i);
	let mut rng = XorShiftRng::from_seed(seed);
	random_tree1(depth, &mut rng)
}

fn random_tree1(depth: usize, rng: &mut XorShiftRng) -> Tree<u32> {
	let children = if depth == 0 {
		vec![]
	} else {
		(0..rng.gen_range(0..4))
			.map(|_| random_tree1(depth - 1, rng))
			.collect()
	};

	Tree {
		val: rng.gen_range(0..1_000_000),
		children,
	}
}

#[test]
fn update_tree() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			let mut tree = random_tree(10);
			let values = tree.iter().cloned().collect::<Vec<_>>();
			tree.update(|v| *v += 1);
			let new_values = tree.iter().cloned().collect::<Vec<_>>();
			assert_eq!(values.len(), new_values.len());
			for (&i, &j) in values.iter().zip(&new_values) {
				assert_eq!(1 + i, j);
			}
		})
		.unwrap()
}
