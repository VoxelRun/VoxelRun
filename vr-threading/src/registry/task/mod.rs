//! Task spawning capabilities of the threadpool

use vr_core::{sync::Arc, unwind::AbortIfPanic};

use crate::job::HeapJob;

use super::WorkerThread;

impl WorkerThread {
	/// Spawns a task in the threadpool
	pub fn task<F>(&self, f: F)
	where
		F: FnOnce() + Send + 'static,
	{
		// Safety: We know f is static
		let abort_guard = AbortIfPanic {
			target: "ThreadPool",
		};
		self.registry.increment_terminate_count();
		// Safety: We know the registry will outlive this task
		unsafe {
			self.push(
				HeapJob::new({
					let registry = Arc::clone(&self.registry);
					move || {
						registry.catch_unwind(f);
						registry.terminate();
					}
				})
				.into_static_job_ref(),
			)
		};
		core::mem::forget(abort_guard)
	}
}

#[cfg(test)]
mod tests {
	use core::{any::Any, num::NonZeroUsize, time::Duration};
	use std::sync::mpsc;

	use crate::{handle::DefaultHandle, threadpool::ThreadPoolBuilder, ThreadPoolHandle};

	#[test]
	#[cfg_attr(not(panic = "unwind"), ignore)]
	fn panic_forward() {
		let (sender, receiver) = mpsc::channel();
		let panic_handler = move |err: Box<dyn Any + Send>| {
			if let Some(&msg) = err.downcast_ref::<&str>() {
				if msg == "success" {
					sender.send(1).unwrap();
				} else {
					sender.send(2).unwrap();
				}
			} else {
				sender.send(3).unwrap();
			}
		};

		ThreadPoolBuilder::new()
			.panic_handler(panic_handler)
			.spawn(|| DefaultHandle::task(|| panic!("success")))
			.unwrap();

		assert_eq!(1, receiver.recv().unwrap());
	}

	#[test]
	fn panic_nested_spawn() {
		let (sender, receiver) = mpsc::channel();

		let panic_handler = move |e| {
			sender.send(e).unwrap();
		};

		const PANICS: usize = 4;
		ThreadPoolBuilder::new()
			.panic_handler(panic_handler)
			.spawn(|| {
				DefaultHandle::task(|| {
					for _ in 0..PANICS {
						DefaultHandle::task(|| panic!("success"));
					}
				});
			})
			.unwrap();

		for _ in 0..PANICS {
			let error = receiver.recv().unwrap();
			if let Some(&msg) = error.downcast_ref::<&str>() {
				assert_eq!(msg, "success");
			} else {
				panic!("did not receive a string from panic handler")
			}
		}
	}

	#[test]
	fn termination_while_task_running() {
		let (sender0, receiver0) = mpsc::channel();
		let (sender1, receiver1) = mpsc::channel();

		ThreadPoolBuilder::new()
			.spawn(|| {
				sender0.send(1).unwrap();
				DefaultHandle::task(move || {
					std::thread::sleep(Duration::from_millis(400));
					let data = receiver0.recv().unwrap();
					DefaultHandle::task(move || {
						sender1.send(data).unwrap();
					})
				})
			})
			.unwrap();

		let v = receiver1.recv().unwrap();
		assert_eq!(v, 1);
	}

	#[test]
	fn test_order() {
		let vec = {
			let (sender, receiver) = mpsc::channel();
			ThreadPoolBuilder::new()
				.num_threads(NonZeroUsize::new(1).unwrap())
				.spawn(move || {
					for i in 0..10 {
						let sender = sender.clone();
						DefaultHandle::task(move || {
							for j in 0..10 {
								let sender = sender.clone();
								DefaultHandle::task(move || {
									sender.send(i * 10 + j).unwrap();
								})
							}
						})
					}
				})
				.unwrap();
			receiver.iter().collect::<Vec<_>>()
		};

		let expected = (0..100).rev().collect::<Vec<_>>();
		assert_eq!(expected, vec);
	}

	#[test]
	fn spawn_then_join_in_worker() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				let (sender, receiver) = mpsc::channel();
				DefaultHandle::scope(move |_| {
					DefaultHandle::task(move || sender.send(11).unwrap());
				});
				assert_eq!(11, receiver.recv().unwrap());
			})
			.unwrap();
	}

	#[test]
	fn spawn_then_join_outside_worker() {
		ThreadPoolBuilder::new()
			.spawn(|| {
				let (sender, receiver) = mpsc::channel();
				DefaultHandle::task(move || sender.send(11).unwrap());
				assert_eq!(11, receiver.recv().unwrap());
			})
			.unwrap();
	}
}
