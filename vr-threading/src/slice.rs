//! Utilities for dealing with slices in parallel

use core::marker::PhantomData;

use crate::{
	iter::{
		plumbing::{bridge, Producer},
		IndexedParallelIterator, IntoParallelIterator, ParallelIterator,
	},
	ThreadPoolHandle,
};

/// A [`ParallelIterator`] over slices, using immutable references
#[derive(Debug)]
pub struct Iter<'data, T, H>
where
	T: Sync,
	H: ThreadPoolHandle,
{
	/// The underlying slice
	slice: &'data [T],
	/// This [`ParallelIterator`]'s [ThreadPoolHandle]
	_handle: PhantomData<H>,
}

impl<'data, T, H> ParallelIterator<H> for Iter<'data, T, H>
where
	H: ThreadPoolHandle,
	T: Sync + 'data,
{
	type Item = &'data T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.slice.len())
	}
}

impl<'data, T, H> IndexedParallelIterator<H> for Iter<'data, T, H>
where
	H: ThreadPoolHandle,
	T: Sync + 'data,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.slice.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		callback.callback(IterProducer { slice: self.slice })
	}
}

/// A [`Producer`] for the immutable slice [`ParallelIterator`]
struct IterProducer<'data, T>
where
	T: Sync,
{
	/// The underlying splitting slice
	slice: &'data [T],
}

impl<'data, T, H> Producer<H> for IterProducer<'data, T>
where
	H: ThreadPoolHandle,
	T: Sync + 'data,
{
	type Item = &'data T;

	type IntoIter = core::slice::Iter<'data, T>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.slice.split_at(index);
		(IterProducer { slice: left }, IterProducer { slice: right })
	}

	fn into_iter(self) -> Self::IntoIter {
		self.slice.iter()
	}
}

/// A [`ParallelIterator`] over slices, using mutable references
#[derive(Debug)]
pub struct IterMut<'data, T, H>
where
	T: Send,
	H: ThreadPoolHandle,
{
	/// The underlying slice
	slice: &'data mut [T],
	/// The handle to the threadpool.
	_handle: PhantomData<H>,
}

impl<'data, T, H> ParallelIterator<H> for IterMut<'data, T, H>
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	type Item = &'data mut T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.slice.len())
	}
}

impl<'data, T, H> IndexedParallelIterator<H> for IterMut<'data, T, H>
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: crate::iter::plumbing::Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.slice.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: crate::iter::plumbing::ProducerCallback<Self::Item, H>,
	{
		callback.callback(IterMutProducer { slice: self.slice })
	}
}

/// A [`Producer`] for the immutable slice [`ParallelIterator`]
struct IterMutProducer<'data, T> {
	/// The underlying splitting slice
	slice: &'data mut [T],
}

impl<'data, T, H> Producer<H> for IterMutProducer<'data, T>
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	type Item = &'data mut T;

	type IntoIter = core::slice::IterMut<'data, T>;

	fn split_at(self, index: usize) -> (Self, Self) {
		let (left, right) = self.slice.split_at_mut(index);
		(
			IterMutProducer { slice: left },
			IterMutProducer { slice: right },
		)
	}

	fn into_iter(self) -> Self::IntoIter {
		self.slice.iter_mut()
	}
}

impl<'data, T, H> IntoParallelIterator<H> for &'data [T]
where
	T: Sync + 'data,
	H: ThreadPoolHandle,
{
	type Iter = Iter<'data, T, H>;

	type Item = &'data T;

	fn into_par_iter(self) -> Self::Iter {
		Iter {
			slice: self,
			_handle: PhantomData,
		}
	}
}

impl<'data, T, H> IntoParallelIterator<H> for &'data mut [T]
where
	T: Send + 'data,
	H: ThreadPoolHandle,
{
	type Iter = IterMut<'data, T, H>;

	type Item = &'data mut T;

	fn into_par_iter(self) -> Self::Iter {
		IterMut {
			slice: self,
			_handle: PhantomData,
		}
	}
}
