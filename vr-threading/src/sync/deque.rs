//! A LIFO work stealing bounded queue.
//!
//! Operations on this queue are lockfree and due to bounded. The queue itself
//! can be described as a bounded Chache-Lev queue. The Worker pushes element
//! onto the queue and pops them in LIFO order, whereas stealers steal from
//! the head of the queu in FIFO order.

// Based on and adapted from the st3 crates lifo queue:
//
// The MIT License (MIT)
// Copyright (c) 2022 Serge Barral
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use std::{
	alloc::Layout,
	iter::FusedIterator,
	mem::MaybeUninit,
	panic::{RefUnwindSafe, UnwindSafe},
	sync::atomic::Ordering,
};

use vr_core::{
	cell::UnsafeCell,
	debug_or_loom_assert,
	sync::{
		atomic::{AtomicUnsignedLong, AtomicUnsignedShort},
		Arc,
	},
	utils::CachePadded,
	UnsignedLong, UnsignedShort,
};

/// The maximum capacity of a Queue of this type
///
/// This is architecture specific:
/// - On 64 bit architectures this is $ 2^{31} $
/// - On 32 bit architectures this is $ 2^{15} $
pub const MAX_CAPACITY: usize = 1 << (UnsignedShort::BITS - 1);

/// A double-ended LIFO queue
///
/// The queue tracks its tail and head position within a ring buffer with
/// wrap-around integers. All positions have bit widths that are intentionally
/// larger than necessary for buffer indexing because:
/// - an extra bit is needed to disambiguate between empty and full buffers when
/// the start and end position of the buffer are equal
///
#[derive(Debug)]
struct Inner<T> {
	/// Total number of push operations.
	push_count: CachePadded<AtomicUnsignedShort>,

	/// Total number of pop operations, packed together with the position of
	/// the head that a stealer will set once stealing is complete, represented
	/// as a [`PopCountAndHead`]. The head position always coincides with the
	/// [`head`](Inner::head) field below if the last stealing operation has
	/// completed.
	pop_count_and_head: CachePadded<AtomicUnsignedLong>,

	/// The position of the current head, updated after the completion of each
	/// stealing operation
	head: CachePadded<AtomicUnsignedShort>,

	/// Queue items as a ring buffer, the length of this is always a power of
	/// two
	ring_buffer: Box<[UnsafeCell<MaybeUninit<T>>]>,

	/// Mask for the buffer index. (Always a power of two minus 1)
	mask: UnsignedShort,
}

/// Handle to a single-threaded LIFO worker queue end
#[derive(Debug)]
#[repr(transparent)]
pub struct Worker<T> {
	/// The reference to the inner queue structure
	inner: Arc<Inner<T>>,
}

/// Handle to the Stealing end of a [`Worker`], Elements are stolen in FIFO order.
#[derive(Debug)]
#[repr(transparent)]
pub struct Stealer<T> {
	/// The reference to the inner queue structure
	inner: Arc<Inner<T>>,
}

impl<T> UnwindSafe for Worker<T> {}
impl<T> RefUnwindSafe for Worker<T> {}

/// # Safety:
///
/// The worker can be sent as it will not be owned by more than one thread and
/// therefore it will not be possible for a concurrent [`push`](Worker::push)
/// or [`pop`](Worker::pop)
unsafe impl<T: Send> Send for Worker<T> {}

impl<T> UnwindSafe for Stealer<T> {}
impl<T> RefUnwindSafe for Stealer<T> {}

/// # Safety:
///
/// The stealing end can only [`steal`](Stealer::steal) and since that is a
/// thread-safe operation the Stealer may be owned by multiple threads
unsafe impl<T: Sync> Sync for Stealer<T> {}

/// # Safety:
///
/// As the contents of the queue are stored in an [`Arc`] we can
/// send a copy of this to another thread, or transfer ownership
unsafe impl<T: Send> Send for Stealer<T> {}

impl<T> Worker<T> {
	/// Creates a new [`Worker<T>`] handle.
	///
	/// # Note
	///
	/// The capacity of a work stealing queue is **always a power of 2**. So
	/// the `min_capacity` will be round up to the nearest power of two.
	///
	/// # Panics
	///
	/// Panics if `min_capacity` is larger than the [`MAX_CAPACITY`] for this
	/// platform. Which on 32 bit architectures is $ 2^{15} $ and on 64 bit
	/// architectures $ 2^{31} $.
	pub fn new(min_capacity: usize) -> Self {
		assert!(
			min_capacity <= MAX_CAPACITY,
			"The capacity of the queue cannot exceed {MAX_CAPACITY}"
		);

		let capacity = min_capacity.next_power_of_two();
		let buffer = allocate_buffer(capacity);
		let mask = capacity as UnsignedShort - 1;

		Self {
			inner: Arc::new(Inner {
				push_count: AtomicUnsignedShort::new(0).into(),
				pop_count_and_head: AtomicUnsignedLong::new(0).into(),
				head: AtomicUnsignedShort::new(0).into(),
				ring_buffer: buffer,
				mask,
			}),
		}
	}

	/// Returns a [`Stealer`] handle to this [`Worker<T>`].
	///
	/// [`Stealer`]s steal work from the head of the queue in FIFO order
	/// concurrently.
	///
	/// There can be an arbitrary number of [`Stealer`]s, created via this method
	/// or cloning an existing [`Stealer`] handle
	pub fn stealer(&self) -> Stealer<T> {
		Stealer {
			inner: self.inner.clone(),
		}
	}

	/// Creates a reference to a [`Stealer`] handle associated to this [`Worker`]
	///
	/// This is a zero-cost reference-to-reference conversion: the reference count
	/// to the underlying Queue will not be modified. The returned reference
	/// can in particular be used to perform a cheap equality check with
	/// another [`Stealer`] and verify that it is associated to the same
	/// [`Worker`]
	#[allow(unused)]
	pub fn stealer_ref(&self) -> &Stealer<T> {
		// Sanity check, will be opmized away in release
		assert_eq!(Layout::for_value(&self.inner), Layout::new::<Stealer<T>>());

		// Safety: Transmute requirements
		// - We know that both types have the same underlying layout.
		// - The reference to the stealer will not outlive this type
		unsafe { core::mem::transmute::<&'_ Arc<Inner<T>>, &'_ Stealer<T>>(&self.inner) }
	}

	/// Get the total capacity of the queue, disregarding all the elements that
	/// are in it
	#[allow(unused)]
	pub fn capacity(&self) -> usize {
		self.inner.capacity()
	}

	/// Returns the spare capacity of this [`Worker<T>`]. Referring to the
	/// difference of the capacity and occupied length. This may not be accurate
	/// as it may underestemate the capacity due to concurrent stealing operations
	#[allow(unused)]
	pub fn spare_capacity(&self) -> usize {
		let push_count = self.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead { pop_count, .. } =
			self.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let tail = push_count.wrapping_sub(pop_count);

		let head = self.inner.head.load(Ordering::Relaxed);
		let len = tail.wrapping_sub(head);

		self.capacity() - len as usize
	}

	/// Returns whether this [`Worker<T>`] is empty or not.
	pub fn is_empty(&self) -> bool {
		let push_count = self.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead { pop_count, head } =
			self.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let tail = push_count.wrapping_sub(pop_count);

		tail == head
	}

	/// pushes an element to the queue of this [`Worker<T>`] handle.
	///
	/// # Errors
	///
	/// This function will return an error if the queue this [`Worker<T>`] is
	/// full
	pub fn push(&self, item: T) -> Result<(), T> {
		// Ordering: This is the only thread which owns the [`Worker<T>`] so it
		// is the only thread allowed to use this method. the push and pop counts
		// can only be modified by this thread meaning [`Ordering::Relaxed`] is
		// sufficient.
		let push_count = self.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead { pop_count, .. } =
			self.inner.pop_count_and_head.load(Ordering::Relaxed).into();

		// this threads own pop_count and push_count so it owns the tail so it
		// will persist
		let tail = push_count.wrapping_sub(pop_count);

		// Ordering: Acquire ordering is required to synchronise with the Release
		// of the `head` atomic at the end of a stealing operation and ensure the
		// stealer has finished copying the items from the buffer
		let head = self.inner.head.load(Ordering::Acquire);

		if tail.wrapping_sub(head) > self.inner.mask {
			return Err(item);
		}

		// Safety: write_at requirements
		// - This is the only thread that may write at the tail
		// - The tail is not initialized, otherwise the list would be full and
		// this function would not reach this point.
		unsafe { self.inner.write_at(tail, item) };

		// Report the push to the queue
		//
		// Ordering: The Release ordering ensures that the subsequent
		// acquisition of this atomic by a stealer will make the previous write
		// visible
		self.inner
			.push_count
			.store(push_count.wrapping_add(1), Ordering::Release);
		Ok(())
	}

	/// Extends the queue with elements from the iterator iter
	///
	/// The elements of the iterator will be pushed onto the queue in succession,
	/// therefore the next [`pop`](Worker::pop) operation will yield the last
	/// element of the iterator that could be pushed.
	///
	/// # Note
	///
	/// It is the responsibility of the caller to ensure that there is enough
	/// spare capacity to accommodate all iterator items, for instance by
	/// calling [`spare_capacity`](Worker::spare_capacity) beforehand.
	/// Otherwise, the iterator is dropped while still holding the excess items.
	///
	#[allow(unused)]
	pub fn extend<I>(&self, iter: I)
	where
		I: IntoIterator<Item = T>,
	{
		// Ordering: This is the only thread which owns the [`Worker<T>`] so it
		// is the only thread allowed to use this method. the push and pop counts
		// can only be modified by this thread meaning [`Ordering::Relaxed`] is
		// sufficient
		let push_count = self.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead { pop_count, .. } =
			self.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let mut tail = push_count.wrapping_sub(pop_count);

		// Ordering: Acquire ordering is required to synchronise with the Release
		// of the `head` atomic at the end of a stealing operation and ensure the
		// stealer has finished copying the items from the buffer
		let head = self.inner.head.load(Ordering::Acquire);

		let max_tail = head.wrapping_add(self.inner.capacity() as _);
		for item in iter {
			if tail == max_tail {
				break;
			}

			// Safety: write_at requirements
			// - This is the only thread that may write at the tail
			// - The tail is not initialized, otherwise the list would be full and
			// this function would not reach this point.
			unsafe { self.inner.write_at(tail, item) };
			tail = tail.wrapping_add(1);
		}
		// Make the item visible by incrementing the push count
		//
		// Ordering: The Release ordering ensures that the subsequent
		// acquisition of this atomic by a stealer will make the previous write
		// visible
		self.inner
			.push_count
			.store(tail.wrapping_add(pop_count), Ordering::Release);
	}

	/// Pops the last element that was [`push`](Worker::push)ed unto this
	/// [`Worker`]s queue from it.
	///
	/// # Errors
	///
	/// this function will return None if the queue is empty.
	pub fn pop(&self) -> Option<T> {
		// Acquire the item to be popped
		//
		// Ordering: Relaxed ordering is sufficient since
		// 1. the push and pop count are only set by this thread and
		// 2. no stealer will read this slot until it has been again written to
		// with a push operation.
		// In the worst case, the head position read below will be obsolete and
		// the first CAS will fail
		let mut pop_count_and_head: PopCountAndHead =
			self.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let push_count = self.inner.push_count.load(Ordering::Relaxed);

		let tail = push_count.wrapping_sub(pop_count_and_head.pop_count);
		let new_pop_count = pop_count_and_head.pop_count.wrapping_add(1);

		loop {
			if tail == pop_count_and_head.head {
				return None;
			}

			let new_pop_and_head = PopCountAndHead {
				head: pop_count_and_head.head,
				pop_count: new_pop_count,
			};

			// Attempt to claim this slot to pop
			//
			// Ordering:
			// 1. Release is necessary so that stealers can acquire
			// the pop count and be sure that all previous push operations have
			// been accounted for, otherwise the calculated tail could end up
			// less than the head.
			// 2. Relaxed, same reasoning as above, nothing happened
			match self.inner.pop_count_and_head.compare_exchange_weak(
				pop_count_and_head.into(),
				new_pop_and_head.into(),
				Ordering::Release,
				Ordering::Relaxed,
			) {
				Ok(_) => break,
				// lost race against steal
				Err(actual) => {
					pop_count_and_head = actual.into();
				}
			};
		}

		// Read the item
		//
		// Safety: This position will remain valid until the next push operation
		unsafe { Some(self.inner.read_at(tail.wrapping_sub(1))) }
	}

	/// Returns the iterator that steals items from the head of the queue.
	///
	/// The returned iterator steals up to $ N $ items, where $ N $ is the
	/// specified by a closure which takes as argument the total count of
	/// items available for stealing. Upon success, the number of items
	/// ultimately stolen can be from 1 to `N`, depending on the number of
	/// available items.
	///
	/// # Beware
	///
	/// All items stolen by the iterator should be moved out as soon as
	/// possible, because until then or until the iterator is dropped, all
	/// concurrent stealing operations will fail with [`StealError::Busy`] or
	/// [`Steal::Retry`].
	///
	/// # Leaking
	///
	/// If the iterator is leaked before all stolen items have been moved out,
	/// subsequent stealing operations fill fail with [`StealError::Busy`] or
	/// [`Steal::Retry`].
	///
	/// # Errors
	///
	/// This function will return an error if another steal operation is active
	/// or no element is free to steal.
	#[allow(unused)]
	pub fn drain<F>(&self, count_fn: F) -> Result<Drain<T>, StealError>
	where
		F: FnMut(usize) -> usize,
	{
		let Reserved {
			old_head, new_head, ..
		} = self.inner.reserve_items(count_fn, UnsignedShort::MAX)?;

		Ok(Drain {
			inner: &self.inner,
			current: old_head,
			end: new_head,
		})
	}
}

/// A draining iterator for [`Worker<T>`]
///
/// This iterator is created by [`Worker<T>::drain`]. See its documentation for
/// more information
#[derive(Debug)]
pub struct Drain<'a, T> {
	/// The queue this iterator references
	inner: &'a Arc<Inner<T>>,
	/// The element the iterator is currently pointing to
	current: UnsignedShort,
	/// The element the iterator needs to reach until empty
	end: UnsignedShort,
}

impl<'a, T> UnwindSafe for Drain<'a, T> {}
impl<'a, T> RefUnwindSafe for Drain<'a, T> {}
/// Safety: This is safe to be sent between threads as long as it outlives
/// the [`Worker<T>`] from which is being stolen.
unsafe impl<'a, T: Send> Send for Drain<'a, T> {}

impl<T> Iterator for Drain<'_, T> {
	type Item = T;

	fn next(&mut self) -> Option<Self::Item> {
		if self.current == self.end {
			return None;
		}

		// Safety: this thread is guaranteed to be the only thread stealing and
		// has the elements to be stolen reserved.
		let item = Some(unsafe { self.inner.read_at(self.current) });
		self.current = self.current.wrapping_add(1);

		// Update the head position
		if self.current == self.end {
			// Ordering: the Release ordering ensures that all items have been
			// moved out when a subsequent push operation synchronises by
			// acquiring `head`. It also ensures that the push count seen by a
			// subsequent steal operation (which acquires head) is at least
			// equal to the one seen by the present steal operation.
			self.inner.head.store(self.end, Ordering::Release);
		}
		item
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		let sz = self.end.wrapping_sub(self.current) as usize;
		(sz, Some(sz))
	}
}

impl<T> ExactSizeIterator for Drain<'_, T> {}
impl<T> FusedIterator for Drain<'_, T> {}

impl<T> Drop for Drain<'_, T> {
	fn drop(&mut self) {
		for _it in self {}
	}
}

impl<T> Stealer<T> {
	/// Attempt to steal an element from the head of the queue.
	///
	/// On success a [`Steal::Success`] containing the stolen element is
	/// returned.
	///
	/// # Errors
	///
	/// This function will return the following erros in the following cases:
	/// 1. [`Steal::Empty`] in the case the queue from which the element that
	/// should be stolen comes from is empty.
	/// 2. [`Steal::Retry`] if a steal operation is currently ongoing.
	pub fn steal(&self) -> Steal<T> {
		let reserved = match self.inner.reserve_items(|_| 1, 1) {
			Ok(r) => r,
			Err(e) => return e.into(),
		};

		// Safety: we guaranteed that we are the only one with access to the
		// element at the head we we declared we were going to steal.
		let item = unsafe { self.inner.read_at(reserved.old_head) };

		// Update the head position
		//
		// Ordering: The Release ordering ensures that all items have been moved
		// out when a subsequent push operation is synchronised by acquiring
		// `head`. It also ensures that the push count seen by a subsequent
		// steal operation (which acquires `head`) is at least equal to the
		// one by the present stealing operation. Also, officially ends this
		// transaction
		self.inner.head.store(reserved.new_head, Ordering::Release);
		Steal::Success(item)
	}

	/// Attempts to steal items from the head of the queue and move them into
	/// tail of another queue.
	///
	/// Up to $ N $ items are moved to the destination queue, where $ N $ is
	/// specified by a closure which takes as argument the total count of items
	/// available for stealing. Upon success, the number of items ultimately
	/// transferred to the destination queue can be from 1 to $ N $, depending
	/// on the number of available items and the capacity of the destination
	/// queue; the count of transferred items is returned as the success payload
	///
	/// # Errors
	///
	/// This function will return an error in the following cases:
	/// 1. No item was stolen, either because the queue is empty or the
	/// destination is full or $ N $ is equal to $ 0 $ or
	/// 2.  A concurrent stealing operation is ongoing.
	#[allow(unused)]
	pub fn steal_batch<F>(&self, dest: &Worker<T>, count_fn: F) -> Result<usize, StealError>
	where
		F: FnMut(usize) -> usize,
	{
		// Compute the free capacity of the destination queue.
		//
		// Note that even if the value of `dest_head` is stale, the subtraction
		// that computes `dest_free_capacity` will never overflow since it is
		// computed on the same thread that pushed items to the destination
		// queue, but `push` would fail if `dest_head` suggested that there is
		// no spare capacity.
		//
		// Ordering: see [`Worker::push`]
		let dest_push_count = dest.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead {
			pop_count: dest_pop_count,
			..
		} = dest.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let dest_tail = dest_push_count.wrapping_sub(dest_pop_count);
		let dest_head = dest.inner.head.load(Ordering::Acquire);
		let dest_free_capacity =
			dest.inner.capacity() as UnsignedShort - dest_tail.wrapping_sub(dest_head);

		debug_or_loom_assert!(dest_free_capacity as usize <= dest.inner.capacity());

		let reserved = self.inner.reserve_items(count_fn, dest_free_capacity)?;

		debug_or_loom_assert!(reserved.transfer_count as usize <= dest_free_capacity as usize);

		for offset in 0..reserved.transfer_count {
			// Safety: we guaranteed that we are the only one with access to the
			// elements at the head we we declared we were going to steal.
			unsafe {
				let item = self.inner.read_at(reserved.old_head.wrapping_add(offset));
				dest.inner.write_at(dest_tail.wrapping_add(offset), item);
			}
		}

		// Make the moved items visible by updating the destination tail position.
		//
		// Ordering: see comments in the `push()` method.
		dest.inner.push_count.store(
			dest_push_count.wrapping_add(reserved.transfer_count),
			Ordering::Release,
		);

		// Update the head position
		//
		// Ordering: The Release ordering ensures that all items have been moved
		// out when a subsequent push operation is synchronised by acquiring
		// `head`. It also ensures that the push count seen by a subsequent
		// steal operation (which acquires `head`) is at least equal to the
		// one by the present stealing operation. Also, officially ends this
		// transaction
		self.inner.head.store(reserved.new_head, Ordering::Release);

		Ok(reserved.transfer_count as usize)
	}

	/// Attempts to steal items from the head of the queue, returning oen of
	/// them directly and moving the others to the tail of another queue.
	///
	/// Up to $ N $ elements are stolen  (inclusive to the one returned
	/// directly), where $ N $ is specified by a closure which takes in the
	/// total count of items available for stealing. Upon success, one item
	/// is returned and from $ 0 $ to $ N - 1 $ elements are moved to the
	/// destination queue. depending on the number of available items and
	/// the capacity of the destination queue; the number of transferred items
	/// is returned as the second field of the success value.
	///
	/// The returned item is the most recent among the stolen items.
	///
	/// # Errors
	///
	/// This function will return an error in one of the following cases:
	/// 1. No item was stolen, either because the queue is empty or $ N $ is
	/// $ 0 $, or
	/// 2. a concurrent stealing operation is ongoing
	///
	/// Failure to transfer any item to the destination queue is not considered
	/// an error as long as one element could be returned directly. This can
	/// occure if the destination queue is full, if the source queue has only
	/// one item or if $ N $ is 1.
	#[allow(unused)]
	pub fn steal_batch_and_pop<F>(
		&self,
		dest: &Worker<T>,
		count_fn: F,
	) -> Result<(T, usize), StealError>
	where
		F: FnMut(usize) -> usize,
	{
		// Compute the free capacity of the destination queue.
		//
		// Ordering: See `Worker::push`
		let dest_push_count = dest.inner.push_count.load(Ordering::Relaxed);
		let PopCountAndHead {
			pop_count: dest_pop_count,
			..
		} = dest.inner.pop_count_and_head.load(Ordering::Relaxed).into();
		let dest_tail = dest_push_count.wrapping_sub(dest_pop_count);
		let dest_head = dest.inner.head.load(Ordering::Acquire);
		let dest_free_capacity = dest.inner.capacity() - dest_tail.wrapping_sub(dest_head) as usize;

		debug_or_loom_assert!(dest_free_capacity <= dest.inner.capacity());
		let reserved = self
			.inner
			.reserve_items(count_fn, dest_free_capacity as UnsignedShort + 1)?;
		// we copy the last one directly
		let transfer_count = reserved.transfer_count - 1;
		debug_or_loom_assert!(transfer_count as usize <= dest_free_capacity);

		for offset in 0..transfer_count {
			// Safety: we guaranteed that we are the only one with access to the
			// elements at the head we we declared we were going to steal.
			unsafe {
				let item = self.inner.read_at(reserved.old_head.wrapping_add(offset));
				dest.inner.write_at(dest_tail.wrapping_add(offset), item);
			}
		}

		// Read the last item
		//
		// Safety: we guaranteed that we are the only one with access to the
		// elements at the head we we declared we were going to steal.
		let last_item = unsafe {
			self.inner
				.read_at(reserved.old_head.wrapping_add(transfer_count))
		};

		// Update the head position.
		//
		// Ordering: The Release ordering ensures that all items have been moved
		// out when a subsequent push operation is synchronised by acquiring
		// `head`. It also ensures that the push count seen by a subsequent
		// steal operation (which acquires `head`) is at least equal to the
		// one by the present stealing operation. Also, officially ends this
		// transaction
		dest.inner.push_count.store(
			dest_push_count.wrapping_add(transfer_count),
			Ordering::Release,
		);
		self.inner.head.store(reserved.new_head, Ordering::Release);

		Ok((last_item, transfer_count as _))
	}
}

impl<T> Clone for Stealer<T> {
	fn clone(&self) -> Self {
		Self {
			inner: self.inner.clone(),
		}
	}
}

impl<T> PartialEq for Stealer<T> {
	fn eq(&self, other: &Self) -> bool {
		Arc::ptr_eq(&self.inner, &other.inner)
	}
}

impl<T> Eq for Stealer<T> {}

impl<T> Inner<T> {
	/// Capacity of the queue
	fn capacity(&self) -> usize {
		self.mask.wrapping_add(1) as _
	}

	/// Read an item at the given position
	///
	/// The position is automatically mapped to an index using using a modulo
	/// operation with the current mast + 1.
	///
	/// # Safety
	///
	/// The item at the given position must have been initialized before and
	/// cannot have been moved out.
	///
	/// The caller must guarantee that no one else is writing or moving this
	/// item concurrently.
	unsafe fn read_at(&self, position: UnsignedShort) -> T {
		let index = (position & self.mask) as usize;
		// Safety: The caller is responsible for the safety requirements of
		// reading a possible uninitialized item, and being the only one
		// allowed to access it
		unsafe { (*self.ring_buffer).as_ref()[index].with(|slot| slot.read().assume_init()) }
	}

	/// Write an item at the given position
	///
	/// The position is automatically mapped to an index using using a modulo
	/// operation with the current mast + 1.
	///
	/// # Note
	///
	/// If an valid item is at the position it will be leaked by this operation
	///
	/// # Safety
	///
	/// The caller must guarantee that the item at this position cannot be
	/// read or writen concurrently.
	unsafe fn write_at(&self, position: UnsignedShort, item: T) {
		let index = (position & self.mask) as usize;
		// Safety: The caller is responsible for the safety requirements of
		// reading a possible uninitialized item, and being the only one
		// allowed to access it
		unsafe {
			(*self.ring_buffer).as_ref()[index].with_mut(|slot| slot.write(MaybeUninit::new(item)))
		}
	}

	/// Attempts to book $ N $ items for stealing where $ N $ is specified by
	/// a closure which takes the element count as an argument.
	///
	/// In case of success, the returned triplet is the *current* head, the
	/// *next* head and an item count at least equal to 1.
	///
	/// # Errors
	///
	/// This function will return an error in the following cases:
	/// 1. No item could be stolen, either because the queue is empty or because
	/// $ N $ is $ 0 $
	/// 2. A concurrent stealing operation is ongoing
	///
	/// # Safety
	///
	/// This function is not strictly unsafe, but because it initialises the
	/// stealing operation by modifying the post-stealing head in
	/// `push_count_and_head` without ever updating the `head` atomic variable,
	/// its misuse can result in permanently blocking subsequent stealing
	/// operations.
	fn reserve_items<F>(
		&self,
		mut count_fn: F,
		max_count: UnsignedShort,
	) -> Result<Reserved, StealError>
	where
		F: FnMut(usize) -> usize,
	{
		// Ordering: Acquire on the `pop_count_and_head` load synchronises with
		// the release at the end of a previous pop operation. It is therefore
		// warranted that the push count loaded later is at least the same as it
		// was when pop count was set, ensuring in turn that the computed tail
		// is not less than the head and therefore the item count does not wrap
		// around. For the same reason, the failure ordering on the CAS is also
		// Acquire since the push count is loaded at every CAS
		let mut pop_count_and_head: PopCountAndHead =
			self.pop_count_and_head.load(Ordering::Acquire).into();

		// Ordering: Acquire on the `head` load synchronises with the a release
		// at the end of a previous steal operation. Once this head is confirmed
		// to equal the head in `pop_count_and_head`, it is therefore warranted
		// that the push count loaded later is at least the same as it was on
		// the last completed steal operation, ensuring in turn that the item
		// count does not wrap around. Alternatively, the ordering could be
		// Relaxed if the success ordering on the CAS was AcqRel, which would
		// achieve the same by synchronising with the head field of `pop_count_and_head`
		let old_head = self.head.load(Ordering::Acquire);

		loop {
			// another steal operation is ongoing
			if old_head != pop_count_and_head.head {
				return Err(StealError::Busy);
			}

			// Ordering: Acquire synchronises with the Release in the push
			// method and ensure that all items pushed to the queue are visible
			let push_count = self.push_count.load(Ordering::Acquire);
			let tail = push_count.wrapping_sub(pop_count_and_head.pop_count);

			// Note: it is possible for the computed item_count to be spuriously
			// greater than the number of available items if, in this iteration
			// of the CAS loop, `pop_count_and_head` and `head` are both
			// obsolete. This is not an issue, however, since the CAS will then
			// fail due to `pop_count_and_head` being obsolete.
			let item_count = tail.wrapping_sub(pop_count_and_head.head);

			// `item_count` is tested now because `count_fn` may expect
			// `item_count>0`.
			if item_count == 0 {
				return Err(StealError::Empty);
			}

			// UnwindSafety: it is OK if `count_fn` panics as no state has yet
			// been modified, meanin on unwind, there won't be invalid state.
			let wanting_to_steal =
				count_fn(item_count as usize).min(max_count as usize) as UnsignedShort;
			let count = wanting_to_steal.min(item_count);

			// The special case `count_fn() == 0` must be tested specifically,
			// because if the compare-exchange succeeds with `count=0`, the new
			// value will be the same as the old one so other stealers will not
			// detect that stealing is currently ongoing and may try to actually
			// steal items and concurrently modify the position of the head.
			if count == 0 {
				return Err(StealError::Empty);
			}

			let new_pop_and_head = PopCountAndHead {
				head: pop_count_and_head.head.wrapping_add(count),
				pop_count: pop_count_and_head.pop_count,
			};

			// Attempt to book the slots. Only one stealer can succeed since
			// once this atomic is changed, the other thread will necessarily
			// observe a mismatch between `head` and the head sub-field of
			// `pop_count_and_head`.
			//
			// Ordering: see justification for Acquire on failure in the first
			// load of `pop_count_and_head`. No further synchronization is
			// necessary on success.
			match self.pop_count_and_head.compare_exchange_weak(
				pop_count_and_head.into(),
				new_pop_and_head.into(),
				Ordering::Acquire,
				Ordering::Acquire,
			) {
				Ok(_) => {
					break Ok(Reserved {
						old_head: pop_count_and_head.head,
						new_head: new_pop_and_head.head,
						transfer_count: count,
					})
				}
				Err(actual) => {
					pop_count_and_head = actual.into();
				}
			}
		}
	}
}

impl<T> Drop for Inner<T> {
	fn drop(&mut self) {
		let head = self.head.load(Ordering::Relaxed);
		let push_count = self.push_count.load(Ordering::Relaxed);
		let PopCountAndHead { pop_count, .. } =
			self.pop_count_and_head.load(Ordering::Relaxed).into();
		let tail = push_count.wrapping_sub(pop_count);

		let count = tail.wrapping_sub(head);
		for offset in 0..count {
			// Safety: We are the last handle to this `Inner` so it is safe to
			// assume we are the only one accessing elements.
			drop(unsafe { self.read_at(head.wrapping_add(offset)) })
		}
	}
}

/// An enumeration representing the result of [`Stealer<T>::steal`].
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Steal<T> {
	/// The queue was empty and therefore nothing could be stolen
	Empty,
	/// Another stealing operation was ongoing at the point of stealing, therefore
	/// one has to try again.
	Retry,
	/// The stealing operation was successful and one can take the value out of
	/// this
	Success(T),
}

impl<T> From<StealError> for Steal<T> {
	fn from(value: StealError) -> Self {
		match value {
			StealError::Empty => Self::Empty,
			StealError::Busy => Self::Retry,
		}
	}
}

impl<T> From<Steal<T>> for Option<T> {
	fn from(value: Steal<T>) -> Self {
		match value {
			Steal::Success(value) => Some(value),
			_ => None,
		}
	}
}

impl<T> Steal<T> {
	/// Returns whether the queue was empty on [`steal`](Stealer::steal).
	pub fn is_empty(&self) -> bool {
		matches!(self, Steal::Empty)
	}

	/// Returns whether the [`steal`](Stealer::steal) operation was successful
	/// or not.
	pub fn is_success(&self) -> bool {
		matches!(self, Steal::Success(_))
	}

	/// Returns whether the [`steal`](Stealer::steal) operation needs to be
	/// retried if it was interruped.
	pub fn is_retry(&self) -> bool {
		matches!(self, Steal::Retry)
	}

	/// Converts this type to an [`Option<T>`].
	pub fn success(self) -> Option<T> {
		self.into()
	}
	/// If no task was stolen, attempts another operation
	///
	/// Returns this steal result if it is [`Success`](Steal::Success).
	/// Otherwise, closure `f` is invoked and then:
	///
	/// - If the second steal resulted in [`Success`](Steal::Success), it is
	/// returned.
	/// - If both steals were unsuccessful but any resulted in [`Retry`](Steal::Retry),
	/// then [`Retry`](Steal::Retry) is returned.
	/// - If both resulted in [`Empty`](Steal::Empty), [`Empty`](Steal::Empty)
	/// is returned
	pub fn or_else<F>(self, f: F) -> Steal<T>
	where
		F: FnOnce() -> Steal<T>,
	{
		match self {
			Steal::Empty => f(),
			Steal::Success(_) => self,
			Steal::Retry => {
				if let Steal::Success(res) = f() {
					Steal::Success(res)
				} else {
					Steal::Retry
				}
			}
		}
	}
}
/// Allocates a new buffer for queues with `capacity` size.
fn allocate_buffer<T>(capacity: usize) -> Box<[UnsafeCell<MaybeUninit<T>>]> {
	let mut buffer = Vec::with_capacity(capacity);
	buffer.resize_with(capacity, || UnsafeCell::new(MaybeUninit::uninit()));
	buffer.into_boxed_slice()
}

/// A type labelling the return value from [`Inner::reserve_items`].
struct Reserved {
	/// The position of the old head of the queue, still active on
	/// [`Inner::head`]
	old_head: UnsignedShort,
	/// The position of the head after the following steal operation
	new_head: UnsignedShort,
	/// The number of elements that this steal operation encompasses
	transfer_count: UnsignedShort,
}

/// A type indicating the failures of a batch steal operation like
/// [`Stealer::steal_batch`].
#[derive(Debug, thiserror::Error, PartialEq, Eq)]
pub enum StealError {
	/// The queue was empty and therefore nothing could be stolen
	#[error("Tried to steal from an empty queue.")]
	Empty,
	/// Another stealing operation was ongoing at the point of stealing, therefore
	/// one has to try again.
	#[error("Tried to steal while another thief was stealing.")]
	Busy,
}

/// A type representing the layout of [`Inner::pop_count_and_head`].
#[repr(C, packed)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct PopCountAndHead {
	/// The first half of the number representing the number of element popped
	/// from the queue.
	pop_count: UnsignedShort,
	/// The second half of the number representing the new_head after a steal
	/// operation
	head: UnsignedShort,
}

impl From<UnsignedLong> for PopCountAndHead {
	fn from(value: UnsignedLong) -> Self {
		// Safety: We know what layout UnsignedLong has and all states
		// which UnsignedLong may have are valid states here.
		unsafe { core::mem::transmute::<UnsignedLong, Self>(value) }
	}
}

impl From<PopCountAndHead> for UnsignedLong {
	fn from(value: PopCountAndHead) -> Self {
		// Safety: We know what layout UnsignedLong has and all states
		// which PopCountAndHead may have are valid states here.
		unsafe { core::mem::transmute::<PopCountAndHead, Self>(value) }
	}
}

#[cfg(all(test, not(loom)))]
mod tests_general {
	use super::*;
	use vr_core::sync::atomic::*;
	// Note: test values are boxed in Miri tests so that destructors called on freed
	// values and forgotten destructors can be detected.

	#[cfg(miri)]
	type TestValue<T> = Box<T>;

	#[cfg(not(miri))]
	#[derive(Debug, Default, PartialEq)]
	struct TestValue<T>(T);

	#[cfg(not(miri))]
	impl<T> TestValue<T> {
		fn new(val: T) -> Self {
			Self(val)
		}
	}

	#[cfg(not(miri))]
	impl<T> std::ops::Deref for TestValue<T> {
		type Target = T;

		fn deref(&self) -> &T {
			&self.0
		}
	}

	/// Rotates the internal ring buffer indices by `n`
	fn rotate<T: Default + core::fmt::Debug>(worker: &Worker<T>, n: usize) {
		let stealer = worker.stealer();
		let dummy_worker = Worker::<T>::new(2);
		for _ in 0..n {
			worker.push(T::default()).unwrap();
			stealer.steal_batch_and_pop(&dummy_worker, |_| 1).unwrap();
		}
	}

	#[test]
	fn stealer_equality() {
		let worker_a = Worker::<TestValue<u32>>::new(32);
		let worker_b = Worker::<TestValue<u32>>::new(32);

		assert_eq!(worker_a.stealer(), worker_a.stealer());
		assert_ne!(worker_b.stealer(), worker_a.stealer());
		assert_eq!(worker_b.stealer(), worker_b.stealer());
		assert_ne!(worker_a.stealer(), worker_b.stealer());
	}

	#[test]
	fn stealer_ref_equality() {
		let worker_a = Worker::<TestValue<u32>>::new(32);
		let worker_b = Worker::<TestValue<u32>>::new(32);

		assert_eq!(worker_a.stealer_ref(), &worker_a.stealer());
		assert_ne!(worker_b.stealer_ref(), &worker_a.stealer());
		assert_eq!(worker_b.stealer_ref(), &worker_b.stealer());
		assert_ne!(worker_a.stealer_ref(), &worker_b.stealer());
	}

	#[test]
	fn single_threaded_steal() {
		const ROTATIONS: &[usize] = if cfg!(miri) {
			&[42]
		} else {
			&[0, 255, 256, 257, 65535, 65536, 65537]
		};

		for &rotation in ROTATIONS {
			let worker1 = Worker::new(128);
			let worker2 = Worker::new(128);
			let stealer1 = worker1.stealer();
			rotate(&worker1, rotation);
			rotate(&worker2, rotation);

			worker1.push(TestValue::new(1)).unwrap();
			worker1.push(TestValue::new(2)).unwrap();
			worker1.push(TestValue::new(3)).unwrap();
			worker1.push(TestValue::new(4)).unwrap();

			assert_eq!(worker1.pop().map(|e| *e), Some(4));
			assert_eq!(
				stealer1
					.steal_batch_and_pop(&worker2, |_| 2)
					.map(|(e, s)| (*e, s)),
				Ok((2, 1))
			);
			assert_eq!(worker1.pop().map(|e| *e), Some(3));
			assert_eq!(worker1.pop(), None);
			assert_eq!(worker2.pop().map(|e| *e), Some(1));
			assert_eq!(worker2.pop(), None);
		}
	}

	#[test]
	fn drain_steal() {
		const ROTATIONS: &[usize] = if cfg!(miri) {
			&[42]
		} else {
			&[0, 255, 256, 257, 65535, 65536, 65537]
		};

		for &rotation in ROTATIONS {
			let worker = Worker::new(128);
			let dummy_worker = Worker::new(128);
			let stealer = worker.stealer();
			rotate(&worker, rotation);

			worker.push(TestValue::new(1)).unwrap();
			worker.push(TestValue::new(2)).unwrap();
			worker.push(TestValue::new(3)).unwrap();
			worker.push(TestValue::new(4)).unwrap();

			assert_eq!(worker.pop().map(|e| *e), Some(4));
			let mut iter = worker.drain(|n| n - 1).unwrap();
			assert_eq!(
				stealer.steal_batch_and_pop(&dummy_worker, |_| 1),
				Err(StealError::Busy)
			);
			assert_eq!(iter.next().map(|e| *e), Some(1));
			assert_eq!(
				stealer.steal_batch_and_pop(&dummy_worker, |_| 1),
				Err(StealError::Busy)
			);
			assert_eq!(iter.next().map(|e| *e), Some(2));
			assert_eq!(
				stealer
					.steal_batch_and_pop(&dummy_worker, |_| 1)
					.map(|(e, s)| (*e, s)),
				Ok((3, 0))
			);
			assert_eq!(iter.next(), None);
		}
	}

	#[test]
	fn extend_basic() {
		const ROTATIONS: &[usize] = if cfg!(miri) {
			&[42]
		} else {
			&[0, 255, 256, 257, 65535, 65536, 65537]
		};

		for &rotation in ROTATIONS {
			let worker = Worker::new(128);
			rotate(&worker, rotation);

			let initial_capacity = worker.spare_capacity();
			worker.push(TestValue::new(1)).unwrap();
			worker.push(TestValue::new(2)).unwrap();
			worker.extend([TestValue::new(3), TestValue::new(4)]);

			assert_eq!(worker.spare_capacity(), initial_capacity - 4);
			assert_eq!(worker.pop().map(|e| *e), Some(4));
			assert_eq!(worker.pop().map(|e| *e), Some(3));
			assert_eq!(worker.pop().map(|e| *e), Some(2));
			assert_eq!(worker.pop().map(|e| *e), Some(1));
			assert_eq!(worker.pop(), None);
		}
	}

	#[test]
	fn extend_overflow() {
		const ROTATIONS: &[usize] = if cfg!(miri) {
			&[42]
		} else {
			&[0, 255, 256, 257, 65535, 65536, 65537]
		};

		for &rotation in ROTATIONS {
			let worker = Worker::new(128);
			rotate(&worker, rotation);

			let initial_capacity = worker.spare_capacity();
			worker.push(1).unwrap();
			worker.push(2).unwrap();
			worker.extend(3..); // try to append infinitely many integers

			assert_eq!(worker.spare_capacity(), 0);
			for i in (1..=initial_capacity).rev() {
				assert_eq!(worker.pop(), Some(i));
			}
			assert_eq!(worker.pop(), None);
		}
	}

	#[test]
	fn queue_capacity() {
		for (min_capacity, expected_capacity) in [(0, 1), (1, 1), (3, 4), (8, 8), (100, 128)] {
			let worker = Worker::new(min_capacity);

			assert_eq!(worker.capacity(), expected_capacity);
			assert_eq!(worker.spare_capacity(), expected_capacity);
			for _ in 0..expected_capacity {
				assert!(worker.push(TestValue::new(42)).is_ok())
			}
			assert!(worker.push(TestValue::new(42)).is_err());
			assert_eq!(worker.spare_capacity(), 0);
		}
	}

	#[test]
	fn steal_multi_threaded() {
		const N: usize = if cfg!(miri) { 200 } else { 10_000_000 };

		let counter = Arc::new(AtomicUsize::new(0));
		let worker = Worker::new(128);
		let stealer = worker.stealer();

		let counter0 = counter.clone();
		let stealer1 = stealer.clone();
		let counter1 = counter.clone();
		let stealer2 = stealer;
		let counter2 = counter;

		// lifo::Worker thread.
		//
		// Push all numbers from 0 to N, popping one from time to time.
		let t0 = std::thread::spawn(move || {
			let mut i = 0;
			let mut rng = oorandom::Rand32::new(0);
			let mut stats = vec![0; N];
			'outer: loop {
				for _ in 0..rng.rand_range(1..10) {
					while worker.push(TestValue::new(i)).is_err() {}
					i += 1;
					if i == N {
						break 'outer;
					}
				}
				if let Some(j) = worker.pop() {
					stats[*j] += 1;
					counter0.fetch_add(1, Ordering::Relaxed);
				}
			}

			stats
		});

		// Stealer threads.
		//
		// Repeatedly steal a random number of items.
		fn steal_periodically(
			stealer: Stealer<TestValue<usize>>,
			counter: Arc<AtomicUsize>,
			rng_seed: u64,
		) -> Vec<usize> {
			let mut stats = vec![0; N];
			let mut rng = oorandom::Rand32::new(rng_seed);
			let dest_worker = Worker::new(128);

			loop {
				if let Ok((i, _)) = stealer.steal_batch_and_pop(&dest_worker, |m| {
					rng.rand_range(0..(m + 1) as u32) as usize
				}) {
					stats[*i] += 1; // the popped item
					counter.fetch_add(1, Ordering::Relaxed);
					while let Some(j) = dest_worker.pop() {
						stats[*j] += 1;
						counter.fetch_add(1, Ordering::Relaxed);
					}
				}
				let count = counter.load(Ordering::Relaxed);
				if count == N {
					break;
				}
				assert!(count < N);
			}

			stats
		}

		let t1 = std::thread::spawn(move || steal_periodically(stealer1, counter1, 1));
		let t2 = std::thread::spawn(move || steal_periodically(stealer2, counter2, 2));
		let mut stats = Vec::new();
		stats.push(t0.join().unwrap());
		stats.push(t1.join().unwrap());
		stats.push(t2.join().unwrap());
		for i in 0..N {
			let mut count = 0;
			(0..stats.len()).for_each(|j| {
				count += stats[j][i];
			});
			assert_eq!(count, 1);
		}
	}
}

/// Be warned, this test suite may take days to complete
#[cfg(all(test, loom))]
mod tests_loom {
	use vr_core::thread;

	use super::*;

	#[test]
	fn basic_steal() {
		const LOOP_COUNT: usize = 2;
		const ITEM_COUNT_PER_LOOP: usize = 3;

		loom::model(|| {
			let worker = Worker::<usize>::new(4);
			let stealer = worker.stealer();

			let th = thread::spawn(move || {
				let dest_worker = Worker::<usize>::new(4);
				let mut n = 0;

				for _ in 0..3 {
					let _ = stealer.steal_batch(&dest_worker, |n| n - n / 2);
					while dest_worker.pop().is_some() {
						n += 1;
					}
				}

				n
			});

			let mut n = 0;

			for _ in 0..LOOP_COUNT {
				for _ in 0..(ITEM_COUNT_PER_LOOP - 1) {
					if worker.push(42).is_err() {
						n += 1;
					}
				}

				if worker.pop().is_some() {
					n += 1;
				}

				// Push another task
				if worker.push(42).is_err() {
					n += 1;
				}

				while worker.pop().is_some() {
					n += 1;
				}
			}

			n += th.join().unwrap();

			assert_eq!(ITEM_COUNT_PER_LOOP * LOOP_COUNT, n);
		});
	}

	#[test]
	fn drain_overflow() {
		const ITEM_COUNT: usize = 7;

		loom::model(|| {
			let worker = Worker::<usize>::new(4);
			let stealer = worker.stealer();

			let th = thread::spawn(move || {
				let dest_worker = Worker::<usize>::new(4);
				let mut n = 0;

				let _ = stealer.steal_batch(&dest_worker, |n| n - n / 2);
				while dest_worker.pop().is_some() {
					n += 1;
				}

				n
			});

			let mut n = 0;

			// Push an item, pop an item.
			worker.push(42).unwrap();

			if worker.pop().is_some() {
				n += 1;
			}

			for _ in 0..(ITEM_COUNT - 1) {
				if worker.push(42).is_err() {
					// Spin until some of the old items can be drained to make room
					// for the new item.
					loop {
						if let Ok(drain) = worker.drain(|n| n - n / 2) {
							for _ in drain {
								n += 1;
							}
							assert_eq!(worker.push(42), Ok(()));
							break;
						}
						thread::yield_now();
					}
				}
			}

			n += th.join().unwrap();

			while worker.pop().is_some() {
				n += 1;
			}

			assert_eq!(ITEM_COUNT, n);
		});
	}

	#[test]
	fn multithreaded_stealer() {
		const ITEM_COUNT: usize = 5;

		fn steal_half(stealer: Stealer<usize>) -> usize {
			let dest_worker = Worker::<usize>::new(4);

			let _ = stealer.steal_batch(&dest_worker, |n| n - n / 2);

			let mut n = 0;
			while dest_worker.pop().is_some() {
				n += 1;
			}

			n
		}

		loom::model(|| {
			let worker = Worker::<usize>::new(4);
			let stealer1 = worker.stealer();
			let stealer2 = worker.stealer();

			let th1 = thread::spawn(move || steal_half(stealer1));
			let th2 = thread::spawn(move || steal_half(stealer2));

			let mut n = 0;
			for _ in 0..ITEM_COUNT {
				if worker.push(42).is_err() {
					n += 1;
				}
			}

			while worker.pop().is_some() {
				n += 1;
			}

			n += th1.join().unwrap();
			n += th2.join().unwrap();

			assert_eq!(ITEM_COUNT, n);
		});
	}

	#[test]
	fn chained_steal() {
		loom::model(|| {
			let w1 = Worker::<usize>::new(4);
			let w2 = Worker::<usize>::new(4);
			let s1 = w1.stealer();
			let s2 = w2.stealer();

			for _ in 0..4 {
				w1.push(42).unwrap();
				w2.push(42).unwrap();
			}

			let th = thread::spawn(move || {
				let dest_worker = Worker::<usize>::new(4);
				let _ = s1.steal_batch(&dest_worker, |n| n - n / 2);

				while dest_worker.pop().is_some() {}
			});

			while w1.pop().is_some() {}

			let _ = s2.steal_batch(&w1, |n| n - n / 2);

			th.join().unwrap();

			while w1.pop().is_some() {}
			while w2.pop().is_some() {}
		});
	}

	#[test]
	fn push_and_steal() {
		fn steal_half(stealer: Stealer<usize>) -> usize {
			let dest_worker = Worker::<usize>::new(4);

			stealer
				.steal_batch(&dest_worker, |n| n - n / 2)
				.unwrap_or(0)
		}

		loom::model(|| {
			let worker = Worker::<usize>::new(4);
			let stealer1 = worker.stealer();
			let stealer2 = worker.stealer();

			let th1 = thread::spawn(move || steal_half(stealer1));
			let th2 = thread::spawn(move || steal_half(stealer2));

			worker.push(42).unwrap();
			worker.push(42).unwrap();

			let mut n = 0;
			while worker.pop().is_some() {
				n += 1;
			}

			n += th1.join().unwrap();
			n += th2.join().unwrap();

			assert_eq!(n, 2);
		});
	}

	#[test]
	fn extend() {
		fn steal_half(stealer: Stealer<usize>) -> usize {
			let dest_worker = Worker::<usize>::new(4);

			stealer
				.steal_batch(&dest_worker, |n| n - n / 2)
				.unwrap_or(0)
		}

		loom::model(|| {
			let worker = Worker::<usize>::new(4);
			let stealer1 = worker.stealer();
			let stealer2 = worker.stealer();

			let th1 = thread::spawn(move || steal_half(stealer1));
			let th2 = thread::spawn(move || steal_half(stealer2));

			worker.push(1).unwrap();
			worker.push(7).unwrap();

			// Try to fill up the queue.
			let spare_capacity = worker.spare_capacity();
			assert!(spare_capacity >= 2);
			worker.extend(0..spare_capacity);

			let mut n = 0;

			n += th1.join().unwrap();
			n += th2.join().unwrap();

			while worker.pop().is_some() {
				n += 1;
			}

			assert_eq!(2 + spare_capacity, n);
		});
	}
}
