//! Module containing the different Latch synchronization primitives

use core::sync::atomic::Ordering;
use vr_core::sync::{
	atomic::{AtomicU8, AtomicUsize},
	Arc,
};

use crate::registry::{Registry, WorkerThread};

/// The trait a latch must implmeent
///
/// A latch is either set or not, it is only intended to be set once via the
/// set method. Once the latch is set every succesquent call of probe will return
/// true
///
pub trait Latch {
	/// Sets the latch
	fn set(&self);

	/// Probes whether the latch is set
	fn probe(&self) -> bool;
}

/// A [`Latch`] which can be waited blockingly for completion.
#[allow(unused)]
pub trait WaitableLatch: Latch {
	/// Wait blockingly for the latch to be set, returns true when the latch
	/// is actually set
	fn wait(&self) -> bool;
}

/// A [`Latch`] which can be reset.
#[allow(unused)]
pub trait ResetableLatch: Latch {
	/// Resets the latch, returns true if the latch was previously set
	fn reset(&self) -> bool;
}

/// A [`Latch`] which can be waited blockingly for and which is resetable
#[allow(unused)]
pub trait WaitableResetableLatch: WaitableLatch + ResetableLatch {
	/// Wait for the latch to be set and then reset it, returns the state the
	/// latch was in before resetting
	fn wait_and_reset(&self) -> bool;
}

#[repr(u8)]
/// The inner states of a [`CoreLatch`].
enum CoreLatchStates {
	/// The latch is unset.
	Unset = 0,
	/// The thread, which this latch belongs to, is sleepy.
	Sleepy = 1,
	/// The thread, which this latch belongs to, is sleeping.
	Sleeping = 2,
	/// The latch is set.
	Set = 3,
}

/// A type that can be viewed as a [`CoreLatch`]
pub trait AsCoreLatch {
	/// Convert a reference of the type, to a reference to a [`CoreLatch`]
	fn as_core_latch(&self) -> &CoreLatch;
}

/// A core latch
pub struct CoreLatch {
	/// A value of [`CoreLatchStates`]
	inner: AtomicU8,
}

impl Default for CoreLatch {
	fn default() -> Self {
		Self {
			inner: AtomicU8::new(CoreLatchStates::Unset as _),
		}
	}
}

impl AsCoreLatch for CoreLatch {
	fn as_core_latch(&self) -> &CoreLatch {
		self
	}
}

impl CoreLatch {
	/// Creates a new [`CoreLatch`].
	#[allow(unused)]
	pub fn new() -> Self {
		Self::default()
	}

	/// Sets the latch from [`Unset`](CoreLatchStates::Unset) to
	/// [`Sleepy`](CoreLatchStates::Sleepy). Returns true on success.
	pub fn set_sleepy(&self) -> bool {
		// Ordering: if this was unsuccessful, it means that the latch is either
		// already sleepy, sleeping or set which is not a valid state to construct
		// a new sleepy from meaning polling is irrelevant.
		self.inner
			.compare_exchange(
				CoreLatchStates::Unset as _,
				CoreLatchStates::Sleepy as _,
				Ordering::SeqCst,
				Ordering::Relaxed,
			)
			.is_ok()
	}

	/// Converts the latch from [`Sleepy`](CoreLatchStates::Sleepy) to
	/// [`Sleeping`](CoreLatchStates::Sleeping). Returns true on success.
	pub fn set_sleep(&self) -> bool {
		// Ordering: if this was unsuccessful, it means that the latch is either
		// already sleepign, unset or set which is not a valid state to construct
		// a new sleepy from meaning polling is irrelevant.
		self.inner
			.compare_exchange(
				CoreLatchStates::Sleepy as _,
				CoreLatchStates::Sleeping as _,
				Ordering::SeqCst,
				Ordering::Relaxed,
			)
			.is_ok()
	}

	/// Converts the latch from [`Sleeping`](CoreLatchStates::Sleeping) to
	/// [`Unset`](CoreLatchStates::Unset). Returns true on success.
	pub fn wake_up(&self) -> bool {
		// Ordering: if this was unsuccessful, it means that the latch is either
		// already sleepign, unset or set which is not a valid state to construct
		// a new sleepy from meaning polling is irrelevant.
		self.inner
			.compare_exchange(
				CoreLatchStates::Sleeping as _,
				CoreLatchStates::Unset as _,
				Ordering::SeqCst,
				Ordering::Relaxed,
			)
			.is_ok()
	}

	/// Sets the latch. Returns true if the latch is in a sleeping state.
	pub fn set(&self) -> bool {
		self.inner.swap(CoreLatchStates::Set as _, Ordering::AcqRel)
			== CoreLatchStates::Sleeping as u8
	}

	/// Probes whether the latch is set.
	pub fn probe(&self) -> bool {
		// Ordering: It is just important on probe that all accesses after it remain.
		self.inner.load(Ordering::Acquire) == CoreLatchStates::Set as u8
	}
}

/// A basic latch that packages a core latch and can wake up sleeping waiting
/// threads.
pub struct SpinLatch<'a> {
	/// The underlying latch
	core_latch: CoreLatch,
	/// reference to the worker's registry
	registry: &'a Arc<Registry>,
	/// The worker in question.
	worker_index: usize,
}

impl<'a> SpinLatch<'a> {
	/// Creates a new [`SpinLatch`], for a specific worker and registry.
	pub fn new(registry: &'a Arc<Registry>, worker_index: usize) -> Self {
		SpinLatch {
			core_latch: CoreLatch::default(),
			registry,
			worker_index,
		}
	}
}

impl<'a> AsCoreLatch for SpinLatch<'a> {
	fn as_core_latch(&self) -> &CoreLatch {
		&self.core_latch
	}
}

impl<'a> Latch for SpinLatch<'a> {
	fn set(&self) {
		if self.core_latch.set() {
			self.registry.notify_latch_set(self.worker_index);
		}
	}

	fn probe(&self) -> bool {
		self.core_latch.probe()
	}
}

/// A latch that counts the Set operations with a counter.
///
/// On set the counter will be reduced by one and if it reaches zero the
/// latch counts as set.
pub struct CountLatch<'a> {
	/// The count of increments and set operations
	count: AtomicUsize,
	/// The underlying core latch
	core_latch: CoreLatch,
	/// a reference to the registry of this latch
	registry: &'a Arc<Registry>,
	/// The worker this latch actually belongs to, it will be notified upon set.
	worker_index: usize,
}

impl<'a> CountLatch<'a> {
	/// Construct a new [`CountLatch`] with the count of 1
	pub fn new(owner: &'a WorkerThread) -> Self {
		Self::with_count(1, owner)
	}

	/// Construct a new [`CountLatch`] with a specific count
	pub fn with_count(count: usize, owner: &'a WorkerThread) -> Self {
		Self {
			count: AtomicUsize::new(count),
			core_latch: CoreLatch::default(),
			registry: owner.registry(),
			worker_index: owner.index(),
		}
	}

	/// increment the count of the [`CountLatch`]
	pub fn increment(&self) {
		let old = self.count.fetch_add(1, Ordering::Relaxed);
		debug_assert_ne!(old, 0)
	}
}

impl<'a> Latch for CountLatch<'a> {
	fn set(&self) {
		if self.count.fetch_sub(1, Ordering::SeqCst) == 1 && self.core_latch.set() {
			self.registry.notify_latch_set(self.worker_index);
		}
	}

	fn probe(&self) -> bool {
		self.core_latch.probe()
	}
}

impl<'a> AsCoreLatch for CountLatch<'a> {
	fn as_core_latch(&self) -> &CoreLatch {
		&self.core_latch
	}
}

/// A latch that can be set once
#[derive(Default)]
pub struct OnceLatch {
	/// The underlying core latch
	core_latch: CoreLatch,
}

impl OnceLatch {
	/// Creates a new [`OnceLatch`].
	pub fn new() -> Self {
		Self::default()
	}

	/// Sets the latch and wakes the thread which has been waiting on it.
	pub fn set_and_wake(&self, registry: &Registry, worker_index: usize) {
		if self.core_latch.set() {
			registry.notify_latch_set(worker_index)
		}
	}
}

impl AsCoreLatch for OnceLatch {
	fn as_core_latch(&self) -> &CoreLatch {
		&self.core_latch
	}
}
