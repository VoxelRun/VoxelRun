//! The synchronization primitives used by this crate

pub mod deque;
pub mod latch;
pub mod sleep;
