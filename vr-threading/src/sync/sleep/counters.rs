//! Counters for the number of sleeping and awake threads and job events.

use core::sync::atomic::Ordering;

use vr_core::sync::atomic::AtomicUsize;

/// Number of bits used for the thread counters.
#[cfg(target_pointer_width = "64")]
pub const THREADS_BITS: usize = 16;

#[cfg(target_pointer_width = "32")]
pub const THREADS_BITS: usize = 8;

/// Bits to shift to select the sleeping threads
/// (used with `select_bits`).
#[allow(clippy::erasing_op)]
pub const SLEEPING_SHIFT: usize = 0 * THREADS_BITS;

/// Bits to shift to select the inactive threads
/// (used with `select_bits`).
#[allow(clippy::identity_op)]
pub const INACTIVE_SHIFT: usize = 1 * THREADS_BITS;

/// Bits to shift to select the JEC
/// (use JOBS_BITS).
pub const JEC_SHIFT: usize = 2 * THREADS_BITS;

/// Max value for the thread counters.
pub const THREADS_MAX: usize = (1 << THREADS_BITS) - 1;

/// Constant that can be added to add one sleeping thread.
pub const ONE_SLEEPING: usize = 1;

/// Constant that can be added to add one inactive thread.
/// An inactive thread is either idle, sleepy, or sleeping.
pub const ONE_INACTIVE: usize = 1 << INACTIVE_SHIFT;

/// Constant that can be added to add one to the JEC.
pub const ONE_JEC: usize = 1 << JEC_SHIFT;

/// Atomic wrapper for [`Counters`]
pub struct AtomicCounters {
	/// - Bits 0..[`THREADS_BITS`]: n of sleeping threads
	/// - Bits [`THREADS_BITS`]..2 * [`THREADS_BITS`]: n of inactive threads
	/// - Bits 2 * [`THREADS_BITS`]..: Stores the [JobEventCounter]
	values: AtomicUsize,
}

/// Helpers for the inner value of [`AtomicCounters`]
#[derive(Default, Clone, Copy)]
pub struct Counters {
	/// - Bits 0..[`THREADS_BITS`]: n of sleeping threads
	/// - Bits [`THREADS_BITS`]..2 * [`THREADS_BITS`]: n of inactive threads
	/// - Bits 2 * [`THREADS_BITS`]..: Stores the [JobEventCounter]
	values: usize,
}

/// Counts how many job events have taken place.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct JobEventCounter {
	/// The job event count
	value: usize,
}

impl JobEventCounter {
	/// An awake job event counter
	pub const DUMMY: JobEventCounter = JobEventCounter { value: usize::MAX };

	/// Check whether the thread should is going to sleep
	pub fn is_going_to_sleep(self) -> bool {
		(<JobEventCounter as Into<usize>>::into(self) & 1) == 0
	}

	/// Check whether the thread is active.
	pub fn is_active(self) -> bool {
		!self.is_going_to_sleep()
	}
}

impl From<usize> for JobEventCounter {
	fn from(value: usize) -> Self {
		JobEventCounter {
			value: value >> JEC_SHIFT,
		}
	}
}

impl From<JobEventCounter> for usize {
	fn from(value: JobEventCounter) -> Self {
		value.value
	}
}

impl Counters {
	/// Construct a new [`Counters`]
	pub fn new(values: usize) -> Self {
		Self { values }
	}

	/// Increment the Job counter
	pub fn increment_jobs_counter(self) -> Counters {
		Counters {
			values: self.values.wrapping_add(ONE_JEC),
		}
	}

	/// Get the job counter
	pub fn job_counter(self) -> JobEventCounter {
		self.values.into()
	}

	/// Get the number of inactive threads
	pub fn inactive_threads(self) -> usize {
		(self.values >> INACTIVE_SHIFT) & THREADS_MAX
	}

	/// Get the number of sleeping threads
	pub fn sleeping_threads(self) -> usize {
		(self.values >> SLEEPING_SHIFT) & THREADS_MAX
	}

	/// Get the number of inactive awake threads
	pub fn awake_but_idle_threads(self) -> usize {
		debug_assert!(self.sleeping_threads() <= self.inactive_threads());
		self.inactive_threads() - self.sleeping_threads()
	}
}

impl From<usize> for Counters {
	fn from(value: usize) -> Self {
		Counters::new(value)
	}
}

impl AtomicCounters {
	/// Creates a new [`AtomicCounters`].
	pub fn new() -> Self {
		Self {
			values: AtomicUsize::new(0),
		}
	}

	/// Load the underlying [`Counters`]
	pub fn load(&self, ordering: Ordering) -> Counters {
		Counters::new(self.values.load(ordering))
	}

	/// Stores a value into the [`AtomicCounters`] if the current value is the
	/// same as the `current` value.
	///
	/// The return value is a result indicating whether the new value was
	/// written and containing the previous value. On success this value is
	/// guaranteed to be equal to `current`.
	///
	/// `compare_exchange` takes two [`Ordering`] arguments to describe the
	/// memory ordering of this operation. `success` describes the required
	/// ordering for the read-modify-write operation that takes place if the
	/// comparison with `current` succeeds. `failure` describes the required
	/// ordering for the load operation that takes place when the comparison
	/// fails. Using [`Acquire`](Ordering::Acquire) as success ordering makes
	/// the store part of this operation [`Relaxed`](Ordering::Relaxed), and
	/// using [`Release`](Ordering::Release) makes the successful load
	/// [`Relaxed`](Ordering::Relaxed). The failure ordering can only be
	/// [`SeqCst`](Ordering::SeqCst), [`Acquire`](Ordering::Acquire) or
	/// [`Relaxed`](Ordering::Relaxed).
	pub fn compare_exchange(
		&self,
		current: Counters,
		new: Counters,
		ordering: Ordering,
	) -> Result<Counters, Counters> {
		self.values
			.compare_exchange(current.values, new.values, ordering, Ordering::Relaxed)
			.map(|ok| ok.into())
			.map_err(|err| err.into())
	}

	/// Add an inactive thread to the counters
	pub fn add_inactive(&self) {
		self.values.fetch_add(ONE_INACTIVE, Ordering::SeqCst);
	}

	/// Remove an inactive thread from the counters
	pub fn sub_inactive(&self) -> usize {
		let old_value = Counters::new(self.values.fetch_sub(ONE_INACTIVE, Ordering::SeqCst));
		debug_assert!(old_value.inactive_threads() > 0);
		debug_assert!(old_value.sleeping_threads() <= old_value.inactive_threads());

		let sleeping_threads = old_value.sleeping_threads();
		Ord::min(sleeping_threads, 2)
	}

	/// Try to add a sleeping thread to the counters, returns true on success
	pub fn try_add_sleeping(&self, old_value: Counters) -> bool {
		debug_assert!(old_value.inactive_threads() > 0);
		debug_assert!(old_value.sleeping_threads() < THREADS_MAX);
		let mut new_value = old_value;
		new_value.values += ONE_SLEEPING;
		self.compare_exchange(old_value, new_value, Ordering::SeqCst)
			.is_ok()
	}

	/// Remove a sleeping thread from the counters
	pub fn sub_sleeping(&self) {
		let old_value = Counters::new(self.values.fetch_sub(ONE_SLEEPING, Ordering::SeqCst));
		debug_assert!(old_value.sleeping_threads() > 0);
		debug_assert!(old_value.sleeping_threads() <= old_value.inactive_threads());
	}

	/// Increment the job counter on a condition
	pub fn increment_jobs_counter_if<F>(&self, condition: F) -> Counters
	where
		F: Fn(JobEventCounter) -> bool,
	{
		loop {
			let old_value = self.load(Ordering::SeqCst);
			if condition(old_value.job_counter()) {
				let new_value = old_value.increment_jobs_counter();
				if self
					.compare_exchange(old_value, new_value, Ordering::SeqCst)
					.is_ok()
				{
					return new_value;
				}
			} else {
				return old_value;
			}
		}
	}
}

impl Default for AtomicCounters {
	fn default() -> Self {
		Self::new()
	}
}

impl std::fmt::Debug for Counters {
	fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let word = format!("{:016x}", self.values);
		fmt.debug_struct("Counters")
			.field("word", &word)
			.field("jobs", &self.job_counter())
			.field("inactive", &self.inactive_threads())
			.field("sleeping", &self.sleeping_threads())
			.finish()
	}
}
