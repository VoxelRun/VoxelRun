//! Sleeping state managers for threads

use core::sync::atomic::Ordering;

use vr_core::{
	debug_or_loom_assert, debug_or_loom_assert_eq,
	sync::{self, Condvar, Mutex},
	thread::yield_now,
	utils::CachePadded,
};

use self::counters::{AtomicCounters, JobEventCounter};

use super::latch::CoreLatch;

pub mod counters;

/// Struct containing the sleep state for all workers of a threadpool
pub struct Sleep {
	/// The sleep states
	worker_sleep: Vec<CachePadded<SleepState>>,
	/// The counters of inactive, idle, sleeping threads
	counters: AtomicCounters,
}

/// A workers sleep state
pub struct SleepState {
	/// Active?
	is_active: Mutex<bool>,
	/// The condition variable that will be waited on when idle
	condition: Condvar,
}

impl Default for SleepState {
	fn default() -> Self {
		Self {
			is_active: Mutex::new(true),
			condition: Condvar::new(),
		}
	}
}

/// A struct that will be used for callback when entering an idle state before
/// sleeping
pub struct IdleState {
	/// The worker that is currently idling
	worker_index: usize,
	/// How many stealing rounds it was already idle
	rounds: u32,
	/// How many Job events took place
	jobs_counter: JobEventCounter,
}

/// Number of rounds until a thread announces that it will be going to sleep
const ROUNDS_UNTIL_SLEEPY: u32 = 32;
/// Number of rounds until a thread will sleep.
const ROUNDS_UNTIL_SLEEP: u32 = ROUNDS_UNTIL_SLEEPY + 2;

impl Sleep {
	/// Creates a new [`Sleep`], with a given number of threads.
	pub fn new(threads: usize) -> Self {
		Self {
			worker_sleep: (0..threads).map(|_| Default::default()).collect(),
			counters: AtomicCounters::new(),
		}
	}

	/// The thread's queue is empty and it will now be looking for work
	pub fn start_looking(&self, worker_index: usize) -> IdleState {
		self.counters.add_inactive();
		IdleState {
			worker_index,
			rounds: 0,
			jobs_counter: JobEventCounter::DUMMY,
		}
	}

	/// The thread didn't find any work in the current iteration. Will
	/// set the thread to sleep after a certain number of rounds.
	pub fn no_work_found<F>(&self, idle: &mut IdleState, latch: &CoreLatch, global_jobs: F)
	where
		F: FnOnce() -> bool,
	{
		if idle.rounds < ROUNDS_UNTIL_SLEEPY {
			idle.rounds += 1;
			yield_now();
			return;
		}

		if idle.rounds == ROUNDS_UNTIL_SLEEPY {
			idle.jobs_counter = self.announce_sleepy();
			idle.rounds += 1;
			yield_now();
			return;
		}

		if idle.rounds < ROUNDS_UNTIL_SLEEP {
			idle.rounds += 1;
			yield_now();
			return;
		}

		debug_or_loom_assert_eq!(idle.rounds, ROUNDS_UNTIL_SLEEP);
		self.sleep(idle, latch, global_jobs);
	}

	/// Announce that the thread will soon be going to sleep.
	pub fn announce_sleepy(&self) -> JobEventCounter {
		self.counters
			.increment_jobs_counter_if(JobEventCounter::is_active)
			.job_counter()
	}

	/// Set the thread to sleep waiting on a latch. It needs other threads to
	/// wake it via [`Sleep::wake_thread`] and eventually by [`Sleep::wake_any`]
	/// or more idiomatically [`Sleep::notify_latch_set`].
	pub fn sleep<F>(&self, idle: &mut IdleState, latch: &CoreLatch, global_jobs: F)
	where
		F: FnOnce() -> bool,
	{
		let worker_index = idle.worker_index;

		if !latch.set_sleepy() {
			return;
		}

		let sleep_state = &self.worker_sleep[worker_index];
		let mut is_active = sleep_state.is_active.lock().unwrap();
		debug_or_loom_assert!(*is_active);

		if !latch.set_sleep() {
			idle.wake_fully();
			return;
		}

		loop {
			let counters = self.counters.load(Ordering::SeqCst);
			debug_or_loom_assert!(idle.jobs_counter.is_going_to_sleep());
			if counters.job_counter() != idle.jobs_counter {
				idle.wake_partly();
				latch.wake_up();
				return;
			}

			if self.counters.try_add_sleeping(counters) {
				break;
			}
		}

		sync::atomic::fence(Ordering::SeqCst);
		if global_jobs() {
			self.counters.sub_sleeping();
		} else {
			*is_active = false;
			while !*is_active {
				is_active = sleep_state.condition.wait(is_active).unwrap();
			}
		}

		idle.wake_fully();
		latch.wake_up();
	}

	/// Wake a specific thread, will return true if the thread was actually sleeping
	pub fn wake_thread(&self, worker_index: usize) -> bool {
		let sleep_state = &self.worker_sleep[worker_index];
		let mut is_active = sleep_state.is_active.lock().unwrap();

		if *is_active {
			return false;
		}

		*is_active = true;
		sleep_state.condition.notify_one();
		self.counters.sub_sleeping();

		true
	}

	/// Wake at most a certain number of threads.
	pub fn wake_any(&self, mut num_to_wake: usize) {
		if num_to_wake == 0 {
			return;
		}
		for i in 0..self.worker_sleep.len() {
			if self.wake_thread(i) {
				num_to_wake -= 1;
				if num_to_wake == 0 {
					return;
				}
			}
		}
	}

	/// Notify a thread that the latch it was blocking on is set, wake it if
	/// it was sleeping.
	pub fn notify_latch_set(&self, target_worker: usize) {
		self.wake_thread(target_worker);
	}

	/// Announce that there are new jobs in the global queue.
	pub fn new_global_jobs(&self, num_jobs: u32, queue_was_empty: bool) {
		sync::atomic::fence(Ordering::SeqCst);
		self.new_jobs(num_jobs, queue_was_empty);
	}

	/// Announce that there are new jobs on a local queue.
	pub fn new_jobs(&self, num_jobs: u32, queue_was_empty: bool) {
		let counters = self
			.counters
			.increment_jobs_counter_if(JobEventCounter::is_going_to_sleep);
		let num_awake_but_idle = counters.awake_but_idle_threads();
		let num_sleeping = counters.sleeping_threads();

		if num_sleeping == 0 {
			return;
		}

		let num_sleeping = num_sleeping as _;
		let num_awake_but_idle = num_awake_but_idle as u32;
		if !queue_was_empty {
			let num_to_wake = std::cmp::min(num_jobs, num_sleeping);
			self.wake_any(num_to_wake as _);
		} else if num_awake_but_idle < num_jobs {
			let num_to_wake = std::cmp::min(num_jobs - num_awake_but_idle, num_sleeping);
			self.wake_any(num_to_wake as _);
		}
	}

	/// Indicate that work was found and wake up an appropriate number of
	/// threads.
	pub(crate) fn work_found(&self) {
		let to_wake = self.counters.sub_inactive() as _;
		self.wake_any(to_wake);
	}
}

impl IdleState {
	/// Reset the [`IdleState`]
	fn wake_fully(&mut self) {
		self.rounds = 0;
		self.jobs_counter = JobEventCounter::DUMMY;
	}

	/// Set the [`IdleState`] to sleepy again.
	fn wake_partly(&mut self) {
		self.rounds = ROUNDS_UNTIL_SLEEPY;
		self.jobs_counter = JobEventCounter::DUMMY;
	}
}
