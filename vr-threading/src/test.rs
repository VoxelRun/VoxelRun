use core::num::NonZeroUsize;
use std::{
	env,
	os::unix::process::ExitStatusExt,
	process::{Command, ExitStatus, Stdio},
	sync::Barrier,
};

use crate::{
	handle::DefaultHandle,
	registry::WorkerThread,
	threadpool::{ThreadPoolBuilder, ThreadPoolCreationError},
	ThreadPoolHandle,
};

#[test]
fn double_init_fail() {
	ThreadPoolBuilder::new()
		.spawn(|| {
			assert!(ThreadPoolBuilder::new()
				.spawn(|| {})
				.is_err_and(|err| matches!(err, ThreadPoolCreationError::AlreadyInThreadPool)))
		})
		.unwrap();
}

#[test]
fn cleanup_test() {
	ThreadPoolBuilder::new().spawn(|| ()).unwrap();
	assert_eq!(core::ptr::null(), WorkerThread::current())
}

#[test]
#[should_panic(expected = "success")]
fn simple_panic() {
	ThreadPoolBuilder::new()
		.spawn(|| panic!("success"))
		.unwrap();
}

fn force_stack_overflow(depth: u32) {
	let mut buffer = [0u8; 1024 * 1024];
	std::hint::black_box(&mut buffer);
	if depth > 0 {
		force_stack_overflow(depth - 1);
	}
}

#[cfg(unix)]
fn disable_core() {
	// Safety: FFI- But disabling the production of a core file is safe
	unsafe {
		libc::setrlimit(
			libc::RLIMIT_CORE,
			&libc::rlimit {
				rlim_cur: 0,
				rlim_max: 0,
			},
		);
	}
}

#[cfg(unix)]
fn overflow_code() -> Option<i32> {
	None
}

#[cfg(windows)]
fn overflow_code() -> Option<i32> {
	use std::os::windows::process::ExitStatusExt;

	ExitStatus::from_raw(0xc00000fd /*STATUS_STACK_OVERFLOW*/).code()
}

fn run_ignored(test: &str) -> ExitStatus {
	Command::new(env::current_exe().unwrap())
		.arg(test)
		.arg("--ignored")
		.arg("--exact")
		.stdout(Stdio::null())
		.stderr(Stdio::null())
		.status()
		.unwrap()
}

#[test]
#[cfg_attr(not(any(unix, windows)), ignore)]
fn stack_overflow_crash() {
	let status = run_ignored("test::run_with_small_stack");
	assert!(!status.success());

	assert_eq!(status.code(), overflow_code());

	#[cfg(target_os = "linux")]
	assert!(matches!(
		status.signal(),
		Some(libc::SIGABRT | libc::SIGSEGV)
	));

	let status = run_ignored("test::run_with_large_stack");

	#[cfg(target_os = "linux")]
	assert_eq!(status.signal(), None);

	assert_eq!(status.code(), Some(0));
}

#[test]
#[ignore]
fn run_with_small_stack() {
	run_with_stack(8);
}

#[test]
#[ignore]
fn run_with_large_stack() {
	run_with_stack(48);
}

fn run_with_stack(stack_size_in_mb: usize) {
	ThreadPoolBuilder::new()
		.num_threads(NonZeroUsize::new(2).unwrap())
		.stack_size(stack_size_in_mb * 1024 * 1024)
		.spawn(|| {
			let barrier = Barrier::new(2);
			DefaultHandle::join(
				|| barrier.wait(),
				|| {
					barrier.wait();
					#[cfg(unix)]
					disable_core();
					force_stack_overflow(32)
				},
			);
		})
		.unwrap();
}
