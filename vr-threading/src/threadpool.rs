//! Helpers for the construction of a threadpool
//!
//! Provies the [`ThreadPoolBuilder`]-struct, with which one can spawn a new
//! threadpool, with a main task that will be executed within the threadpool.
//!
//! Importantly, the threadpool provided here cannot be nested.
//!
//! You can specify following aspects of the threadpool's iniital state
//!
//! - the [number of threads](ThreadPoolBuilder::num_threads) the threadpool will have
//! - the [stack_size](ThreadPoolBuilder::stack_size) of the newly spawned threads
//! - the [names](ThreadPoolBuilder::get_thread_name) of the newly spawned threads
//! - the [queue capacity](ThreadPoolBuilder::worker_capacity) of the newly created
//!   thread-local queues.
//! - the [panic handler](ThreadPoolBuilder::panic_handler), that will be called
//!   if propagating a panic is not a viable course of action, overwriting the
//!   default abort behaviour.
//!
//! You can then spawn the threadpool with a main function with the
//! [`spawn`](ThreadPoolBuilder::spawn) function.

use core::{any::Any, num::NonZeroUsize};

use crate::{registry::Registry, PanicHandler};

/// A helper to concstruct a ThreadPool
///
/// # Creating a ThreadPool
///
/// The following creates a threadpool with the following settings:
/// - The threadpool will have 20 threads available
/// - the stack size for the 19 newly created threads will be 8 mb
/// - the 19 newly created threads will be named `Idiot #n`, where `n` is `1..=19`,
///   with each index occuring once.
/// - The worker local queues will be able to store 512 items before resorting
///   to the global, slower queue.
/// - If a task, for which the panic is not viable to propagate panics it will
///   print out `I am surrounded by idiots` and abort the process.
/// - It will spawn the threadpool, where `execute_in_pool` is executed on
///   the current thread, which if it is the main thread will be named main and
///   will have a, you could say, infinitely scaling stack size.
/// - If, for some reason the threadpool couldn't be created, because kind
///   thread I/O error occured when spawning the threads
///   [`ThreadPoolCreationError::IoError`] will be returned packaging said error.
///   The other possible failure is, if you try to nest threadpools, then
///   it will return [`ThreadPoolCreationError::AlreadyInThreadPool`].
///
///   If all goes well `Ok(v)` is returned, where `v` describes the return type
///   of the main closure, in this case `v` is a `()`.
/// ```
/// use vr_threading::threadpool::ThreadPoolBuilder;
/// use core::num::NonZeroUsize;
///
/// fn execute_in_pool() {
///     println!("Yeah parallelisation")
/// }
///
/// ThreadPoolBuilder::new()
///     .num_threads(NonZeroUsize::new(20).unwrap())
///     .stack_size(8 * (1 << 20))
///     .get_thread_name(|n| format!("Idiot #{n}"))
///     .worker_capacity(512)
///     .panic_handler(|_| {
///         println!("I am surrounded by idiots");
///         std::process::abort();
///     })
///     .spawn(execute_in_pool)
///     .expect("Failed to create threadpool")
/// ```
///
pub struct ThreadPoolBuilder {
	/// The number of threads the Threadpool will have,
	/// [`available_parallelism`](std::thread::available_parallelism) by
	/// default.
	pub(crate) num_threads: Option<NonZeroUsize>,
	/// The Stack size of the thread
	pub(crate) stack_size: Option<usize>,
	/// Function to generate a name for the thread
	pub(crate) get_thread_name: Option<Box<dyn FnMut(usize) -> String>>,
	/// The capacity of  the worker's queue.
	pub(crate) worker_capacity: Option<usize>,
	/// The panic handler for the new threadpool
	pub(crate) panic_handler: Option<Box<PanicHandler>>,
}

impl ThreadPoolBuilder {
	/// Creates a new [`ThreadPoolBuilder`].
	pub fn new() -> ThreadPoolBuilder {
		Self {
			stack_size: Default::default(),
			num_threads: Default::default(),
			get_thread_name: Default::default(),
			worker_capacity: Default::default(),
			panic_handler: Default::default(),
		}
	}

	/// Sets the stack size of the threads in this ThreadPool. Uses
	/// OS default by default.
	///
	/// Importantly, this only applies to newly spawned threads, not the one
	/// the main task is executed in when calling [`ThreadPoolBuilder::spawn`].
	pub fn stack_size(mut self, stack_size: usize) -> ThreadPoolBuilder {
		self.stack_size = Some(stack_size);
		self
	}

	/// The number of threads that will be in this ThreadPool. Uses
	/// [`available_parallelism`](std::thread::available_parallelism) by
	/// default.
	pub fn num_threads(mut self, num_threads: NonZeroUsize) -> ThreadPoolBuilder {
		self.num_threads = Some(num_threads);
		self
	}

	/// Sets the function to generate the name of the thread. Will be named
	/// `Worker n` by default.
	///
	/// The function will take in a usize, which is the thread's id and must
	/// produce a string. By default Threads are named `Worker n`.
	///
	/// It will only apply to newly spawned threads. The current thread's name
	/// will remain.
	pub fn get_thread_name<F>(mut self, get_thread_name: F) -> ThreadPoolBuilder
	where
		F: FnMut(usize) -> String + 'static,
	{
		self.get_thread_name = Some(Box::new(get_thread_name));
		self
	}

	/// Set's the capacity of the worker's queue. 256 by default. If the queue
	/// is full threads will continue to push tasks on a global queue, from
	/// which dequeing is more expensive.
	pub fn worker_capacity(mut self, worker_capacity: usize) -> ThreadPoolBuilder {
		self.worker_capacity = Some(worker_capacity);
		self
	}

	/// Assign a panic handler to a threadpool that will be called if a
	/// [`ThreadPoolHandle::task`](crate::ThreadPoolHandle::task) panics.
	///
	/// If this is not set or this panics, the process will be aborted.
	pub fn panic_handler<H>(mut self, panic_handler: H) -> ThreadPoolBuilder
	where
		H: Fn(Box<dyn Any + Send>) + Send + Sync + 'static,
	{
		self.panic_handler = Some(Box::new(panic_handler));
		self
	}

	/// Spawns the ThreadPool with a main function inside it and the previously
	/// configured settings.
	///
	/// # Panics
	///
	/// If main panics it will wait for all other tasks to finish and then
	/// propagate the panic to the caller
	pub fn spawn<F, T>(self, main: F) -> Result<T, ThreadPoolCreationError>
	where
		F: FnOnce() -> T,
	{
		Registry::spawn(self, main)
	}
}

impl Default for ThreadPoolBuilder {
	fn default() -> Self {
		Self::new()
	}
}

/// Errors that can occure when creating a new threadpool
#[derive(Debug, thiserror::Error)]
pub enum ThreadPoolCreationError {
	/// An IO-Error occured
	#[error("An IO-error occured: {error}")]
	IoError {
		/// The underlying io error
		error: std::io::Error,
	},
	/// Trying to construct a threadpool within a threadpool
	#[error("Trying to construct a threadpool within a ThreadPool.")]
	AlreadyInThreadPool,
}

impl From<std::io::Error> for ThreadPoolCreationError {
	fn from(value: std::io::Error) -> Self {
		Self::IoError { error: value }
	}
}
