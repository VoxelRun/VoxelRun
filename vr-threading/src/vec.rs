//! Module containing the [`IntoParallelIterator`] implementation for [`Vec`]
use core::{marker::PhantomData, ops::Range};

use vr_core::utils::math::simplify_range;

use crate::{
	iter::{
		plumbing::{bridge, Consumer, Producer, ProducerCallback, UnindexedConsumer},
		IndexedParallelIterator, IntoParallelIterator, ParallelDrainRange, ParallelIterator,
	},
	slice::{Iter, IterMut},
	ThreadPoolHandle,
};

/// A [`ParallelIterator`] over a [`Vec`] consuming its elements
#[derive(Debug, Clone)]
pub struct IntoIter<T, H>
where
	T: Send,
	H: ThreadPoolHandle,
{
	/// The underlying vector.
	vec: Vec<T>,
	/// The handle to the Threadpool.
	_handle: PhantomData<H>,
}

/// A [`ParallelIterator`] that drains the items of a [`Vec`] when iterating
/// over the elements.
pub struct Drain<'data, T>
where
	T: Send,
{
	/// The vector being drained
	vec: &'data mut Vec<T>,
	/// The range that will be drained.
	range: Range<usize>,
	/// The original length of the [`Vec`]
	orig_len: usize,
}

impl<T, H> ParallelIterator<H> for IntoIter<T, H>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.vec.len())
	}
}

impl<T, H> IndexedParallelIterator<H> for IntoIter<T, H>
where
	H: ThreadPoolHandle,
	T: Send,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.vec.len()
	}

	fn with_producer<CB>(mut self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		ParallelDrainRange::<H>::par_drain(&mut self.vec, ..).with_producer(callback)
	}
}

impl<'data, T, H> ParallelDrainRange<H> for &'data mut Vec<T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;
	type Iter = Drain<'data, Self::Item>;

	fn par_drain<R>(self, range: R) -> Self::Iter
	where
		R: std::ops::RangeBounds<usize>,
	{
		Self::Iter {
			orig_len: self.len(),
			range: simplify_range(range, self.len()),
			vec: self,
		}
	}
}

impl<'data, T, H> ParallelIterator<H> for Drain<'data, T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	fn drive_unindexed<C>(self, consumer: C) -> C::Result
	where
		C: UnindexedConsumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn opt_len(&self) -> Option<usize> {
		Some(self.range.len())
	}
}

impl<'data, T, H> IndexedParallelIterator<H> for Drain<'data, T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	fn drive<C>(self, consumer: C) -> C::Result
	where
		C: Consumer<Self::Item, H>,
	{
		bridge(self, consumer)
	}

	fn len(&self) -> usize {
		self.range.len()
	}

	fn with_producer<CB>(self, callback: CB) -> CB::Output
	where
		CB: ProducerCallback<Self::Item, H>,
	{
		// Safety: The elements are still valid for all accessis, until dropped
		// when self is dropped
		unsafe {
			self.vec.set_len(self.range.start);
			let producer = DrainProducer::from_vec(self.vec, self.range.len());
			callback.callback(producer)
		}
	}
}

impl<'data, T> Drop for Drain<'data, T>
where
	T: Send,
{
	fn drop(&mut self) {
		let Range { start, end } = self.range;
		if self.vec.len() == self.orig_len {
			self.vec.drain(start..end);
			return;
		}

		if start == end {
			// Safety: This was not iterated over and thus the vector can be reset
			unsafe { self.vec.set_len(self.orig_len) }
			return;
		}

		if end < self.orig_len {
			// Safety: The drain moved elements out of the range. The leftovers
			// are put at the back and the length is amended to include them.
			unsafe {
				let ptr = self.vec.as_mut_ptr().add(start);
				let tail_ptr = self.vec.as_ptr().add(end);
				let tail_len = self.orig_len - end;
				core::ptr::copy(tail_ptr, ptr, tail_len);
				self.vec.set_len(start + tail_len)
			}
		}
	}
}

/// A [`Producer`] draining items from an underlying slice
pub(crate) struct DrainProducer<'data, T>
where
	T: Send,
{
	/// The underlying slice.
	slice: &'data mut [T],
}

impl<T> DrainProducer<'_, T>
where
	T: Send,
{
	/// Creates a new [`DrainProducer<T>`] which moves the items from the slice.
	///
	/// # Safety
	///
	/// Unsafe as `!Copy` data must not be read after the borrow is released.
	pub(crate) unsafe fn new(slice: &mut [T]) -> DrainProducer<'_, T> {
		DrainProducer { slice }
	}

	/// Creates a new [`DrainProducer<T>`] which moves the items from the [`Vec`].
	///
	/// # Panics
	///
	/// Panics if `len` exceeds the length of `vec`.
	///
	/// # Safety
	///
	/// Unsafe as this moves from beyond `vec.len()`, so the caller must
	/// ensure that data is initialised and not read after the borrow is released.
	unsafe fn from_vec(vec: &mut Vec<T>, len: usize) -> Self {
		let start = vec.len();
		debug_assert!(vec.capacity() - start >= len);

		// Safety: Ensured by caller.
		unsafe {
			let ptr = vec.as_mut_ptr().add(start);
			Self::new(core::slice::from_raw_parts_mut(ptr, len))
		}
	}
}

impl<'data, T, H> Producer<H> for DrainProducer<'data, T>
where
	H: ThreadPoolHandle,
	T: Send,
{
	type Item = T;

	type IntoIter = SliceDrain<'data, T>;

	fn split_at(mut self, index: usize) -> (Self, Self) {
		// replace the slice so it isn't dropped twice.
		let slice = core::mem::take(&mut self.slice);
		let (left, right) = slice.split_at_mut(index);
		// Safety: Is ensured by the creator of the DrainProducer
		unsafe { (DrainProducer::new(left), DrainProducer::new(right)) }
	}

	fn into_iter(mut self) -> Self::IntoIter {
		let slice = core::mem::take(&mut self.slice);
		Self::IntoIter {
			iter: slice.iter_mut(),
		}
	}
}

/// Similar to [`Drain`](std::vec::Drain), without updating a source Vec.
pub(crate) struct SliceDrain<'data, T> {
	/// The core iterator that underlies.
	iter: core::slice::IterMut<'data, T>,
}

impl<'data, T> Iterator for SliceDrain<'data, T>
where
	T: 'data,
{
	type Item = T;

	fn next(&mut self) -> Option<Self::Item> {
		let ptr = self.iter.next()? as *const T;
		// Safety: This came from an option that wasn't none.
		Some(unsafe { core::ptr::read(ptr) })
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.iter.size_hint()
	}

	fn count(self) -> usize
	where
		Self: Sized,
	{
		self.iter.len()
	}
}

impl<'data, T> DoubleEndedIterator for SliceDrain<'data, T>
where
	T: 'data,
{
	fn next_back(&mut self) -> Option<Self::Item> {
		let ptr = self.iter.next_back()? as *const T;
		// Safety: This came from an option that wasn't none.
		Some(unsafe { core::ptr::read(ptr) })
	}
}

impl<'data, T> ExactSizeIterator for SliceDrain<'data, T>
where
	T: 'data,
{
	fn len(&self) -> usize {
		self.iter.len()
	}
}

impl<'data, T> core::iter::FusedIterator for SliceDrain<'data, T> where T: 'data {}
impl<'data, T> Drop for SliceDrain<'data, T> {
	fn drop(&mut self) {
		let slice_ptr: *mut [T] = core::mem::replace(&mut self.iter, [].iter_mut()).into_slice();
		// Safety: We know this is alighed as a slice
		unsafe { core::ptr::drop_in_place::<[T]>(slice_ptr) };
	}
}

impl<T, H> IntoParallelIterator<H> for Vec<T>
where
	T: Send,
	H: ThreadPoolHandle,
{
	type Iter = IntoIter<T, H>;

	type Item = T;

	fn into_par_iter(self) -> Self::Iter {
		IntoIter {
			vec: self,
			_handle: PhantomData,
		}
	}
}

impl<'data, T, H> IntoParallelIterator<H> for &'data Vec<T>
where
	T: 'data + Sync,
	H: ThreadPoolHandle,
{
	type Iter = Iter<'data, T, H>;

	type Item = &'data T;

	fn into_par_iter(self) -> Self::Iter {
		<&[T] as IntoParallelIterator<H>>::into_par_iter(self)
	}
}

impl<'data, T, H> IntoParallelIterator<H> for &'data mut Vec<T>
where
	T: 'data + Send,
	H: ThreadPoolHandle,
{
	type Iter = IterMut<'data, T, H>;

	type Item = &'data mut T;

	fn into_par_iter(self) -> Self::Iter {
		<&mut [T] as IntoParallelIterator<H>>::into_par_iter(self)
	}
}
